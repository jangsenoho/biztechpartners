<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub erp imple" data-depth="erp" data-menu="erp_03" data-subnav="erp_03">
    <div class="inner_1200">
      <?php include($_SERVER['DOCUMENT_ROOT'].'/sap/erp_nav.php');?>

        <div class="sub_cont" data-depth="imple" data-menu="imple_04" data-imnav="imple_04">

            <!-- <h2 class="ttl ttl_02">SAP Implementation​</h2> -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/sap/im_nav.php');?>

            <div class="ttl_box mar_no">
                <a href="../pdf_down/Biz-YTS(0).pdf" class="down" download>Biz-YTS Brochure <span>PDF Download</span> <i></i></a>
                <h2 class="ttl">Biz-YTS</h2>
                <span class="line"></span>
                <p class="desc_02">
                    ㈜비즈테크파트너스는 사용자의 편의와 업무 효율성을 제고하기 위하여 <span>연말정산을 절차를 간소화</span>한 <br class="only_w">
                    Biz-YTS (Year-End Tax Settlement) 솔루션을 보유하고 있습니다.
                </p>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>Biz-YTS</span> 개요</h3>
                </div>
                <div class="gray_box">
                    <p class="desc_02">Biz-YTS (Year-End Tax Settlement) 솔루션은 <span>국세청 전자문서 Upload 만으로</span> 모든 정보가 <span>SAP HR 로 등록</span>되며 <br class="only_w">
                        국세청 외 연말정산 데이터도 사용자가 스스로 등록 및 검증 할 수 있는 편의를 제공합니다.</p>
                </div>
                <div class="line_box">
                    <div class="dot_wrap">
                        <strong><span>연말정산 간소화</span> 서비스란?</strong>
                    </div>
                    <p class="desc_02">근로소득세 연말정산에 필요한 각종 소득공제증명자료를 국세청이 13백만 근로자를 대신해서 은행, 학교, 병의원 등 <br class="only_w">
                    영수증발급기관으로부터 수집하여 이를 국세청홈택스 (www.hometax.go.kr) 에서 제공하는 서비스입니다.</p>
                </div>
            </div>

            
            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>솔루션 도입</span> 효과</h3>
                </div>
                <ul class="ico_w25">
                    <li>
                        <div class="color_wrap">
                            <span class="wh"><div><i>01</i> 편의성 증대</div></span>
                        </div>
                        <p>한번의 저장을 통해 <br class="only_w">자료를 Upload하여 사용자의<br class="only_w"> <span>편의성</span>을 증대</p>
                    </li>
                    <li>
                        <div class="color_wrap">
                            <span class="pi" style=" background:#c00c3f "><div><i>02</i> 업무의 효율성</div></span>
                        </div>
                        <p>사용자의 입력 시간을<br class="only_w"> 최소화하여 업무의<br class="only_w"> <span>효율성</span>을 제공</p>
                    </li>
                    <li>
                        <div class="color_wrap">
                            <span class="gr" style=" background:#b0b0b0 "><div><i>03</i> 데이터 신뢰성</div></span>
                        </div>
                          <!-- BEGIN 2022 04 26 텍스트 수정  -->  
                        <p>국세청이 제공한 검증된 <br class="only_w"> 데이터를 사용자 변경 없이 <br class="only_w"> 등록하여 데이터의 <span>신뢰성</span> 향상</p>
                          <!-- END 2022 04 26 텍스트 수정  -->  
                    </li>
                    <li>
                        <div class="color_wrap">
                            <span class="dgr" style=" background:#707070 "><div><i>04</i> 인력/ 시간 절감</div></span>
                        </div>
                        <p>전자문서와 입력 데이터를<br class="only_w"> 구분하여 사용자 검증에<br class="only_w"> <span>필요한 인력 및 시간 절감</span></p>
                    </li>
                </ul>
                <div class="img_wrap show">
                    <img src="../img/sub/sap_im_14_m.jpg" alt="효과" class="only_m">
                    <img src="../img/sub/sap_im_14.png" alt="효과"  class="only_w">
                </div>
            </div>
 
           

            

           
            
           

        </div>

    </div> <!-- inner -->
     
    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="">
            </div>
           
		</div>
	</div> 


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>