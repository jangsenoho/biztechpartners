<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub erp imple" data-depth="erp" data-menu="erp_03" data-subnav="erp_03">
    <div class="inner_1200">
    

      <?php include($_SERVER['DOCUMENT_ROOT'].'/sap/erp_nav.php');?>

        <div class="sub_cont" data-depth="imple" data-menu="imple_03" data-imnav="imple_03">

            <!-- <h2 class="ttl ttl_02">SAP Implementation​</h2> -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/sap/im_nav.php');?>

            <div class="ttl_box mar_no">
                <!-- BEGIN 2022 04 26 텍스트 수정  -->  
                <a href="../pdf_down/Biz-RES.pdf" class="down" download>BIZ-RMS Brochure <span>PDF Download</span> <i></i></a>
                <h2 class="ttl">BIZ-RMS</h2>>
                <span class="line"></span>
                <p class="desc_02">
                BIZ-RMS(Retired Employee Separator Solution)는 개인정보보호법 인사노무 가이드에 따라 <br class="only_w">
                 <!-- END 2022 04 26 텍스트 수정  -->
                퇴직 근로자의 개인정보를 재직자와 분리 처리하여<br class="only_w">
                퇴직자 개인정보 보호법을 준수할 수 있는 <span>SAP HR 퇴직자 정보 분리 솔루션</span>입니다.
                </p>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>퇴직자개인정보</span> 보호법</h3>
                </div>
                <div class="img_wrap show">
                    <img src="../img/sub/sap_im_10_m.jpg" alt="보호법" class="only_m">
                    <img src="../img/sub/sap_im_10.png" alt="보호법" class="only_w">
                </div>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>Value</span> Process Overview</h3>
                </div>
                <div class="img_wrap show" >
                    <img src="../img/sub/sap_im_11_m.jpg" alt="보호법" class="only_m">
                    <img src="../img/sub/sap_im_11.png" alt="res벨류" class="only_w">
                </div>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                     <!-- BEGIN 2022 04 26 텍스트 수정  --> 
                    <h3 class="bor_ttl"><span>BIZ-RMS</span> 실행화면</h3>
                        <!-- END 2022 04 26 텍스트 수정  -->
                </div>
                <div class="img_wrap show">
                    <img src="../img/sub/sap_im_12_m.jpg" alt="보호법" class="only_m">
                    <img src="../img/sub/sap_im_12.png" alt="실행화면" class="only_w">
                </div>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>퇴직자개인정보</span> 분리 결과</h3>
                </div>
                <p class="desc_02">기존 Program 활용성에 영향을 주지 않는 범위에서 <span>퇴직자의 인사정보 중 개인을 식별할 수 있는 값 분리</span></p>
                <div class="img_wrap show">
                    <img src="../img/sub/sap_im_13_m.jpg" alt="보호법" class="only_m">
                    <img src="../img/sub/sap_im_13.png" alt="분리 결과" class="only_w">
                </div>
            </div>
 
           

            

           
            
           

        </div>

    </div> <!-- inner -->
     
    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="">
            </div>
           
		</div>
	</div>


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>