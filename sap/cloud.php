<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub erp" data-depth="erp" data-menu="erp_02" data-subnav="erp_02">
    <div class="inner_1200">

      <?php include($_SERVER['DOCUMENT_ROOT'].'/sap/erp_nav.php');?>

        <div class="sub_cont">
            <div class="ttl_box mar_no">
                <a href="../pdf_down/BizTech_Cloud_v3.pdf" class="down" download>Cloud Brochure <span>PDF Download</span> <i></i></a>
                <h2 class="ttl">Cloud</h2>
                <span class="line"></span>
                <p class="desc_02">비즈테크파트너스는 SAP Gold 파트너이자 AWS, AZURE 파트너이며 <br class="only_w"> 
                    LG 그룹사 SAP ERP 부문 Cloud 전문 역량을 바탕으로 <br class="only_w"> 
                    Cloud Service 컨설팅, 구축, 운영까지 <span>Cloud 전 영역을 체계적이고 전문적으로 지원</span>합니다.
                </p>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>Cloud Service</span> 영역</h3>
                </div>
                
                <div class="img_wrap cloud_01 show">
                    <img src="../img/sub/cloud_01_m.jpg" alt="서비스 영역" class="only_m">
                    <img src="../img/sub/cloud_01.jpg" alt="서비스 영역" class="only_w">
                  
                </div>
               <ul class="service_list">
                    <li>
                        <strong><span>A</span>rchitecture</strong>
                        <ul>
                            <li><i>·</i>  Multi Cloud(AWS, AZURE, GCP) 기반 Architecture 설계</li>
                            <li><i>·</i>  비즈니스에 적합한 설계 및 Roadmap 제시</li>
                        </ul>
                    </li>
                    <li>
                        <strong><span>C</span>onsulting</strong>
                        <ul>
                            <li><i>·</i>  AS-IS 시스템 분석 및 비즈니스에 최적화된 TO-BE 설계</li>
                            <li><i>·</i>  안정성, 비즈니스 우선 순위 고려 전환 전략 수립</li>
                        </ul>
                    </li>
                    <li>
                        <strong><span>M</span>anagement</strong>
                        <ul>
                            <li><i>·</i>  시스템 모니터링, 보안 등 관제 서비스 제공</li>
                            <li><i>·</i>  Cloud 기반 Application 운영 유지보수</li>
                            <li><i>·</i>  사용량 기준 최적화 방안 정기적인 offering</li>
                        </ul>
                    </li>
                    <li> 
                        <strong><span>I</span>mplementation</strong>
                        <ul>
                            <li><i>·</i>  SAP 및 연계 솔루션 구축 및 이관</li>
                            <li><i>·</i>  고객의 요구사항 및 비즈니스에 최적화된 Cloud 솔루션 적용</li>
                            <li><i>·</i>  Cloud 기반 개발 및 배포</li>
                        </ul>
                    </li>
               </ul>
            </div>
 
            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>Why SAP</span> On Cloud?</h3>
                </div>
                
                <ul class="ico_50w">
                    <li>
                        <div class="ico_img">
                            <img src="../img/sub/cloud_02_m.png" alt="Cloud_아이콘" class="only_m">
                            <img src="../img/sub/cloud_02.png" alt="Cloud_아이콘" class="only_w">
                            
                        </div>
                        <div class="ico_txt">
                            <strong><span>Why SAP</span> On Cloud?</strong>
                            <p><i>ㆍ</i> TCO 절감</p>
                            <p><i>ㆍ</i> 빠른 인프라 설치 및 확장 가능</p>
                            <p><i>ㆍ</i> 빠른 복제 및 이중화 구성 가능</p>
                            <p><i>ㆍ</i> 사용량 기준 서버 관리 가능</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_img">
                            <img src="../img/sub/cloud_03_m.png" alt="Partners_아이콘" class="only_m">  
                            <img src="../img/sub/cloud_03.png" alt="Partners_아이콘" class="only_w">
                            
                        </div>
                        <div class="ico_txt">
                            <strong><span>Why</span> BizTech Partners?</strong>
                            <p><i>ㆍ</i> SAP ERP Competency 보유</p>
                            <p><i>ㆍ</i> LG그룹 SAP ERP Cloud 전문 역량</p>
                            <p><i>ㆍ</i> 기업 성장 맞춤형 컨설팅 가능</p>
                            <p><i>ㆍ</i> Multi Cloud 역량 보유로 고객에 최적화된 컨설팅 가능</p>
                            <p><i>ㆍ</i> SAP 연계 Web, 암호화 등 Total 관리 역량 보유</p>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>Cloud</span> 특장점</h3>
                </div>
                
                <ul class="ico_20w">
                    <li>
                        <div class="ico_img">
                            <img src="../img/sub/cloud_04_m.png" alt="Fast 아이콘" class="only_m">
                            <img src="../img/sub/cloud_04.png" alt="Fast 아이콘" class="only_w">
                            
                        </div>
                        <div class="ico_txt">
                            <strong>Fast</strong>
                            <p><i>ㆍ</i> 비즈니스 요구 사항에 대한 빠르고 검증된 시스템 구현</p>
                            <p><i>ㆍ</i> 새로운 솔루션 테스트 및 적용 기간 최소화</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_img">
                            <img src="../img/sub/cloud_05.png" alt="Cost 아이콘" class="only_w">
                            <img src="../img/sub/cloud_05_m.png" alt="Cost 아이콘" class="only_m">
                        </div>
                        <div class="ico_txt">
                            <strong>Low Cost</strong>
                            <p><i>ㆍ</i> 초기 투자비용 절감 <br> (CAPEX to OPEX)</p>
                            <p><i>ㆍ</i> 주기적인 서버 교체로 인해 발생하는 시간 및 비용 부담 최소화 </p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_img">
                            <img src="../img/sub/cloud_06.png" alt="Continuity 아이콘" class="only_w">
                            <img src="../img/sub/cloud_06_m.png" alt="Continuity 아이콘" class="only_m">
                        </div>
                        <div class="ico_txt">
                            <strong>Continuity</strong>
                            <p><i>ㆍ</i> 재해 복구 및 장애 조치 절차 개선</p>
                            <p><i>ㆍ</i> Workload 증감에 빠르게 대응할 수 있는 시스템 영속성</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_img">
                            <img src="../img/sub/cloud_07.png" alt="Flexibility 아이콘" class="only_w">
                            <img src="../img/sub/cloud_07_m.png" alt="Flexibility 아이콘" class="only_m">
                        </div>
                        <div class="ico_txt">
                            <strong>Flexibility</strong>
                            <p><i>ㆍ</i> 지역 확장, 조직 변경 등에 대한 비즈니스 전략 적용</p>
                            <p><i>ㆍ</i> 구조조정, M&A, 매각 등 비즈니스 방향 변화 대응 </p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_img">
                            <img src="../img/sub/cloud_08.png" alt="Innovation 아이콘" class="only_w">
                            <img src="../img/sub/cloud_08_m.png" alt="Innovation 아이콘" class="only_m">
                        </div>
                        <div class="ico_txt">
                            <strong>Innovation</strong>
                            <!-- BIGIN: 2022 04 25 줄바꿈 수정  -->
                            <p><i>ㆍ</i> S/4HANA, IoT, AI, ML 등 Digital Transformation의 <br class="only_w">손 쉬운 확장</p>
                              <!-- END: 2022 04 25 줄바꿈 수정  -->
                        </div>
                    </li>
                </ul>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>Cloud</span> 구축 및 이관 사례</h3>
                </div>
                
                <div class="img_wrap show">
                    <img src="../img/sub/cloud_09_m.jpg" alt="구축 및 이관 사례" class="only_m">
                    <img src="../img/sub/cloud_09.png" alt="구축 및 이관 사례" class="only_w">
                   
                </div>
            </div>
            
            
           

        </div>

    </div> <!-- inner -->
    

    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="">
            </div>
           
		</div>
	</div>
     
   


</section>

<?php include($_SERVER['DOCUMENT_ROOT'] .'/inc/footer.php'); ?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가

    $('.m_header').addClass('sub')

    });
</script>