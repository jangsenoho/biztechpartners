<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub erp hr" data-depth="erp" data-menu="erp_03" data-subnav="erp_03">
    <div class="inner_1200">


      <?php include($_SERVER['DOCUMENT_ROOT'].'/sap/erp_nav.php');?>

        <div class="sub_cont" data-depth="imple" data-menu="imple_07" data-imnav="imple_07">

            <!-- <h2 class="ttl ttl_02">SAP Implementation​</h2> -->
          <?php include($_SERVER['DOCUMENT_ROOT'].'/sap/im_nav.php');?>

            <div class="ttl_box  ">
                <h2 class="ttl">NEXT-HR</h2>
                <span class="line"></span>
                <p class="desc_02"><strong> <span>Cloud 기반 HR 서비스, Next HR을 소개</span> 합니다.​</strong></p>
                <p class="desc_02">Next HR은 비즈테크파트너스 클라우드 기술력을 바탕으로 SaaS 형태로 서비스를 제공하여 초기 구축 비용과 운영 비용에 대한 부담을 최소화합니다. <br class="only_w">서비스에 가입하고 사용하고자 하는 기능을 선택하면 해당 기업을 위한 HR 시스템을 도입할 수 있게 됩니다.​</p>
                <p class="desc_02">비즈테크파트너스의 HR 시스템 구축, 운영 노하우를 집대성하여 기업이 꼭 필요로 하는 핵심 기능을 제공하며 외산 솔루션에서 부족한 근로기준법, <br class="only_w">세법, 개인정보보호법 등과 관련된 국내만의 고유한 업무 특성에 맞는 서비스를 제공합니다.​​</p>
                <p class="desc_02">Next HR은 인사 관리자, 사용자 모두가 쉽게 사용할 수 있는 편리하고 유연한 서비스로서 기업의 상황에 따라 필요한 프로세스 및 기능을 선택하여 <br class="only_w">빠르게 인사 업무에 활용할 수 있습니다. 또한, 비즈테크파트너스의 챗봇, RPA, AI, 빅데이터 등 디지털 신기술과 연계된 새로운 서비스로 부담 없이 <br class="only_w">업그레이드 할 수 있습니다.​</p>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>NEXT-HR</span> 개요</h3>
                </div>
                <p class="desc_02">조직, 직무, 근태, 급여, 평가 및 연말정산 등 인사 업무 전에 걸친 서비스를 제공합니다.</p>
                <div class="img_wrap show">
                    <img src="../img/sub/sap_im_35_m.jpg" alt="" class="only_m">
                    <img src="../img/sub/sap_im_35.png" alt="" class="only_w">
                </div>
            </div>

            
            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>서비스</span> 영역</h3>
                </div>
                <div class="dot_wrap">
                    <strong>표준 Next HR 서비스</strong>
                </div>
                <div class="service">
                    <p><i>01.</i>국내 기업에 적용, 검증되어 최적화된 표준 프로세스를 제공하는 서비스입니다.</p>
                    <div class="img_wrap">
                        <img src="../img/sub/sap_im_30_m.jpg" alt="" class="only_m">
                        <img src="../img/sub/sap_im_30.jpg" alt="" class="only_w">
                    </div>
                </div>

                <div class="service">
                    <p><i>02.</i>초기 구축 및 유지보수 비용을 절감할 수 있는 구독형 서비스입니다.</p>
                    <div class="img_wrap show">
                        <img src="../img/sub/sap_im_30_1_m.jpg" alt="" class="only_m">
                        <img src="../img/sub/sap_im_30_1.jpg" alt="" class="only_w">
                    </div>
                </div>

                <div class="service">
                    <p><i>03.</i>근로기준법, 세법, 개인정보보호법 등 제도 변경에 따른 업그레이드 대응을 빠르게 할 수 있는 서비스 입니다.</p>
                    <div class="img_wrap show">
                        <img src="../img/sub/sap_im_30_2_m.jpg" alt="" class="only_m">
                        <img src="../img/sub/sap_im_30_2.jpg" alt="" class="only_w">
                    </div>
                </div>

                <div class="service">
                    <p><i>04.</i>표준 Next HR 서비스에 더하여 고객 특화기능을 구현하거나 LG CNS가 확보한 신기술을 적용하는 서비스입니다.</p>
                    <div class="img_wrap show" > 
                        <img src="../img/sub/sap_im_30_3_m.jpg" alt="" class="only_m">
                        <img src="../img/sub/sap_im_30_3.jpg" alt="" class="only_w">
                    </div>
                </div>

               
                
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>차별화</span> 포인트</h3>
                </div>
                <p class="desc_02">국내 업무 특성에 맞는 인사 기능과 Cloud 기술을 결합한 HR SaaS 입니다.</p>
            </div>

            <ul class="gray_asome_02 ">
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/sap_im_31_m.png" alt="" style="max-width: 60px" class="only_m">
                            <img src="../img/sub/sap_im_31.png" alt="" class="only_w">
                        </div>
                        <strong>사용량 기반의 <br>SaaS형 서비스</strong>
                        <p>·  초기 구축 비용과 운영 비용에 <br class="only_w">대한 부담을 최소화</p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/sap_im_32_m.png" alt="" style="max-width: 53px" class="only_m">
                            <img src="../img/sub/sap_im_32.png" alt=""  class="only_w">
                        </div>
                        <strong>국내 업무 특성에 <br>맞는 HR 서비스</strong>
                        <p>·  세법, 근로기준법, 개인정보보호법  <br class="only_w">등에 대한 즉시 대응</p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/sap_im_33_m.png" alt="" style="max-width: 53px" class="only_m">
                            <img src="../img/sub/sap_im_33.png" alt=""  class="only_w">
                        </div>
                        <strong>편리하고 유연한<br>HR 서비스</strong>
                        <p>·  Drag &Drop방식 조직 개편 기능, <br class="only_w">One Page 인사정보 관리</p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/sap_im_34_m.png" alt="" style="max-width: 60px" class="only_m">
                            <img src="../img/sub/sap_im_34.png" alt=""  class="only_w">
                        </div>
                        <strong>부담 없는 신기술 <br>도입 및 업그레이드</strong>
                        <p>·   LG CNS 로드맵에 따른, <br class="only_w">신기술 적용 및 업그레이드</p>
                    </li>
                </ul>
            

           

            

           
            
           

        </div>

    </div> <!-- inner -->
     
    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="">
            </div>
           
		</div>
	</div>
     


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>