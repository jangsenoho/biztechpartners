<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub erp imple" data-depth="erp" data-menu="erp_03" data-subnav="erp_03">
    <div class="inner_1200">
      <?php include($_SERVER['DOCUMENT_ROOT'].'/sap/erp_nav.php');?>

        <div class="sub_cont" data-depth="imple" data-menu="imple_01" data-imnav="imple_01">

            <!-- <h2 class="ttl ttl_02">SAP Implementation​</h2> -->
      <?php include($_SERVER['DOCUMENT_ROOT'].'/sap/im_nav.php');?>

            <div class="ttl_box mar_no">
                <a href="../pdf_down/03_EXprism.pdf" class="down" download>EXprism Brochure  <span>PDF Download</span> <i></i></a>
                <h2 class="ttl">EXprism ERP</h2>
                <span class="line"></span>
                <div class="img_wrap show">
                    <img src="../img/sub/sap_im_01_m.jpg" alt="솔루션소개" class="only_m"  >
                    <img src="../img/sub/sap_im_01.png" alt="솔루션소개" class="only_w">
                   
                </div>
                <p class="desc_02">
                    EXprism은 축적된 산업 경험을 바탕으로 중소∙중견 기업의 전사 자원을 ERP시스템으로 구현하기 위한 <br class="only_w">
                    <span>SAP S/4HANA Cloud, private edition 기반의 BizTech Partners ERP 구축 솔루션</span>입니다.
                </p>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span> Value</span> Proposition</h3>
                </div>
                <p class="desc_02">
                SAP S/4HANA Cloud, private edition 환경에서 BizTech Partners의 Best Practice를 통해 <br class="only_w">
                비즈니스 복잡성을 개선하고 효율적으로 업무를 처리할 수 있으며 기업 경영의 Visibility를 확보할 수 있습니다.
                </p>
                
                <div class="img_wrap pad show">
                    <img src="../img/sub/sap_im_02_m.jpg" alt="솔루션소개" class="only_m">
                    <img src="../img/sub/sap_im_02.png" alt="구축이미지" class="only_w">
                   
                </div>
               <ul class="ico_100w">
                    <li>
                        <div class="ico_img">
                            <img src="../img/sub/sap_im_03.png" alt="구독기반 아이콘"  class="only_w">
                            <img src="../img/sub/sap_im_03_m.png" alt="구독기반 아이콘" class="only_m">
                        </div>
                        <div class="ico_txt">
                            <strong><span>구독기반</span> 경제성</strong>
                            <!-- BEGIN: 2022 04 25 마크업 수정 -->
                            <p class="text-base-wrap ">FUE<span>주01&#41;</span> 라이센싱을 통해 사용 강도에 따라 공정하고 유연하게 비용 관리 가능</p>
                            <!-- END: 2022 04 25 마크업 수정 -->
                            <p>일원화된 계약 구조 기반의 비용 최적화 및 TCO 절감</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_img">
                            <img src="../img/sub/sap_im_04.png" alt="발전적 아이콘"  class="only_w">
                            <img src="../img/sub/sap_im_04_m.png" alt="발전적 아이콘" class="only_m">
                        </div>
                        <div class="ico_txt">
                            <strong><span>발전적</span> 혁신</strong>
                            <p>SAP S/4HANA Cloud 기반 최신 기술 및 프로세스의 지속적인 도입 및 반영을 위한 업그레이드 수행</p>
                            <p>자사 AMS 솔루션 BIZcare 시스템을 통한 혁신적인 비즈니스 결정 및 프로세스 처리 지원</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_img">
                            <img src="../img/sub/sap_im_05.png" alt="뛰어난 아이콘"  class="only_w">
                            <img src="../img/sub/sap_im_05_m.png" alt="뛰어난 아이콘" class="only_m">
                        </div>
                        <div class="ico_txt">
                            <strong><span>뛰어난</span> 확장성</strong>
                            <p>SAP BTP, SAP Ariba 등 Add-on 솔루션 및 비즈니스 네트워크 확장 가능</p>
                            <p>BizTech Best Practice를 통한 검증된 시스템 구현 및 확장 가능</p>
                            <p><i>- </i><span>예 :  Biz-TRADE(수출입 관리 패키지), <span>Biz-YTS(연말정산 간소화 시스템) 등</span></span></p>
                        </div>
                    </li>
               </ul>
               <span class="ps"><i>*</i>주1&#41; FUE (Full Usage Equivalent) : 구독 기반 모델의 클라우드 이용 방식이며 계산식을 통해 중량 사용자와 경량 사용자 수를 산출</span>
            </div>
 
            <div class="cont_box top_m">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>EXprism</span> powered by RISE with SAP</h3>
                </div>
                <p class="desc_02">EXprism Package Scope -> 재활용 가능한 Add-On 154개 포함</p>
                <div class="img_wrap show">
                    <img src="../img/sub/sap_im_06_m.jpg" alt="구축이미지" class="only_m">
                    <img src="../img/sub/sap_im_06.png" alt="구축이미지"  class="only_w">
                </div>
            </div>

            <div class="cont_box Times">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>Package Costs</span> & Implementation Times</h3>
                </div>
                
                <div class="img_wrap show">
                    <img src="../img/sub/sap_im_07_m.jpg" alt="구축 및 이관 사례" class="only_m" >
                    <img src="../img/sub/sap_im_07.png" alt="구축 및 이관 사례"  class="only_w">
                    
                </div>

                <ul class="list">
                    <li><i> · </i>기본 시나리오 이외에 고객의 필요에 따라 선택적으로 구현할 수 있으며, 그에 따른 시간 및 비용은 추가적으로 산정함.</li>
                    <li><i> · </i>기본 서비스 이외에 고객의 필요에 따라 추가 개발, 기준 정보 표준화, 안정화 지원 추가 서비스를 제공할 수 있으며, 그에 따른 시간 및 비용은 추가적으로 산정함.</li>
                </ul>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>SAP S/4HANA</span> 관련 문의</h3>
                </div>
                <p class="desc_02"><span>SAP S/4HANA 관련 문의사항</span>이 있으시면 연락주시기 바랍니다.</p>
                <div class="question">
                    <a href="tel:02-2084-6755" class="tel"><i></i><strong>TEL</strong>02-2084-6755</a>
                    <a href="mailto:info@Biztechpartners.co.kr" class="mail"><i></i><strong>E-mail</strong>info@Biztechpartners.co.kr</a>
                    <a href="/business/business_question.php" class="center"><i></i><strong>문의</strong><span>고객문의 바로가기 <i></i></span></a>
                </div>

                
            </div>
            
            
           

        </div>

    </div> <!-- inner -->


    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="" >
            </div>
           
		</div>
	</div>
     
   


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>

