<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub erp consulting" data-depth="erp" data-menu="erp_01" data-subnav="erp_01">
    <div class="inner_1200">

      <?php include($_SERVER['DOCUMENT_ROOT'].'/sap/erp_nav.php');?>

        <div class="sub_cont" >
            <div class="ttl_box   ">
                <h2 class="ttl">SAP PI/ISP Consulting</h2>
                <span class="line"></span>
                <p class="desc_02">BizTech는 다양한 산업별로 차별화된 컨설팅 역량을 바탕으로 비즈니스 전략 수립, 프로세스 혁신, <br class="only_w">
                최신IT솔루션 도입 등 <span>다양한 IT컨설팅 서비스</span>를 제공하고 있습니다.​</p>
            </div>

            <div class="cont_box">
                <div class="dot_wrap">
                    <strong><span>고객의 Value를 창출</span>하는 <br class="only_m">다양한 IT컨설팅 서비스 제공</strong>
                </div>
                <div class="img_wrap">
                    <img src="../img/sub/pl_01.png" alt="" class="only_w">
                    <img src="../img/sub/pl_01_m.png" alt=""  class="only_m">
                </div>
                <div class="dot_wrap">
                    <strong><span>다양한 산업별</span>로 차별화된 컨설팅 역량</strong>
                </div>
                <div class="img_wrap">
                    <img src="../img/sub/pl_02.png" alt="" class="only_w">
                    <img src="../img/sub/pl_02_m.png" alt="" class="only_m">
                </div>
                
                <ul class="top_line">
                    <li>
                        <h3><span>·</span> 차별화된 컨설팅 역량</h3>
                        <!-- BEGIN 2022 04 26 텍스트 수정  --> 
                        <p>BizTech는 제조, 서비스, 패션/리테일, 유통/물류, 교육 분야 등 다양한 산업별로 차별화된 컨설팅 역량을 바탕으로 다양한 IT컨설팅 서비스를 제공합니다.</p>
                        <!-- END 2022 04 26 텍스트 수정  -->  
                    </li>
                    <li>
                        <h3><span>·</span> 고객의 Value를 창출하는 다양한 IT컨설팅 서비스 제공</h3>
                        <p>비즈니스 전략 수립, 프로세스 혁신, 최신 IT솔루션 도입 등 고객의 Value를 창출하는 다양한 IT컨설팅 서비스를 제공하고 있습니다.</p>
                    </li>
                </ul>
            </div>

            
            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>PI/ISP</span> Service</h3>
                </div>
                <p class="desc_02">
                비즈테크파트너스는 다수의 프로젝트 수행을 통해 축적된 컨설팅 역량을 바탕으로 <br class="only_w">
                고객사의 비전 및 목표 달성을 위해 비즈니스 전략과 Alignment된 <span>PI/ISP 서비스</span>를 제공하고 있습니다.
                </p>
                <div class="bor_img">
                    <div class="dot_wrap">
                        <strong><span>정보 전략</span> 계획 (ISP)</strong>
                    </div>
                    <p class="desc_02">ISP (Information Strategy Planning) 컨설팅 수행을 통한 비즈니스 전략과 Alignment된 중장기 IT 로드맵 수립</p>
                    <div class="img_wrap show">
                        <img src="../img/sub/pl_03_m.jpg" alt="" class="only_m"> 
                        <img src="../img/sub/pl_03.png" alt="" class="only_w">
                    </div>
                </div>
                    
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>PI</span> 방법론</h3>
                </div>
                <p class="desc_02">
                체계적인 PI 활동을 통해 ERP 시스템 구축의 완성도를 제고하고, 궁극적으로  <span>사업성과</span>를 <span>극대화</span>하도록 합니다.
                </p>
                <div class="img_wrap">
                    <img src="../img/sub/pl_04_m.png" alt="" class="only_m">
                    <img src="../img/sub/pl_04.png" alt="" class="only_w">
                    
                </div>
                
            </div>


            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>PI/ISP</span> Service</h3>
                </div>
                <p class="desc_02">고객의 내/외부 환경을 분석하여 IT 비전 및 방향성을 제시하고 업무 영역별 As-Is 분석, ERP Coverage 분석 등을 통해 <br>통합정보시스템의 To-Be 모델 설계 및 ERP 추진을 위한 <span>구체적인 조직, 이행일정</span> 및  <span>예상 투자금액</span> 등을 제시하는 서비스 입니다.</p>
                <div class="dot_wrap top_off">
                    <strong> Approach & Framework</strong>
                </div>
                <div class="img_wrap show">
                    <img src="../img/sub/pl_05_m.png" alt="" class="only_m">
                    <img src="../img/sub/pl_05.png" alt="" class="only_w">
                </div>
            

                <div class="dot_wrap top_off">
                    <strong>주요 Activity</strong>
                </div>

                <ul class="line_asome ">
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/pl_06_m.png" alt="" class="only_m">
                            <img src="../img/sub/pl_06.png" alt="" class="only_w">
                        </div>
                        <p>·  임원, 현업 담당자 Interview</p>
                        <p>·  내/외부 자료 조사 및 분석</p>
                        <p>·  IT 전략 수립</p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/pl_07_m.png" alt="" class="only_m">
                            <img src="../img/sub/pl_07.png" alt="" class="only_w">
                        </div>
                        <p>·  현업 담당자 Interview</p>
                        <p>·  현재 프로세스/시스템 분석</p>
                        <p>·  핵심성공요소 및</p>
                        <p>·  관련 성과지표 도출</p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/pl_08_m.png" alt="" class="only_m">
                            <img src="../img/sub/pl_08.png" alt="" class="only_w">
                        </div>
                        <p>·  Best Practice 및 IT 동향 분석</p>
                        <p>·  핵심업무 프로세스에 대한 <br>향후 이미지 도출</p>
                        <p>·  투자 대비 효과 분석</p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/pl_09_m.png" alt="" class="only_m">
                            <img src="../img/sub/pl_09.png" alt="" class="only_w">
                        </div>
                        <p>·  과제별 이행 우선순위 설정</p>
                        <p>·  ERP 도입 타당성 검토 (Option)</p>
                        <p>·  통합 과제 이행 및</p>
                        <p>·  투자 계획 수립</p>
                    </li>
                </ul>
            </div>

           

            

           
            
           

        </div>

    </div> <!-- inner -->
     
    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="">
            </div>
           
		</div>
	</div>
     


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>