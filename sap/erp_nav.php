<ul class="sub_gnb">
    <li class='on'><a href="../sap/implementation.php">SAP</a></li>
    <li ><a href="../web/si_sm.php">WEB</a></li>
    <li  ><a href="../bpo/bpo_outline.php">BPO</a></li>
</ul>


<div class="sub_nav_wrap">
    <button type="button" class="now_btn only_m"></button>
    <ul class="sub_nav">
        <li data-subnav="erp_01"><a href="/sap/sap_consulting.php">SAP PI/ISP Consulting</a></li>
        <li data-subnav="erp_02"><a href="/sap/cloud.php">Cloud</a></li>
        <li data-subnav="erp_03"><a href="/sap/implementation.php">SAP Implementation​</a></li>
        <li data-subnav="erp_04"><a href="/sap/biz_care.php">SAP Maintenance​</a></li>
    </ul>
</div>


<script>
   
    $(function () {

        var detph = $('#container').data('subnav');
        var nowTxt = $('.sub_nav li[data-subnav="' + detph + '"]').text();
        $('.sub_nav li[data-subnav="' + detph + '"]').addClass('on');
        $('.sub_nav_wrap .now_btn').text(nowTxt);

        $('.sub_nav_wrap .now_btn').on('click', function() {
            $('.now_btn').toggleClass('on')
            $('.sub_nav').toggle();
        })
        
    });




</script>

