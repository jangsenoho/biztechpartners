<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub erp imple" data-depth="erp" data-menu="erp_03" data-subnav="erp_03">
    <div class="inner_1200">

      <?php include($_SERVER['DOCUMENT_ROOT'].'/sap/erp_nav.php');?>

        <div class="sub_cont" data-depth="imple" data-menu="imple_02" data-imnav="imple_02">

            <!-- <h2 class="ttl ttl_02">SAP Implementation​</h2> -->
          <?php include($_SERVER['DOCUMENT_ROOT'].'/sap/im_nav.php');?>

            <div class="ttl_box mar_no">
                <a href="../pdf_down/02_SAP S4HANA Conversion(1).pdf" class="down" download>SAP S/4HANA Conversion Brochure  <span>PDF Download</span> <i></i></a>
                <h2 class="ttl">SAP S/4HANA Conversion</h2>
                <span class="line"></span>
                <p class="desc_02">
                    BizTech는 효율적인 SAP S/4HANA Conversion 노하우와 최신 버전 경험을 기반으로
                    <span>고객 자원의 최적화 전환 전략을 활용하여 최상의 서비스를 제공</span>합니다.
                </p>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>SAP S/4HANA Conversion</span> 사전 요건</h3>
                </div>
                <div class="img_wrap show">
                    <img src="../img/sub/sap_im_08_m.jpg" alt="사전요건" class="only_m">
                    <img src="../img/sub/sap_im_08.png" alt="사전요건" class="only_w">
                </div>
            </div>
 
            <div class="cont_box top_m">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>SAP S/4HANA Conversion</span> 방법론</h3>
                </div>
                <div class="tabel_wrap only_w">
                    <table>
                        <colgroup>
                            <col width="20%">
                            <col width="25%">
                            <col width="18%">
                            <col width="*">
                        </colgroup>

                        <thead>
                            <tr>
                                <th>Type</th>
                                <th>Approach</th>
                                <th>Available for</th>
                                <th>Purpose</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Brownfield ‘Reusing’</td>
                                <td>System Conversion <br>
                                    (Tool : Software Update Manager)
                                </td>
                                <td>SAP ERP System</td>
                                <td>
                                    <ul>
                                        <li><i>·</i> New platform + 기존 ERP Data 및 Business Process 전환</li>
                                        <li><i>·</i> 기존 SAP ERP 시스템에서 SAP Migration Tool을 사용하여 S/4HANA로 Conversion 하는 방식</li>
                                        <li><i>·</i> Down-Time이 발생되며, 기술적 난이도가 복잡함</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td>Greenfield ‘Reengineering’</td>
                                <td>New Implementation <br>
                                    (Tool:Software Update Manager)
                                </td>
                                <td>SAP ERP or <br>
                                    third-party <br>
                                    system(s)
                                </td>
                                <td>
                                    <ul>
                                        <li><i>·</i> New Implementation – 기존 Master 자료는 전환됨</li>
                                        <li><i>·</i> 신규 고객 또는 S/4HANA Migration Cockpit을 이용하여 기존 SAP ERP 시스템에서 데이터를 추출하여 S/4HANA 시스템에 적재</li>
                                        <li><i>·</i> Reengineering을 통한 신규 프로세스 및 Simplifications 기준 정보로 개편할 수 있음</li>
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="tabel_wrap only_m">
                    <table>
                        <colgroup>
                            <col width="3%">
                            <col width="30%">
                            <col width="30%">
                        </colgroup>

                        <thead>
                            <tr>
                                <th>Type</th>
                                <th>Brownfield ‘Reusing’</th>
                                <th>Greenfield ‘Reengineering’</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Approach</td>
                                <td>System Conversion <br>
                                    (Tool : Software Update Manager)
                                </td>
                                <td>New Implementation <br>
                                    (Tool:Software Update Manager)
                                </td>
                            </tr>
                            <tr>
                                <td>Available for</td>
                                <td>SAP ERP System</td>
                                <td>SAP ERP or third-party system(s)</td>
                            </tr>
                            <tr>
                                <td>Purpose</td>
                                <td>
                                    <ul>
                                        <li><i>·</i> New platform + 기존 ERP Data 및 Business Process 전환</li>
                                        <li><i>·</i> 기존 SAP ERP 시스템에서 SAP Migration Tool을 사용하여 S/4HANA로 Conversion 하는 방식</li>
                                        <li><i>·</i> Down-Time이 발생되며, 기술적 난이도가 복잡함</li>
                                    </ul>
                                </td>
                                <td>
                                    <ul>
                                        <li><i>·</i> New Implementation – 기존 Master 자료는 전환됨</li>
                                        <li><i>·</i> 신규 고객 또는 S/4HANA Migration Cockpit을 이용하여 기존 SAP ERP 시스템에서 데이터를 추출하여 S/4HANA 시스템에 적재</li>
                                        <li><i>·</i> Reengineering을 통한 신규 프로세스 및 Simplifications 기준 정보로 개편할 수 있음</li>
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                
                
            </div>

            <div class="cont_box">
                <strong class="b red">01. Brown Field 방법론</strong>
                <div class="img_wrap show">
                    <img src="../img/sub/sap_im_09_01._m.jpg" alt="방법론" class="only_m">
                    <img src="../img/sub/sap_im_09_01.png" alt="방법론" class="only_w">
                </div>
            </div>

            <div class="cont_box">
                <strong class="b green">02. Green Field 방법론</strong>
                <div class="img_wrap show">
                    <img src="../img/sub/sap_im_09_02_m.jpg" alt="방법론" class="only_m">
                    <img src="../img/sub/sap_im_09_02.png" alt="방법론" class="only_w">
                </div>
            </div>

            

           
            
           

        </div>

    </div> <!-- inner -->
     
    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="">
            </div>
           
		</div>
	</div>


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>