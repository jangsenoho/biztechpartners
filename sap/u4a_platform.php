<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub erp imple u4a" data-depth="erp" data-menu="erp_03" data-subnav="erp_03">
    <div class="inner_1200">
      <?php include($_SERVER['DOCUMENT_ROOT'].'/sap/erp_nav.php');?>

        <div class="sub_cont" data-depth="imple" data-menu="imple_05" data-imnav="imple_05">

            <!-- <h2 class="ttl ttl_02">SAP Implementation​</h2> -->
          <?php include($_SERVER['DOCUMENT_ROOT'].'/sap/im_nav.php');?>

            <div class="ttl_box mar_bot mar_no">
                <a href="../pdf_down/U4A IDE Platform(0).pdf" class="down" download>U4A IDE Platform Brochure <span>PDF Download</span> <i></i></a>
                <h2 class="ttl">U4A IDE Platform</h2>
                <span class="line"></span>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>U4A IDE</span> PLATFORM ?</h3>
                </div>
                <div class="img_wrap show">
                    <img src="../img/sub/sap_im_15_m.jpg" alt="플랫폼" class="only_m">
                    <img src="../img/sub/sap_im_15.png" alt="플랫폼" class="only_w">
                </div>
            </div>

            
            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>U4A IDE Platform</span> 특징</h3>
                </div>

                <ul class="platform">
                    <li>
                        <p><strong>01. 개발 환경의 일원화 </strong><br class="only_m">(화면 및 비즈니스 Logic 통합 개발환경 제공)</p>
                        <div class="img_wrap show">
                            <img src="../img/sub/sap_im_16_1_m.jpg" alt="특징" class="only_m">
                            <img src="../img/sub/sap_im_16_1.jpg" alt="특징" class="only_w">
                        </div>
                    </li>
                    <li>
                        <p><strong>02. No Interface를 통한 UI5 Application 개발 가능 </strong><br class="only_m">(※ ERP DB 기준 No Interface Web 개발 가능)</p>
                        <div class="img_wrap show">
                            <img src="../img/sub/sap_im_16_2_m.jpg" alt="특징" class="only_m">
                            <img src="../img/sub/sap_im_16_2.jpg" alt="특징"  class="only_w">
                        </div>
                    </li>
                    <li>
                        <p><strong>03. One Application, One Developer 가능 </strong><br class="only_m">(개발 시간 단축 효과)</p>
                        <div class="img_wrap show">
                            <img src="../img/sub/sap_im_16_3_m.jpg" alt="특징" class="only_m">
                            <img src="../img/sub/sap_im_16_3.jpg" alt="특징"  class="only_w">
                        </div>
                    </li>
                    <li>
                        <p><strong>04. 단순(Simple), 직관적인(Intuitive) 개발환경 제공 </strong><br class="only_m">(Toolkit 교육 수강 기간 이틀 소요)</p>
                        <div class="img_wrap show">
                            <img src="../img/sub/sap_im_16_4_m.jpg" alt="특징" class="only_m">
                            <img src="../img/sub/sap_im_16_4.jpg" alt="특징"  class="only_w">
                        </div>
                    </li>
                </ul>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>개발의 생산성</span> <br class="only_m">(Development Productivity)</h3>
                </div>
                <div class="img_wrap">
                    <img src="../img/sub/sap_im_17_m.png" alt="개발 방법" class="only_m">
                    <img src="../img/sub/sap_im_17.png" alt="개발 방법" class="only_w">
                </div>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>U4A IDE</span> Platform 기반 <br class="only_m">다양한 시스템 구축 사례</h3>
                </div>
                <ul class="ico_100w top_bor">
                    <li>
                        <div class="ico_img">
                            <img src="../img/sub/sap_im_18_m.png" alt="구축사례 아이콘" class="only_m" style="max-width: 39px">
                            <img src="../img/sub/sap_im_18.png" alt="구축사례 아이콘" class="only_w">
                        </div>
                        <div class="ico_txt">
                            <p><i>ㆍ</i> 자재창고 재고관리 시스템 (키오스크)</p>
                            <p><i>ㆍ</i> 영업시설물관리 시스템 구축</p>
                            <p><i>ㆍ</i> 경비, 예산관리 시스템 구축</p>
                            <!-- BIGIN: 2022 04 25 텍스트 수정  -->
                            <p><i>ㆍ</i> 자산관리 시스템 구축(모바일 QR Code스캔 방식)</p>
                            <!-- END: 2022 04 25 텍스트 수정  -->
                            <p><i>ㆍ</i> 적하보험 시스템 구축</p>
                            <p><i>ㆍ</i> 창고재고(입출고, 창고) QR(Bar)Cord 시스템 구축(PDA포함)</p>
                            <p><i>ㆍ</i> 구매포탈관리 시스템 구축(대시보드 포함)</p>
                            <p><i>ㆍ</i> ERP 글로벌 관리지표 통합 모니터링 시스템(대시보드 포함) </p>
                            <p><i>ㆍ</i> CRM/PLM/HR 시스템 구축 </p>
                            <p><i>ㆍ</i> 채용관리 시스템 구축(WDA -> UI5 고도화) </p>
                            <p><i>ㆍ</i> 보험사 고객 보험 상품 계약 시스템 프로젝트 (반응형 웹) </p>
                        </div>
                    </li>
               </ul>
            </div>
            

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>U4A IDE</span> Platform Reference</h3>
                </div>
                <div class="img_wrap show">
                    <img src="../img/sub/sap_im_19_m.jpg" alt="" class="only_m">
                    <img src="../img/sub/sap_im_19.png" alt="" class="only_w">
                </div>
                
                
            </div>
 
           

            

           
            
           

        </div>

    </div> <!-- inner -->
     
    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="">
            </div>
           
		</div>
	</div> 


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>