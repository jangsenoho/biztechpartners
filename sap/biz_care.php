<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub erp maintenance​" data-depth="erp" data-menu="erp_04" data-subnav="erp_04">
    <div class="inner_1200">
           <?php include($_SERVER['DOCUMENT_ROOT'].'/sap/erp_nav.php');?>

        <div class="sub_cont" data-depth="imple" data-menu="main_01" data-manav="main_01">

            <!-- <h2 class="ttl ttl_02">SAP Maintenance​</h2> -->
            <? include('./ma_nav.php');?>

            <div class="ttl_box mar_bot">
                <!-- <a href="#" class="down" download>BIZcare <b>3.0</b> <span>PDF Download</span> <i></i></a> -->
                <h2 class="ttl">BIZcare<i>3.0</i></h2>
                <span class="line"></span>
            </div>

            <div class="cont_box">
                <p class="desc_02">Bizcare3.0은 고객과의 실시간 Response 및 철저한 Request 관리가 가능한 <br class="only_w">
                    BizTech의 고객대응 전문 On-Line Helpdesk 솔루션입니다.
                </p>
                <div class="img_box_f">
                    <div class="img_wrap">
                        <img src="../img/sub/sap_im_27_1_m.jpg" 1alt="솔루션 설명" class="only_m">
                        <img src="../img/sub/sap_im_27_1.jpg" 1alt="솔루션 설명" class="only_w">
                    </div>
                    <div class="img_wrap">
                        <img src="../img/sub/sap_im_27_2_m.jpg" 1alt="솔루션 설명" class="only_m">
                        <img src="../img/sub/sap_im_27_2.jpg" alt="솔루션 설명" class="only_w">
                    </div>
                    <div class="img_wrap">
                        <img src="../img/sub/sap_im_27_3_m.jpg" 1alt="솔루션 설명" class="only_m">
                        <img src="../img/sub/sap_im_27_3.jpg" alt="솔루션 설명" class="only_w">
                    </div>
                </div>
             

                <ul class="top_line w1000">
                    <li>
                        <h3><i>01.</i> <span>다양한 서비스</span> 지원</h3>
                        <p>On-line을 통한 업무 프로세스 및 시스템 문의 등 다양한 서비스 지원</p>
                    </li>
                    <li>
                        <h3><i>02.</i> <span>SMS, E-mail</span> 서비스 제공</h3>
                        <p>실시간 Response 지원을 위해 각 단계 별 SMS, E-mail 서비스 제공</p>
                    </li>
                    <li>
                        <h3><i>03.</i> <span>신속한</span> Read Time</h3>
                        <p>신속한 Read Time과 철저한 Request 관리를 위한 미처리 현황 처리</p>
                    </li>
                    <li>
                        <h3><i>04.</i> <span>서비스 통계</span> 분석 지원</h3>
                        <p>Request에 대한 다양한 분석을 통한 서비스 통계 분석 지원</p>
                    </li>
                    
                </ul>
            </div>

            <div class="cont_box"> 
            <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>BIZcare <i>3.0</i></span>System 구성</h3>
                </div>
                <div class="img_wrap show">
                    <img src="../img/sub/sap_im_28.png" alt="시스템 구성">
                </div>
            </div>
            
            <div class="cont_box">
                <div class="bor_ttl_box">
                    <!-- BEGIN 2022 04 26 텍스트 수정  -->  
                    <h3 class="bor_ttl"><span>CSR 처리</span>Process</h3>
                    <!-- END 2022 04 26 텍스트 수정  -->
                </div>
                <div class="img_wrap show">
                    <img src="../img/sub/sap_im_29.png" alt="처리 프로세스">
                </div>

                <div class="prosess_area">
                    <div class="prosess_box">
                        <div class="prosess_ttl">CSR 처리상태</div>
                        <div class="prosess_ttl pro_txt">내 용</div>
                    </div>
                    <div class="prosess_box">
                        <div class="prosess_ttl">문의대기</div>
                        <div class=" pro_txt"><i>ㆍ</i>고객 등록자가 CSR 문의 작성 후 고객 승인자에게 승인을 요청, 대기중인 상태</div>
                    </div>
                    <div class="prosess_box">
                        <div class="prosess_ttl">문의</div>
                        <div class=" pro_txt"><i>ㆍ</i>고객 승인자가 승인 요청 받은 문의대기를 승인, 문의가 등록된 상태(실제 CSR 처리 단위)</div>
                    </div>
                    <div class="prosess_box">
                        <div class="prosess_ttl">접수 후 분석</div>
                        <div class=" pro_txt"><i>ㆍ</i>등록된 문의를 담당자(또는 관리자)가 인지, 문의 분석 중인 상태</div>
                    </div>
                    <div class="prosess_box">
                        <div class="prosess_ttl">분석 후 진행</div>
                        <div class=" pro_txt"><i>ㆍ</i>분석 내용을 기반으로 문의 최종 답변 등록을 위해 소요되는 예상 기간과 공수를 입력</div>
                    </div>
                    <div class="prosess_box">
                        <div class="prosess_ttl">답변 진행</div>
                        <div class=" pro_txt"><i>ㆍ</i>관리자가 승인 요청 받은 답변대기를 승인, 답변의 일부(%) 등록된 상태</div>
                    </div>
                    <div class="prosess_box">
                        <div class="prosess_ttl">답변 완료</div>
                        <div class=" pro_txt"><i>ㆍ</i>관리자가 승인 요청 받은 답변대기를 승인, 답변이 100% 등록된 상태</div>
                    </div>
                    <div class="prosess_box">
                        <div class="prosess_ttl">재문의</div>
                        <div class=" pro_txt"><i>ㆍ</i>고객 등록자(또는 승인자)가 답변이 등록된 문의에 대하여 추가 문의를 등록한 상태</div>
                    </div>
                    <div class="prosess_box">
                        <div class="prosess_ttl">종결</div>
                        <div class=" pro_txt"><i>ㆍ</i>등록된 답변에 대하여 평가를 완료한 상태로 추가 답변 등록 불가</div>
                    </div>
                </div>
            </div>


        </div>

    </div> <!-- inner -->


    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="">
            </div>
           
		</div>
	</div>
     

</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
    
</script>