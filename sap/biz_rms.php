<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub erp maintenance​" data-depth="erp" data-menu="erp_04" data-subnav="erp_04">
    <div class="inner_1200">

      <?php include($_SERVER['DOCUMENT_ROOT'].'/sap/erp_nav.php');?>

        <div class="sub_cont" data-depth="imple" data-menu="main_02" data-manav="main_02">

            <!-- <h2 class="ttl ttl_02">SAP Maintenance​</h2> -->
            <? include('./ma_nav.php');?>

            <div class="ttl_box mar_bot mar_no">
                <a href="../pdf_down/Biz-RMS.pdf" class="down" download>Biz-RMS<span>PDF Download</span> <i></i></a>
                <h2 class="ttl">BIz-RMS</h2>
                <span class="line"></span>
                <p class="desc_02">
                BIz-RMS (Resource Management Solution) 는 SAP ERP 프로그램 <br class="only_w">
                <span>성능</span> 및 <span>사용 진단 솔루션</span> 입니다.
                </p>
            </div>

            <div class="cont_box">
                <div class="text_img_area">
                    <div class="list_asome">
                        <p><strong>01<span>.</span></strong> 귀사는 SAP ERP 프로그램 성능에 만족하십니까?</p>
                        <p><strong>02<span>.</span></strong> 정량적 측정치가 없어 대상선정에 어려움이 있으십니까?</p>
                        <p><strong>03<span>.</span></strong> 성능개선을 하고 싶으나 비용적인 문제로 고민하고 계십니까?</p>
                    </div>
                    <div class="img_wrap">
                        <img src="../img/sub/sap_im_22_m.png" alt="솔루션" class="only_m" style="max-width:65% ">
                        <img src="../img/sub/sap_im_22.png" alt="솔루션" class="only_w">
                    </div>
                </div>
                
               
            </div>

            <div class="cont_box">
                <div class="dot_wrap">
                    <strong><span>성능 개선</span>의 필요성</strong>
                </div>
                <ul class="gray_asome">
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/sap_im_23.png" alt="" class="only_w">
                            <img src="../img/sub/sap_im_23_m.png" alt="" class="only_m" style="max-width:54px ">
                        </div>
                        <strong><span>IT</span> 비용 절감 </strong>
                        <p><i>·</i>  H/W 투자대비 효율증대</p>
                        <p><i>·</i>  시스템 운영 Resource 절감</p>
                        <p><i>·</i>  효율적 투자 기회 창출</p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/sap_im_24.png" alt="" class="only_w">
                            <img src="../img/sub/sap_im_24_m.png" alt="" class="only_m" style="max-width:55px ">
                        </div>
                        <strong><span>Data Resource</span><br class="only_m"> 효율성 증대 </strong>
                        <p><i>·</i>  Program performances Optimize</p>
                        <p><i>·</i>  미사용 프로그램 활용도 제고</p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/sap_im_25.png" alt="" class="only_w">
                            <img src="../img/sub/sap_im_25_m.png" alt="" class="only_m" style="max-width:37px ">
                        </div>
                        <strong><span>업무</span> 생산성 증대 </strong>
                        <p><i>·</i>  업무 속도 향상</p>
                        <p><i>·</i>  Out put 대기 시간 감소</p>
                    </li>
                </ul>

            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>Biz-RMS</span> 진단 STEP</h3>
                </div>
                <ul class="ico_w25">
                    <li>
                        <div class="color_wrap">
                            <span class="wh"><div><i>01</i> 사전준비 및 <br> Tool 점검</div></span>
                        </div>
                        <p>Biz-RMS Tool <br> <span>설치 및 테스트</span></p>
                    </li>
                    <li>
                        <div class="color_wrap">
                            <span class="pi" style=" background:#c00c3f "><div><i>02</i> 데이터 집계 및 <br>현황 분석</div></span>
                        </div>
                        <p>Real Data<br> <span>집계</span></p>
                    </li>
                    <li>
                        <div class="color_wrap">
                            <span class="gr" style=" background:#b0b0b0 "><div><i>03</i> 진단 결과 및 <br>개선 항목 도출</div></span>
                        </div>
                        <p>현황 분석<br> <span>결과 작성</span></p>
                    </li>
                    <li>
                        <div class="color_wrap">
                            <span class="dgr" style=" background:#707070 "><div><i>04</i> 개선안<br>수립</div></span>
                        </div>
                        
                        <p>일정 수립 및<br> <span>개선 항목 선정</span></p>
                    </li>
                </ul>

                
            </div>
          
            
            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>개발 프로그램</span>성능 측정</h3>
                </div>
                <p class="desc_02">Biz-RMS(Resource Management Solution)를 통해 <br class="only_m"><span>개발 프로그램들의 성능을 측정</span>해 드립니다.</p>
                <div class="img_wrap">
                    <img src="../img/sub/sap_im_26_m.png" alt="성능측정" class="only_m">
                    <img src="../img/sub/sap_im_26.png" alt="성능측정"  class="only_w">
                </div>
                <div class="img_wrap modal_ show">
                    <img src="../img/sub/sap_im_26__1_m.png" alt="성능측정" class="only_m">
                </div>

                
            </div>


        </div>

    </div> <!-- inner -->

    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="">
            </div>
           
		</div>
	</div> 



</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>