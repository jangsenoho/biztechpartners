<ul class="sub_nav_02">
    <li data-imnav="imple_01"><a href="/sap/implementation.php">Exprism ERP</a></li>
    <li data-imnav="imple_02"><a href="/sap/sap_conversion.php">SAP S/4HANA Conversion</a></li>
      <!-- BEGIN 2022 04 26 텍스트 수정  -->  
    <li data-imnav="imple_03"><a href="/sap/biz_res.php">BIZ-RMS</a></li>
    <!-- END 2022 04 26 텍스트 수정  -->  
    <li data-imnav="imple_04"><a href="/sap/biz_yts.php">Biz-YTS​</a></li>
    <li data-imnav="imple_05"><a href="/sap/u4a_platform.php">U4A IDE Platform​</a></li>
    <li data-imnav="imple_06"><a href="/sap/sap_bw.php">SAP BW​</a></li>
    <li data-imnav="imple_07"><a href="/sap/next_hr.php">NEXT-HR​</a></li>

</ul>

<script>
   
    $(function () {

        var detph = $('.sub_cont').data('imnav');
        var nowTxt = $('.sub_nav_02 li[data-imnav="' + detph + '"]').text();
        $('.sub_nav_02 li[data-imnav="' + detph + '"]').addClass('on');
        // $('.sub_nav_row .now_btn').text(nowTxt);

    });




</script>