<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub erp imple" data-depth="erp" data-menu="erp_03" data-subnav="erp_03">
    <div class="inner_1200">

      <?php include($_SERVER['DOCUMENT_ROOT'].'/sap/erp_nav.php');?>

        <div class="sub_cont" data-depth="imple" data-menu="imple_06" data-imnav="imple_06">

            <!-- <h2 class="ttl ttl_02">SAP BW​</h2> -->
          <?php include($_SERVER['DOCUMENT_ROOT'].'/sap/im_nav.php');?>

            <div class="ttl_box mar_bot">
                <!-- <a href="#" class="down">U4A IDE Platform Brochure <span>PDF Download</span> <i></i></a> -->
                <h2 class="ttl">SAP BW</h2>
                <span class="line"></span>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>What</span> Is BW/4HANA ?</h3>
                </div>
                <ul class="top_line m_line">
                    <li>
                        <h3><i>01.</i> <strong><span>차세대 기술</span> 플랫폼</strong></h3>
                        <p>SAP의 HANA In-Memory Database를 사용하는 SAP BW on HANA의 차세대 기술 플랫폼</p>
                    </li>
                    <li>
                        <h3><i>02.</i> <strong><span>고객의 TCO 절감의</span> 효과를 실현</strong></h3>
                        <p>BW on HANA에서 실행되어 결과는 향상된 속도와 성능, 간편한 관리를 통해 고객의 TCO 절감의 효과를 실현</p>
                    </li>
                    <li>
                        <h3><i>03.</i> <strong><span>SAP HANA</span>  기반으로 하는 패키지 데이터 웨어하우스</strong></h3>
                        <p>SAP 및 비 SAP 어플리케이션 및 데이터 소스에 연결하여 데이터 통합 비용 절감 프로세스를 추출, 변환 및 로드</p>
                    </li>
                    <li>
                        <h3><i>04.</i> <strong><span>최적의 기업 분석</span> 환경을 구현</strong></h3>
                        <p>SAP HANA와 BW의 결합으로 기존 일반적인 관계형 데이터베이스 보다 훨씬 빠르게 분석 업무를 수행하며 최적의 기업 분석환경을 구현</p>
                    </li>
                    
                </ul>
            </div>

            <div class="cont_box"> 
                <div  class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>Benefit</span> of BW/4HANA </h3>
                </div>
                <div class="img_wrap">
                    <img src="../img/sub/bw_001.png" alt="베네핏" class=" only_w">
                    <img src="../img/sub/bw_001_m.png" alt="베네핏"  class="only_m">
                </div>
            </div>
            
            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>History</span> of BW/4HANA </h3>
                </div>
                <div class="img_wrap show">
                    <img src="../img/sub/sap_im_21_m.png" alt="베네핏" class="only_m">
                    <img src="../img/sub/sap_im_21.png" alt="베네핏 " class="only_w">
                </div>
            </div>


        </div>

    </div> <!-- inner -->

    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box"> 
                <img src="" alt="">
            </div>
           
		</div>
	</div>
     

</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>