<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>
<section id="container" class="sub business " data-depth="business" data-menu="bus_01" data-subnav="bus_01" >
    <div class="inner_1200">
            <!-- <? include('./bus_nav.php');?> -->

            <!-- <ul class="sub_gnb">
                <li class='on'><a href="/business/business_question.php">사업문의</a></li></li>
                <li ><a href="/business/business_customer01.php">고객사</a></li>
            </ul> -->


        <div class="sub_cont" >
            
            <!-- <h2 class="ttl ttl_02">사업문의</h2> -->
            <!-- <ul class="sub_nav_02">
                <li class="on"><a href="/business/business_question.php">사업문의</a></li>
            </ul> -->
            <div class="ttl_box ">
                <h2 class="ttl">고객문의</h2>
                <span class="line"></span>
                <p class="desc_02">Biztech에 <span>궁금하신 사항이나 제안</span> 등 문의해 주시면 신속한 답변을 해드리겠습니다. <br> 정보통신망이용촉진 및 정보보호 등에 관한 법률에 따라 고객문의를 통해 수집한 고객정보의 취급에 대해 고지하며 동의를 구하고 있습니다.​</p>
            </div>


            <div class="cont_box">
                <form action="">
                    <div class="agree_area">
                    <div class="agree_txt">
                            <span>개인정보의 수집 및 이용 안내</span>
                            <div>㈜비즈테크파트너스(이하“회사”)는 아래와 같이 개인정보의 수집·이용에 관한 내용을 관계 법령에 따라 고지하오니 <br>동의해주시기 바랍니다.</div>

                            <div>
                                <strong>(1). 개인정보 수집 항목</strong>
                                - 입력항목 : 이름, 회사명, 직급, 연락처, 이메일, 제목, 문의내용 <br>
                                - 자동수집항목 : IP Address, 서비스 이용기록 <br>
                            </div>
                            <div>
                                <strong>(2). 개인정보 수집 및 이용 목적</strong>
                                - 회사관련 문의, 서비스 문의, 투자 문의, 채용 문의 등에 대한 정보 제공 및 안내
                            </div>
                            <div>
                                <strong>(3). 개인정보 보유 및 이용 기간</strong>
                                - 수집한 개인정보는 문의내역 관리 및 응대를 위해 12개월간 보유하며, 이후 모든 정보는 파기합니다.<br>
                                  (단, 성명 및 이메일은 지속적인 관리를 위해 36개월간 보관).<br>

                            </div>
                            <div>
                            <!--BEGIN: 2022 04 25 텍스트 수정 -->
                                <strong>(4). 신청인은 개인정보 수집 및 이용과 관련하여 동의를 거부할 권리가 있으나, 동의거부 시 문의를 위한 글 작성에 제약사항이 발생할 수 있습니다.</strong>
                            <!--END: 2022 04 25 텍스트 수정 -->
                            </div>

                        </div>
                            <div class="check_box">
                                <input type="checkbox" name="agree" id="agree" value="">
                                <!--BEGIN: 2022 04 26 텍스트 수정 -->
                                <label for="agree">개인정보 수집 및 이용 안내에 동의합니다.</label>
                                <!--END: 2022 04 26 텍스트 수정 -->
                            </div>
                        </div>

                    </div>

                    <p class="question_ps"><span>*</span>는 필수 입력 항목입니다.</p>
                    <ul class="form_box">
                        <li>
                            <div class="form_ttl">구분<span> *</span></div>
                            <div class="form_wrap">
                                <select name="list_ttl">
                                    <option value="">선택</option>
                                    <option value="">ERP</option>
                                    <option value="">RPA</option>
                                    <option value="">기타</option>
                                </select>
                            </div>
                        </li>
                        <li>
                            <div class="form_ttl">이름<span> *</span></div>
                            <div class="form_wrap">
                                <input type="text" name="product_name" id="product_name" value="">
                            </div>
                        </li>
                        <li>
                            <div class="form_ttl">회사명<span> *</span></div>
                            <div class="form_wrap">
                                <input type="text" name="com_name" id="com_name" value="">
                            </div>
                        </li>
                        <li>
                            <div class="form_ttl">직급</div>
                            <div class="form_wrap">
                                <input type="text" name="com_rank" id="com_rank" value="">
                            </div>
                        </li>
                        <li>
                            <div class="form_ttl">연락처<span> *</span></div>
                            <div class="form_wrap">
                                <!--BEGIN: 2022 04 26 placeholder 수정 -->
                                <input type="text" name="user_tel" id="user_tel" value="" placeholder="'-'없이 입력해주세요.">
                                  <!--END: 2022 04 26 placeholder 수정 -->
                            </div>
                        </li>
                        <li>
                            <div class="form_ttl">이메일<span> *</span></div>
                            <div class="form_wrap">
                                <input type="text" name="user_email" id="user_email" value="">
                            </div>
                        </li>
                       
                        <li>
                            <div class="form_ttl">제목<span> *</span></div>
                            <div class="form_wrap txt_wrap ">
                                <input type="text" name="notice_ttl" id="notice_ttl" value="">
                            </div>
                        </li>
                        <li>
                            <div class="form_ttl">문의내용<span> *</span></div>
                            <div class="form_wrap ">
                                <textarea name="txt_area" id="txt_area" cols="30" rows="10"></textarea>
                            </div>
                        </li>
                        
                    </ul>

                    <div class="btn_box">
                        <button type="submit" class="btn_point_full">확인</button>
                        <button type="reset" class="btn_gray_line" onclick="alert('입력하신 내용은 저장되지 않습니다.');">취소</button>
                    </div>

                </form>
                


            
            </div>

         
        </div>
    </div>


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(3).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('bus')
    });
</script>