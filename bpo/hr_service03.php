<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub bpo  hr" data-depth="bpo" data-menu="bpo_02" data-subnav="bpo_02">
    <div class="inner_1200">

           <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/bpo_nav.php');?>

        <div class="sub_cont" data-depth="imple" data-menu="hr_03" data-hrnav="hr_03">

            <!-- <h2 class="ttl ttl_02">HR BPO Service</h2> -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/hr_nav.php');?> 

            <div class="ttl_box mar_ov m50">
                <h2 class="ttl">교육운영</h2>
                <span class="line"></span>
                <p class="desc_02">교육운영서비스는 직무교육을 실시하는 기업의 <span>교육운영 <br class="only_m">및 교육행정 전반의 교육 운영/위탁서비스</span>를 제공합니다.</p>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>교육 운영 · 위탁</span> Service</h3>
                </div>
                <ul class="gray_asome_02 ">
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/bpo_hr_13.png" alt="" class="only_w">
                            <img src="../img/sub/bpo_hr_13_m.png" alt="" class="only_m" style="max-width:51px ">
                        </div>
                        <strong>집합 과정 운영</strong>
                        <p>·  교육 사전 준비
                            <span>
                                - 교육 대상자 관리 <br>
                                - 교육 참가 안내<br>
                                - 강의실 운영 준비
                            </span>
                        </p>
                        <p>·  강의실 세팅</p>
                        <p>·  교육과정 Opening</p>
                        <p>·  교육 설문 조사</p>
                        <p>·  교육 이수 처리</p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/bpo_hr_14.png" alt="" class="only_w">
                            <img src="../img/sub/bpo_hr_14_m.png" alt="" class="only_m" style="max-width:61px ">
                        </div>
                        <strong>온라인 과정 운영</strong>
                        <p>·  온라인 공부방 운영
                            <span>
                                - 교육 과정 개설  <br>
                                - 교육 대상자 관리  <br>
                                - 교육 설문 조사  <br>
                                - 교육 이수 처리
                            </span>
                        </p>
                        <p>·  실시간 라이브 학습 운영
                            <span>
                                - 스튜디오 환경 세팅  <br>
                                - 사전 리허설  <br>
                                - 라이브 송출  <br>
                                - 채팅방 운영  <br>
                                - QA문의응대
                            </span>
                        </p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/bpo_hr_15.png" alt="" class="only_w">
                            <img src="../img/sub/bpo_hr_15_m.png" alt="" class="only_m" style="max-width:34px ">
                        </div>
                        <strong>교육행정 관리</strong>
                        <p>·  고용보험 신고관리</p>
                        <p>·  교육비용 정산관리</p>
                        <p>·  어학과정 운영관리</p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/bpo_hr_16.png" alt="" class="only_w">
                            <img src="../img/sub/bpo_hr_16_m.png" alt="" class="only_m" style="max-width:41px ">
                        </div>
                        <strong>강의 촬영 및 편집</strong>
                        <p>·  강의 및 인터뷰 촬영</p>
                        <p>·  온라인 교육 영상 편집</p>
                        <p>·  교육과정 홍보 영상 제작</p>
                    </li>
                </ul>
            </div>

            



                
        </div>
            
            

    </div> <!-- inner -->
     
   


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>