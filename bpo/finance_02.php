<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub  bpo finance finance_02" data-depth="bpo" data-menu="bpo_04" data-subnav="bpo_04">
    <div class="inner_1200">

           <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/bpo_nav.php');?>

        <div class="sub_cont" data-depth="finance" data-menu="fin_02" data-finnav="fin_02">

            <!-- <h2 class="ttl ttl_02">재무 BPO Service</h2> -->
            <? include('./fin_nav.php');?> 

            <div class="ttl_box mar_ov m0">
                <h2 class="ttl">재무·관리 결산/ 회계컨설팅</h2>
                <span class="line"></span>
            </div>

            <div class="cont_box">
                 <div class="bor_ttl_box">
                    <h3 class="bor_ttl">재무·관리 결산 <span>BPO</span></h3>
                </div>
                <p class="desc_02">기업의 특성에 맞는 <span>결산 사전 점검과 해당 부서별 마감절차를 통해 결산을 확정</span>하고,<br class="only_w">재무제표 작성 서비스를 제공합니다.</p>

                <!-- 재무관리 3개 박스 -->
                <ul class="gray_asome">
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/bpo_service_11.png" alt="" class="only_w">
                            <img src="../img/sub/bpo_service_11_m.png" alt="" class="only_m" style="max-width: 48px">
                        </div>
                        <strong>결산 사전준비/점검</strong>
                        <p> <i>·</i>  결산 일정 Check</p>
                        <p> <i>·</i>  부서별 거래 내역 취합</p>
                        <p> <i>·</i> 전월/분·반기 실적에 대한 거래 항목 검토
                            <span>- 매출/매입세금계산서 대사</span>
                        </p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/bpo_service_12.png" alt="" class="only_w">
                            <img src="../img/sub/bpo_service_12_m.png" alt="" class="only_m" style="max-width: 49px">
                        </div>
                        <strong>결산 마감/수행</strong>
                        <p>
                            <i>·</i>  결산 계정항목 입력 및 마감
                            <span>-고정자산(감가상각비) <br>
                                - 선급/미지급 비용 <br>
                                - 재고자산 대체 등
                            </span>
                        </p>
                        <p>
                            <i>·</i>  손익 귀속 Cut-off
                            <span>- 결산 수정분개 반영</span>
                        </p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/bpo_service_13.png" alt="" class="only_w">
                            <img src="../img/sub/bpo_service_13_m.png" alt="" class="only_m" style="max-width: 50px">
                        </div>
                        <strong>주석/재무제표 작성</strong>
                        <p><i>·</i>  BS 계정 명세서 작성</p>
                        <p><i>·</i> 기준서 변동에 따른 주석 및 <br>재무제표(IS/BS) 작성</p>
                    </li>
                </ul>
            </div>
            <!-- -----------------------------------------------//재무 관리 3개 박스 -->

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl">회계컨설팅 <span>BPO</span></h3>
                </div>
                <p class="desc_02">기업이 기존의 재무 관련 업무 프로세스를 <span>외부감사기준 및 K-IFRS 회계기준에 맞는 자체결산시스템을 구축</span>할 수 있도록 <br class="only_w"><span>내부 회계 업무 역량을 강화</span>시켜드립니다.</p>
                <ul class="ico_s_100w">
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image:url(../img/sub/bpo_service_08.jpg)" class="only_w"></span> 
                            <span style="background-image:url(../img/sub/bpo_service_08_m.jpg)" class="only_m"></span> 
                        </div>
                        <div class="ico_desc">
                            <strong>재무회계 결산 프로세스 구축 자문</strong>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image:url(../img/sub/bpo_service_09.jpg)" class="only_w"></span> 
                            <span style="background-image:url(../img/sub/bpo_service_09_m.jpg)" class="only_m"></span> 
                        </div>
                        <div class="ico_desc">
                            <strong>추정 재무제표 및 아웃룩 작성 가이드 및 교육</strong>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image:url(../img/sub/bpo_service_10.jpg)" class="only_w"></span> 
                            <span style="background-image:url(../img/sub/bpo_service_10_m.jpg)" class="only_m"></span> 
                        </div>
                        <div class="ico_desc">
                            <strong>재무 업무 처리 기준 보안 및 매뉴얼 구비</strong>
                            <p><i> · </i> 비용 처리 기준, 매출채권 및 자금관리 규정 마련</p>
                            <p><i> · </i>결산 업무 처리 매뉴얼 </p>
                        </div>
                    </li>
                   
                </ul>
            </div>

            
            


            

            

                
        </div>
            
            

    </div> <!-- inner -->
     
   


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>