<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub bpo outline" data-depth="bpo" data-menu="bpo_01" data-subnav="bpo_01">
    <div class="inner_1200">

           <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/bpo_nav.php');?>

        <div class="sub_cont">

            <div class="ttl_box mar_no">
                <a href="#" class="down">고객사용 비즈테크 BPO Brochure <span>Download</span> <i></i></a>
                <h2 class="ttl">BPO 개요</h2>
                <span class="line"></span>
                <div class="gray_box w1000">
                    <p class="text">진정한 파트너십을 통해 <span>고객의 업무혁신을 지원</span>합니다.</p>
                </div>
                <p class="desc_02"><strong>BPO(Business Process Outsourcing)사업부</strong>는 고객이 핵심비즈니스에 집중할 수 있도록 HR(급여, 복리후생 등)서비스, 교육운영, <br class="only_w">회계 및 결산지원 등 다양한 경영지원 업무를 전문적으로 수행함으로써 고객의 성공을 지원하고 있습니다. ​</p>
            </div>

            <!-- 서비스 영역 -->
            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>주요</span> 서비스 영역 </h3>
                </div>
                <!-- <p class="desc_02">(세부적인 내용은 서비스별 상세 메뉴 참조)</p> -->
                <div class="img_box_f">
                    <div class="img_wrap">
                        <img src="../img/sub/out_01.jpg" 1alt="솔루션 설명" class="only_w">
                        <img src="../img/sub/out_01_m.png   " 1alt="솔루션 설명" class="only_m">
                    </div>
                    <div class="img_wrap">
                        <img src="../img/sub/out_02.jpg" alt="솔루션 설명" class="only_w">
                        <img src="../img/sub/out_02_m.png   " 1alt="솔루션 설명" class="only_m">
                    </div>
                    <div class="img_wrap">
                        <img src="../img/sub/out_03.jpg" alt="솔루션 설명" class="only_w">
                        <img src="../img/sub/out_03_m.png   " 1alt="솔루션 설명" class="only_m">
                    </div>
                    <div class="img_wrap">
                        <img src="../img/sub/out_04.jpg" alt="솔루션 설명" class="only_w">
                        <img src="../img/sub/out_04_m.png   " 1alt="솔루션 설명" class="only_m">
                    </div>
                    
                </div>
            </div>
            <!--  ------------------------------------------- //서비스 영역 -->

            <!--  도입효과-->
            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>BPO</span> 도입 효과</h3>
                </div>
                <ul class="ico_s_33w">
                    <li>
                        <span style="background-image: url(../img/sub/out_05.jpg)" class="only_w"></span>
                        <span style="background-image: url(../img/sub/out_05_m.png)" class="only_m"></span>
                        <strong>핵심/본질에 집중</strong>
                        <p><i> · </i> 고객 핵심업무 집중​</p>
                        <p><i> · </i> 새로운 부가가치 창출​</p>
                        <p><i> · </i> 혁신 활동 가속화​​</p>
                    </li>
                    <li>
                    <span  style="background-image:  url(../img/sub/out_06.jpg)" class="only_w"></span>
                    <span style="background-image: url(../img/sub/out_06_m.png)" class="only_m"></span>
                        <strong>업무효율/안정성 증대</strong>
                        <p><i> · </i> 구성원 만족도 증대​​</p>
                        <p><i> · </i> 담당업무 효율성 증가​​​</p>
                        <p><i> · </i>  담당자 변동 리스크 해소​​​</p>
                    </li>
                    <li>
                        <span  style="background-image:  url(../img/sub/out_07.jpg)" class="only_w"></span>
                        <span style="background-image: url(../img/sub/out_07_m.png)" class="only_m"></span>
                        <strong>업무 품질 향상</strong>
                        <p><i> · </i> 일관성 있는 전문성 확보​</p>
                        <p><i> · </i> 서비스별 지표 관리​</p>
                        <p><i> · </i>  업무 개선 외 ​​</p>
                    </li>
                </ul>
            </div>
            <!-- ------------------------------------------------- //  도입 효과 -->

            <!-- 고객사 -->

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>주요</span> 고객사</h3>
                </div>
                <!-- lg그룹사 -->
                <div class="logo_wrap">
                    <ul class="cust_list">
                        <li>
                            <img src="../img/sub/logo_01.png" alt="" class="">
                        </li>
                        <li>
                            <img src="../img/sub/logo_02.png" alt="" class="">
                        </li>
                        <li>
                            <img src="../img/sub/logo_03.png" alt="" class="">
                        </li>
                        <li>
                            <img src="../img/sub/logo_04.png" alt="" class="">
                        </li>
                        <li>
                            <img src="../img/sub/logo_05.png" alt="" class="">
                        </li>
                        <li>
                            <img src="../img/sub/logo_06.png" alt="" class="">
                        </li>
                        <li>
                            <img src="../img/sub/logo_07.png" alt="" class="">
                        </li>
                        <li>
                            <img src="../img/sub/logo_09.png" alt="" class="">
                        </li>
                        <li>
                            <img src="../img/sub/logo_10.png" alt="" class="">
                        </li>
                        <li class="last">
                            <img src="../img/sub/logo_12.png" alt="" class="">
                        </li>
                    </ul>

                </div>
                <!-- ---------------------------------------------------//lg그룹사 -->

                

            </div>

           

          

           

         



            

                
        </div>
            
            

    </div> <!-- inner -->
     
   


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>