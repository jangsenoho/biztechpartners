<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub bpo bpo_table hr" data-depth="bpo" data-menu="bpo_02" data-subnav="bpo_02">
    <div class="inner_1200">

           <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/bpo_nav.php');?>

        <div class="sub_cont" data-depth="imple" data-menu="hr_01" data-hrnav="hr_01">

            <!-- <h2 class="ttl ttl_02">HR BPO Service</h2> -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/hr_nav.php');?> 

            <div class="ttl_box mar_ov">
                <h2 class="ttl">HR Service 개요</h2>
                <span class="line"></span>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl">HR Service <span>개요</span></h3>
                </div>
                <ul class="top_line img_con">
                    <li>
                        <div class="cont_img">
                            <img src="../img/sub/bpo_hr_01.png" alt="" class="only_w">
                            <img src="../img/sub/bpo_hr_01_m.png" alt="" class="only_m">
                        </div>
                        <p>HR 서비스는 <span>2004년 국내 최초로 HR Shared Service Center (HR SSC)를 구축하여, 국내 P&B BPO(Payroll & Benefit Business Process Outsourcing)의 선도적인 역할을 수행</span> 하여 왔습니다. 이후, 전문적인 서비스의 역량과 철저한 보안 관리를 기반으로 하여, 지속적으로 고객사를 확대해 가고 있습니다.</p>
                    </li>
                    <li>
                        <p>서비스 영역에 있어서도 단순히 고객사의 P&B 대행 서비스에 머무르지 않고, P&B 운영의 경험에서 비롯한 실무적인 HR 제도의 운영상 이슈와 대안을 적극적으로 제안하여, 고객사 임직원의 만족도를 높이고, HR부서의 기획역량도 제고하는 실무 기획부서의 역할을 담당하고 있습니다.</p>
                        <div class="cont_img">
                            <img src="../img/sub/bpo_hr_02.png" alt="" class="only_w">
                            <img src="../img/sub/bpo_hr_02_m.png" alt="" class="only_m">
                        </div>
                    </li>
                    <li>
                        <div class="cont_img">
                            <img src="../img/sub/bpo_hr_03.png" alt="" class="only_w">
                            <img src="../img/sub/bpo_hr_03_m.png" alt="" class="only_m">
                        </div>
                        <p>P&B 서비스를 제공하는 고객사와의 신뢰를 바탕으로, 외국인 응대 서비스와 주재원 Tax관련 대응, 임직원의 건강검진 관리 임직원 채용 행정업무 관리와 연수원의 교육운영 분야까지 서비스를 확대하여, 다양한 분야에서 종합적인 서비스를 수행하고 있습니다. <span>HR서비스 임직원 모두는 고객에게 진정한 value를 제공</span>해 드리기 위해, 한결같은 마음으로 끊임없는 노력을 기울이겠습니다.</p>
                    </li>
                </ul>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl">HR Service <span>추구 가치</span></h3>
                </div>
                <ul class="gray_asome shot">
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/bpo_hr_04.png" alt="" class="only_w">
                            <img src="../img/sub/bpo_hr_04_m.png" alt="" class="only_m" style="max-width:62px ">
                        </div>
                        <strong><span>한국 최초의</span> <br>HR Shared Service Center</strong>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/bpo_hr_05.png" alt="" class="only_w">
                            <img src="../img/sub/bpo_hr_05_m.png" alt="" class="only_m" style="max-width:64px ">
                        </div>
                        <strong><span>한국 인사서비스 발전에 기여</span> <br class="only_w">국내 대기업에 사례 전파</strong>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/bpo_hr_06.png" alt="" class="only_w">
                            <img src="../img/sub/bpo_hr_06_m.png" alt="" class="only_m" style="max-width:49px ">
                        </div>
                        <strong><span>고객에게 전문성 및 진정성 높은</span><br class="only_w">HR Service 개발 제공</strong>
                    </li>
                </ul>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl">About <span>HR BPO Service</span></h3>
                </div>
                <ul class="gray_box">
                    <li> 
                        <i>·</i>  
                        <div>기업의 수익창출에는 직접적인 영향을 미치지 않지만, <span>기업 운영에 필수적이고 관련법령과 회사 제반 규정에 대한 전문지식이 필요</span>하므로 전문업체로의 Outsourcing이 확대되고 있습니다.</div>
                    </li>
                    <li>  
                        <i>·</i>  
                        <div>
                            급여/복리후생 업무 Outsourcing으로 고객사는 Business Partner로서 HR 업무 수행에 집중할 수 있으며,  서비스 Quality 향상을 통해 임직원 만족도를  제고할 수 있습니다.
                        </div>
                        
                    </li>
                    <li> 
                        <i>·</i>
                        <!-- BEGIN 2022 04 26 텍스트 수정  -->  
                        <div>
                            비즈테크파트너스는 LG CNS의 자회사로 <span>IT 시스템의 안정적 서비스 제공을 기반으로 하여, 급여/복리후생 BPO서비스를 수행하고</span> 있습니다.
                        </div>
                        <!-- END 2022 04 26 텍스트 수정  -->  
                    </li>
                    <li> 
                        <i>·</i>  
                        <div>
                            전문적인 HR BPO 서비스 노하우를 바탕으로 고객사의 HR 업무 운영에 기여하는 Best Partner가 되기 위해 최선의 노력을 다 할 것입니다.
                        </div>
                        
                    </li>
                </ul>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl">HR Service <span>제공 가치</span></h3>
                </div>
                <ul class="gray_asome">
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/sap_im_23.png" alt="" class="only_w">
                            <img src="../img/sub/sap_im_23_m.png" alt="" class="only_m" style="max-width: 54px">
                        </div>
                        <strong><span>전략적</span> HR업무집중 </strong>
                        <p><i>·</i> HR 전략적 업무 수행 강화</p>
                        <p><i>·</i> HR 인사 서비스 운영 부담완화</p>
                        <p><i>·</i> 인사서비스 지표 관리 <br>(Input cost, Coverage, <br>서비스 만족도 등)</p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/sap_im_24.png" alt="" class="only_w">
                            <img src="../img/sub/sap_im_24_m.png" alt="" class="only_m" style="max-width: 55px">
                        </div>
                        <strong><span>업무</span> 안정성 증대 </strong>
                        <p><i>·</i> 축적된 서비스 역량 활용을 통한 <br>안정적 HR 서비스 제공</p>
                        <p>(업무 담당자 퇴직, 이동 시 발생되는업무 <br>Quality, 역량 저하 방지)</p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/sap_im_25.png" alt="" class="only_w">
                            <img src="../img/sub/sap_im_25_m.png" alt="" class="only_m" style="max-width: 37px">
                        </div>
                        <strong>Service <span>품질향상</span></strong>
                        <p><i>·</i> 다양한 HR Contents 제공 <br>(제도 및 규정, 문의 응대 등)</p>
                        <p><i>·</i> HR 인력 운영 인건비 절감</p>
                        <p><i>·</i> Process, IT System, 전문성 향상을 <br>통한 Delivery Time 축소 실현</p>
                    </li>
                </ul>

            </div>

            <div class="cont_box ">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl">HR BPO <span>Service Core Competency</span></h3>
                </div>
                <p class="desc_02">HR 전문성을 바탕으로 정확한 서비스를 제공하고, 제도 변경 등에 앞서 대응해 드립니다.</p>
                <ul class="top_line w1000">
                    <li>
                        <h3><i>01.</i> <span>HR전문 법인과의</span> Partnership 체결</h3>
                        <!-- BEGIN 2022 04 26 텍스트 수정  -->  
                        <p>Global 회계법인과 노무사 등 HR전문가와의 자문계약을 체결하여, 법령 변경시의 동향을 사전 접수하여, 고객사에 적기 대응을 위한 자료를 제공해 드리고 있습니다</p>
                        <!-- END 2022 04 26 텍스트 수정  -->  
                    </li>
                    <li>
                        <h3><i>02.</i> <span>국내 법령의 적기 대응</span> 및 컨설팅 제공</h3>
                        <!-- BEGIN 2022 04 26 텍스트 수정  --> 
                        <p>주요 고객사가 대기업인만큼 법령 개정 등의 정보를 신속하게 접수하여 검토 후, 적용안/ 적용시의 이슈/ 운영안/시스템 개발 등을 사전에 검토, 제안드리는 수준 높은 컨설팅 서비스를 제공해 드리고 있습니다. 이는 대기업 고객사의 제도 운영을 기반으로 한 풍부한 운영경험과 HR전문지식에 기반한 HR SSC만이 가능한 컨설팅 서비스입니다.</p>
                        <!-- END 2022 04 26 텍스트 수정  -->  
                    </li>
                </ul> 
                <div class="img_wrap mar_asome">
                    <img src="../img/sub/bpo_hr_07.png" alt="제휴파트너" class="only_w">
                    <img src="../img/sub/bpo_hr_07_m.png" alt="제휴파트너"  class="only_m">
                </div>
            </div>


            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl">서비스<span> 이관 절차</span></h3>
                </div>
                <p class="desc_02">
                고객사 경영진과의 Consensus 후, <span>약 4~6주간의 Process Design</span> 단계를 거쳐 변화관리 및 이행을 추진하게 됩니다.
                </p>
                <div class="img_wrap show">
                    <img src="../img/sub/bpo_hr_08_m.jpg" alt="이관절차" class="only_m">
                    <img src="../img/sub/bpo_hr_08.png" alt="이관절차"  class="only_w">
                </div>
                <div class="img_wrap mar_asome">
                    <img src="../img/sub/bpo_hr_09.png" alt="제휴파트너"  class="only_w"> 
                    <img src="../img/sub/bpo_hr_09_m.png" alt="제휴파트너 " class="only_m">
                </div>
            </div>

            

                
        </div>
            
            

    </div> <!-- inner -->

    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="">
            </div>
           
		</div>
	</div>
     
   


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>