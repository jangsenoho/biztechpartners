<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub  bpo finance" data-depth="bpo" data-menu="bpo_04" data-subnav="bpo_04">
    <div class="inner_1200">

           <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/bpo_nav.php');?>

        <div class="sub_cont" data-depth="finance" data-menu="fin_04" data-finnav="fin_04">

            <!-- <h2 class="ttl ttl_02">재무 BPO Service</h2> -->
            <? include('./fin_nav.php');?> 

            <div class="ttl_box mar_ov m0">
                <h2 class="ttl">AR/AP 업무</h2>
                <span class="line"></span>
            </div>

            <div class="cont_box">
                <!-- <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>연말정산</span> Service</h3>
                </div> -->
                <p class="desc_02"><span>AR업무는 기업의 매출과 관련한 청구 및 외상매출금 회수관리</span>이며, <span>AP업무는 지급과 관련된 매입처리 업무</span>입니다. <br>
                    기업의 채권(AR)과 매입(AP) 거래는 반복적이며 업무투입 대비 시간적/에포트 비용이 많이 들어가기 때문에<br class="only_w">
                    BPO서비스의 워크 플로우를 통해 Over-head 비용을 줄이고 업무의 효율성과 정확성을 향상시킵니다.</p>
                <div class="dot_wrap m40">
                    <strong><span>Our Service</span></strong>
                </div>
                <ul class="ico_s_100w wid65">
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image:url(../img/sub/bpo_service_06.png)" class="only_w"></span> 
                            <span style="background-image:url(../img/sub/bpo_service_06_m.jpg)" class="only_m"></span> 
                            <strong>AR 업무 <span>(청구 - Billing)</span></strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>매출 세금계산서 발행/수정/취소 </p>
                            <p><i> · </i>미수채권 현황관리  </p>
                            <p><i> · </i>Billing과 청구 데이타 국세청대사 작업 </p>
                            <p><i> · </i>Invoice 청구 및 매출전표 관리</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image:url(../img/sub/bpo_service_07.png)" class="only_w"></span> 
                            <span style="background-image:url(../img/sub/bpo_service_07_m.jpg)" class="only_m"></span> 
                            <strong>AP 업무 <span>(지급 - Payment)</span></strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>거래처 매입 세금계산서 전표/계정관리</p>
                            <p><i> · </i>대금 결제일 및 CashFlow 예측 관리 </p>
                            <p><i> · </i>매입 데이타와 국세청대사 작업 </p>
                            <p><i> · </i>거래처 등록 및 계좌관리  </p>
                        </div>
                    </li>
                </ul>
            </div>

            


            

            

                
        </div>
            
            

    </div> <!-- inner -->
     
   


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>