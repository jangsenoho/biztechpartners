<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub  bpo finance" data-depth="bpo" data-menu="bpo_04" data-subnav="bpo_04">
    <div class="inner_1200">
           <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/bpo_nav.php');?>

        <div class="sub_cont" data-depth="finance" data-menu="fin_03" data-finnav="fin_03">

            <!-- <h2 class="ttl ttl_02">재무 BPO Service</h2> -->
            <? include('./fin_nav.php');?> 

            <div class="ttl_box mar_ov m0">
                <h2 class="ttl">어카운팅 업무</h2>
                <span class="line"></span>
            </div>

            <div class="cont_box">
                <!-- <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>연말정산</span> Service</h3>
                </div> -->
                <p class="desc_02"><span>기업의 비용처리와 관련된 재무회계 업무를 대행</span>하고 있으며, 처리된 업무의 문의 응대를 통해 <br class="only_w">
                    <span>고객 임직원의 불편함을 해결</span>해주고 있습니다. <br>
                    또한 회계 관련 이슈에 대해 임직원 내부 구성원과 커뮤니케이션하며 업무 프로세스 개선활동을 제공합니다.</p>
                <ul class="gray_asome">
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/bpo_service_01.png" alt="" class="only_w">
                            <img src="../img/sub/bpo_service_01_m.png" alt="" class="only_m" style="max-width: 84px">
                        </div>
                        <strong>일반 경비처리 </strong>
                        <p> 
                            <i>·</i>  법인카드 관리
                            <span>- 법인카드 발급/신청</span>
                            <span>- 법인카드 사용내역 및 청구내역 정산</span>
                        </p>
                        <p>
                            <i>·</i>  비용 전표 입력관리
                            <span>- 임직원 일반경비 (출장 및 교통비 등)</span>
                        </p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/bpo_service_02.png" alt=""  class="only_w">
                            <img src="../img/sub/bpo_service_02_m.png" alt="" class="only_m"  style="max-width: 78px">
                        </div>
                        <strong>비용 모니터링/응대</strong>
                        <p>
                            <i>·</i>  비용 사용내역 거래처, 직원비용 심사
                            <span>(증빙, 문서 등)</span>
                        </p>
                        <p>
                            <i>·</i>  경비처리 관련 제 규정 문의 응대
                            <span>- 통제 경비 한도 및 실적/ 품의서 <br>승인 관련 응대</span>
                        </p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/bpo_service_03.png" alt=""  class="only_w">
                            <img src="../img/sub/bpo_service_03_m.png" alt="" class="only_m"  style="max-width: 59px">
                        </div>
                        <strong>Report</strong>
                        <p><i>·</i>  비용 관련 처리 실적 자료 Report</p>
                        <p><i>·</i>  업무개선 제안 및 회계처리 정책의 이행지원</p>
                    </li>
                </ul>
            </div>

            


            

            

                
        </div>
            
            

    </div> <!-- inner -->
     
   


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>