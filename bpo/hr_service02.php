<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub bpo  hr" data-depth="bpo" data-menu="bpo_02" data-subnav="bpo_02">
    <div class="inner_1200">

           <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/bpo_nav.php');?>

        <div class="sub_cont" data-depth="imple" data-menu="hr_02" data-hrnav="hr_02">

            <!-- <h2 class="ttl ttl_02">HR BPO Service</h2> -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/hr_nav.php');?> 

            <div class="ttl_box mar_ov m50">
                <h2 class="ttl">급여/복리후생</h2>
                <span class="line"></span>
                <p class="desc_02"><span>급여/상여, 퇴직금의 계산 및 지급,</span> 사회보험의 자격관리 및 <br class="only_m">납부 업무와 <span>임직원의 복리후생 서비스</span>를 제공합니다.</p>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>복리후생</span> Service</h3>
                </div>
                <ul class="top_line w25">
                    <li>
                        <div>
                            <div class="img_25">
                                <img src="../img/sub/bpo_hr_10.png" alt="" class="only_w">
                                <img src="../img/sub/bpo_hr_10_m.png" alt="" class="only_m" style="max-width:52px ">
                            </div>
                            <strong>급여 · 퇴직금</strong>
                        </div>
                        <div>
                            <span>급여, 상여, 성과급</span>
                            <p><i>·</i>  급여, 상여, 성과급 계산</p>
                            <p><i>·</i>  수당, 제지급금 계산</p>
                            <p><i>·</i>  공제항목 계산</p>
                            <p><i>·</i>  원천세 검증</p>
                        </div>
                        <div>
                            <span>퇴직금</span>
                            <p><i>·</i>  퇴직연금 대상자 관리</p>
                            <p><i>·</i>  퇴직금 계산</p>
                            <p><i>·</i>  퇴직금 중도청산</p>
                            <p><i>·</i>  합산과세 계산</p>
                        </div>
                        <div>
                            <span>주재원 지원관리</span>
                            <p><i>·</i>  주재원 귀임/부임관리</p>
                            <p><i>·</i>  국내 종합소득 신고</p>
                        </div>
                    </li>
                    <li>
                        <div>
                            <div class="img_25">
                                <img src="../img/sub/bpo_hr_11.png" alt="" class="only_w">
                                <img src="../img/sub/bpo_hr_11_m.png" alt="" class="only_m" style="max-width:35px ">
                            </div>
                            <strong>사회보험</strong>
                        </div>
                        <div>
                            <span>자격관리</span>
                            <p><i>·</i>  입사자 취득 신고</p>
                            <p><i>·</i>  퇴직 상실신고</p>
                        </div>
                        <div>
                            <span>납부 및 정산</span>
                            <p><i>·</i>  사회보험 납부</p>
                            <p><i>·</i>  보수총액 신고</p>
                            <p><i>·</i>  정산신고</p>
                        </div>
                        <div>
                            <span>사회보험 부가서비스</span>
                            <p><i>·</i>  장애인 고용부담금 정산 납부</p>
                            <p><i>·</i>  임금피크제 정부 지원금 관리 </p>
                            <p><i>·</i>  육아휴직 장려금 신청</p>
                        </div>
                    </li>
                    <li>
                        <div>
                            <div class="img_25">
                                <img src="../img/sub/bpo_hr_12.png" alt="" class="only_w">
                                <img src="../img/sub/bpo_hr_12_m.png" alt="" class="only_m" style="max-width:35px ">
                            </div>
                            <strong>복리후생</strong>
                        </div>
                        <div>
                            <span>일반 복리후생 서비스</span>
                            <p><i>·</i>  경조금, 의료비, 학자금 등 지급처리</p>
                            <p><i>·</i>  종합검진 예약관리</p>
                            <p><i>·</i>  공제항목 계산</p>
                            <p><i>·</i>  원천세 검증</p>
                        </div>
                        <div>
                            <span>휴양소 운영서비스</span>
                            <p><i>·</i>  콘도, 연수원 예약 및 취소 </p>
                        </div>
                        <div>
                            <span>선택적 복리후생 서비스</span>
                            <p><i>·</i>  선택적 복리후생 포인트 지급</p>
                            <p><i>·</i>  잔액 이월관리  </p>
                        </div>
                    </li>
                    
                </ul>
            </div>

            



                
        </div>
            
            

    </div> <!-- inner -->
     
   


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>