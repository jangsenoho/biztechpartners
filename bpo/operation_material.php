<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub bpo bpo_table" data-depth="bpo" data-menu="bpo_03" data-subnav="bpo_03">
    <div class="inner_1200">
           <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/bpo_nav.php');?>

        <div class="sub_cont" data-depth="imple" data-menu="oper_03" data-opernav="oper_03">

            <!-- <h2 class="ttl ttl_02">경영지원 BPO Service​</h2> -->
            <? include('./oper_nav.php');?> 

            <div class="ttl_box mar_ov">
                <h2 class="ttl">구매/자재관리</h2>
                <span class="line"></span>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>Purchase</span> Order</h3>
                </div>
                <p class="desc_02">국내외 A/S센터 서비스 부품 및 자재 수요예측, 발주, 공급 및 재고 등 운영관리</p>
                <div class="dot_wrap">
                    <strong><span>서비스 범위</span></strong>
                </div>

                <ul class="ico_s_100w">
                    <li>
                        <div class="ico_wrap">
                            <span></span> 
                            <strong>자산공급</strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>국내 및 해외법인 A/S센터의 부품/자재 공급, 선행물품 주문 </p>
                            <p><i> · </i>공급량 수요예측 정보제공, 구매 및 재고관리 분석, 모니터링 실시 </p>
                            <p><i> · </i>자재 및 부품 주문대행</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span></span> 
                            <strong class="red">운영내역</strong>
                        </div>
                        <div class="ico_desc asome">
                            <p><strong><i> · </i>수요예측</strong> 공급처별 부품 및 자재 적정 수요량 사전예측 정보 제공</p>
                            <p><strong><i> · </i>발주관리</strong> 부품/자재 선행물동 정기발주 및 에러 점검</p>
                            <p><strong><i> · </i>재고관리</strong> 주문상태 및 취소, 법인별 재고금액 관리 등</p>
                            <p><strong><i> · </i>공급관리</strong> 부품주문 및 공급현황 관리</p>
                            <p><strong><i> · </i>품질관리</strong> KPI 관리 : 적기공급(즉납율), 적정재고량(최소화)</p>
                        </div>
                    </li>
                </ul>
                
               
               
            </div>

            

                
        </div>
            
            

    </div> <!-- inner -->
     
   


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>