<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub bpo " data-depth="bpo" data-menu="bpo_03" data-subnav="bpo_03">
    <div class="inner_1200">

           <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/bpo_nav.php');?>

        <div class="sub_cont" data-depth="imple" data-menu="oper_01" data-opernav="oper_01">

            <!-- <h2 class="ttl ttl_02">경영지원 BPO Service​</h2> -->
            <? include('./oper_nav.php');?>

            <div class="ttl_box mar_ov">
                <h2 class="ttl">서비스 영역</h2>
                <span class="line"></span>
                <p class="desc_02">
                경영지원(Business Support) BPO는 기업의 비즈니스 프로세스와 관련된 PC자산, 구매/자재공급관리,임직원복지몰,  <br class="only_w">
                개인정보보안관리, 총무일반 등을 포함한 <span>경영지원분야의 아웃소싱 서비스를 수행하고 있으며,</span> <br class="only_w">
                <span>해당분야의 전문성을 기반으로 체계적이고 안정적인 서비스를 제공</span>합니다.
                </p>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>서비스</span> 영역</h3>
                </div>
                
                <div class="img_wrap pad show">
                    <img src="../img/sub/bpo_01_m.jpg" alt="구축이미지" class="only_m">
                    <img src="../img/sub/bpo_01.png" alt="구축이미지"  class="only_w">
                </div>
               
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>서비스</span> Activity</h3>
                </div>

                <ul class="w33">
                    <li>
                        <strong>Business <br>Support</strong>
                    </li>
                    <li>
                        <i>01.</i>
                        <span>Purchase Order <br>(구매/자재관리)</span>
                        <div class="txt_wrap">
                            <p><i>ㆍ</i>해외 자재부품 수요예측, 모니터링 </p>
                            <p><i>ㆍ</i>주문 및 재고량 관리  </p>
                            <p><i>ㆍ</i>자재부품 적기주문 공급 </p>
                        </div>
                    </li>
                    <li>
                        <i>02.</i>
                        <span>Asset Management<br>(자산관리)</span>
                        <div class="txt_wrap">
                            <p><i>ㆍ</i>PC리스 및 공급  </p>
                            <p><i>ㆍ</i>PC Life Cycle관리   </p>
                            <p><i>ㆍ</i>자산관리 및 운영  </p>
                            <p><i>ㆍ</i>자산실사 대행  </p>
                        </div>
                    </li>
                    <li>
                        <i>03.</i>
                        <span>General Affair<br>(총무 일반)</span>
                        <div class="txt_wrap">
                            <p><i>ㆍ</i>총무일반 대행  </p>
                            <p><i>ㆍ</i>외주구매 및 계약모니터링   </p>
                        </div>
                    </li>
                    <li>
                        <i>04.</i>
                        <span>Privacy Protection<br>General (개인정보보호)</span>
                        <div class="txt_wrap">
                            <p><i>ㆍ</i>평가대상식별/검토/분석  </p>
                            <p><i>ㆍ</i>개선계획 수립, 이행     </p>
                            <p><i>ㆍ</i>평가진단,이슈조치    </p>
                            <p><i>ㆍ</i>이행점검, 확인서발급     </p>
                        </div>
                    </li>
                    <li>
                        <i>05.</i>
                        <span>Employee Welfare Mall<br>(임직원 복지몰)</span>
                        <div class="txt_wrap">
                            <p><i>ㆍ</i>LG그룹임직원복지몰내 커머스운영 (LG Life Care Potal내)</p>
                            <p><i>ㆍ</i>우수공급사 입점 및 상품판매</p>
                        </div>
                    </li>
                </ul>


                    
                    <!-- <div class="img_wrap pad">
                        <img src="../img/sub/bpo_02_m.png" alt="구축이미지"  class="only_m">
                        <img src="../img/sub/bpo_02.png" alt="구축이미지" class="only_w">
                    </div> -->
            </div>

                
        </div>
            
            

    </div> <!-- inner -->
     
    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="">
            </div>
           
		</div>
	</div>


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>