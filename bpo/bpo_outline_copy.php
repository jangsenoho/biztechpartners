<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub bpo outline" data-depth="bpo" data-menu="bpo_01" data-subnav="bpo_01">
    <div class="inner_1200">

           <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/bpo_nav.php');?>

        <div class="sub_cont">

            <div class="ttl_box mar_no">
                <a href="#" class="down">고객사용 비즈테크 BPO Brochure <span>Download</span> <i></i></a>
                <h2 class="ttl">BPO 개요</h2>
                <span class="line"></span>
                <div class="gray_box w1000">
                    <p class="text">진정한 파트너십을 통해 <span>고객의 업무혁신을 지원</span>합니다.</p>
                </div>
                <p class="desc_02"><strong>BPO(Business Process Outsourcing)사업부</strong>는 고객이 핵심비즈니스에 집중할 수 있도록 HR(급여, 복리후생 등)서비스, 교육운영, <br class="only_w">회계 및 결산지원 등 다양한 경영지원 업무를 전문적으로 수행함으로써 고객의 성공을 지원하고 있습니다. ​</p>
            </div>

            <!-- 서비스 영역 -->
            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>주요</span> 서비스 영역 </h3>
                </div>
                <p class="desc_02">(세부적인 내용은 서비스별 상세 메뉴 참조)</p>
                <div class="img_box_f">
                    <div class="img_wrap">
                        <img src="../img/sub/out_01.jpg" 1alt="솔루션 설명" class="only_w">
                        <img src="../img/sub/out_01_m.png   " 1alt="솔루션 설명" class="only_m">
                    </div>
                    <div class="img_wrap">
                        <img src="../img/sub/out_02.jpg" alt="솔루션 설명" class="only_w">
                        <img src="../img/sub/out_02_m.png   " 1alt="솔루션 설명" class="only_m">
                    </div>
                    <div class="img_wrap">
                        <img src="../img/sub/out_03.jpg" alt="솔루션 설명" class="only_w">
                        <img src="../img/sub/out_03_m.png   " 1alt="솔루션 설명" class="only_m">
                    </div>
                    <div class="img_wrap">
                        <img src="../img/sub/out_04.jpg" alt="솔루션 설명" class="only_w">
                        <img src="../img/sub/out_04_m.png   " 1alt="솔루션 설명" class="only_m">
                    </div>
                    
                </div>
            </div>
            <!--  ------------------------------------------- //서비스 영역 -->

            <!--  도입효과-->
            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>BPO</span> 도입 효과</h3>
                </div>
                <ul class="ico_s_33w">
                    <li>
                        <span style="background-image: url(../img/sub/out_05.jpg)" class="only_w"></span>
                        <span style="background-image: url(../img/sub/out_05_m.png)" class="only_m"></span>
                        <strong>핵심/본질에 집중</strong>
                        <p><i> · </i> 고객 핵심업무 집중​</p>
                        <p><i> · </i> 새로운 부가가치 창출​</p>
                        <p><i> · </i> 혁신 활동 가속화​​</p>
                    </li>
                    <li>
                    <span  style="background-image:  url(../img/sub/out_06.jpg)" class="only_w"></span>
                    <span style="background-image: url(../img/sub/out_06_m.png)" class="only_m"></span>
                        <strong>업무효율/안정성 증대</strong>
                        <p><i> · </i> 구성원 만족도 증대​​</p>
                        <p><i> · </i> 담당업무 효율성 증가​​​</p>
                        <p><i> · </i>  담당자 변동 리스크 해소​​​</p>
                    </li>
                    <li>
                        <span  style="background-image:  url(../img/sub/out_07.jpg)" class="only_w"></span>
                        <span style="background-image: url(../img/sub/out_07_m.png)" class="only_m"></span>
                        <strong>업무 품질 향상</strong>
                        <p><i> · </i> 일관성 있는 전문성 확보​</p>
                        <p><i> · </i> 서비스별 지표 관리​</p>
                        <p><i> · </i>  업무 개선 외 ​​</p>
                    </li>
                </ul>
            </div>
            <!-- ------------------------------------------------- //  도입 효과 -->

            <!-- 고객사 -->

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>주요</span> 고객사</h3>
                </div>
                <!-- lg그룹사 -->
                <div class="logo_wrap">
                    <div class="dot_wrap">
                        <strong>LG 그룹사</strong>
                    </div>
                    <ul class="cust_list">
                        <li>
                            <img src="../img/sub/logo_01.png" alt="" class="only_w">
                            <img src="../img/sub/logo_01_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_02.png" alt="" class="only_w">
                            <img src="../img/sub/logo_02_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_03.png" alt="" class="only_w">
                            <img src="../img/sub/logo_03_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_04.png" alt="" class="only_w">
                            <img src="../img/sub/logo_04_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_05.png" alt="" class="only_w">
                            <img src="../img/sub/logo_05_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_06.png" alt="" class="only_w">
                            <img src="../img/sub/logo_06_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_07.png" alt="" class="only_w">
                            <img src="../img/sub/logo_07_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_08.png" alt="" class="only_w">
                            <img src="../img/sub/logo_08_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_09.png" alt="" class="only_w">
                            <img src="../img/sub/logo_09_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_10.png" alt="" class="only_w">
                            <img src="../img/sub/logo_10_m.jpg" alt=""  class="only_m">
                        </li>
                        <li class="last">
                            <img src="../img/sub/logo_11.png" alt="" class="only_w">
                            <img src="../img/sub/logo_11_m.jpg" alt=""  class="only_m">
                        </li>
                        <li class="last">
                            <img src="../img/sub/logo_12.png" alt="" class="only_w">
                            <img src="../img/sub/logo_12_m.jpg" alt=""  class="only_m">
                        </li>
                        <li class="last">
                            <img src="../img/sub/logo_13.png" alt="" class="only_w">
                            <img src="../img/sub/logo_13_m.jpg" alt=""  class="only_m">
                        </li>
                    </ul>

                </div>
                <!-- ---------------------------------------------------//lg그룹사 -->

                <!-- 기업 -->
                <div class="logo_wrap">
                    <div class="dot_wrap">
                        <strong>기업</strong>
                    </div>
                    <ul class="cust_list">
                        <li>
                            <img src="../img/sub/logo_14.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_14_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_15.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_15_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_16.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_16_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_17.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_17_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_18.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_18_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_19.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_19_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_20.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_20_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_21.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_21_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_22.jpg" alt="" class="only_w"> 
                            <img src="../img/sub/logo_22_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_23.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_23_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_24.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_24_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_25.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_25_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_26.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_26_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_27.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_27_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_28.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_28_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_29.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_29_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_30.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_30_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_31.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_31_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_32.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_32_m.jpg" alt=""  class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_33.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_33_m.jpg" alt=""  class="only_m">
                        </li>
                        <li class="last">
                            <img src="../img/sub/logo_34.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_34_m.jpg" alt=""  class="only_m">
                        </li>
                        <li class="last">
                            <img src="../img/sub/logo_35.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_35_m.jpg" alt=""  class="only_m">
                        </li>
                        <li class="last">
                            <img src="../img/sub/logo_36.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_36_m.jpg" alt=""  class="only_m">
                        </li>
                    </ul>
                </div>
                <!-- ---------------------------------------------------//기업 -->
                 <!-- 기타 -->
                 <div class="logo_wrap">
                    <div class="dot_wrap">
                        <strong>기타</strong>
                    </div>
                    <ul class="cust_list">
                        <li>
                            <img src="../img/sub/logo_37.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_37_m.jpg" alt="" class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_38.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_38_m.jpg" alt="" class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_39.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_39_m.jpg" alt="" class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_40.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_40_m.jpg" alt="" class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_41.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_41_m.jpg" alt="" class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_42.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_42_m.jpg" alt="" class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_43.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_43_m.jpg" alt="" class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_44.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_44_m.jpg" alt="" class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_45.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_45_m.jpg" alt="" class="only_m">
                        </li>
                        <li>
                            <img src="../img/sub/logo_46.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_46_m.jpg" alt="" class="only_m">
                        </li>
                        <li class="last">
                            <img src="../img/sub/logo_47.jpg" alt="" class="only_w">
                            <img src="../img/sub/logo_47_m.jpg" alt="" class="only_m">
                        </li>
                    
                    </ul>
                </div>
                <!-- ---------------------------------------------------//기업 -->

            </div>

           

          

           

         



            

                
        </div>
            
            

    </div> <!-- inner -->
     
   


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>