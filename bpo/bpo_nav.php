<ul class="sub_gnb">
    <li><a href="../sap/implementation.php">SAP</a></li>
    <li ><a href="../web/si_sm.php">WEB</a></li>
    <li  class='on'><a href="/bpo/bpo_outline.php">BPO</a></li>
</ul>

<div class="sub_nav_wrap">
    <button type="button" class="now_btn only_m"></button>
    <ul class="sub_nav">
        <li data-subnav="bpo_01"><a href="/bpo/bpo_outline.php">BPO 개요</a></li>
        <li data-subnav="bpo_02"><a href="/bpo/hr_service01.php">HR BPO Service​</a></li>
        <li data-subnav="bpo_03"><a href="/bpo/operation_service.php">경영지원 BPO Service​</a></li>
        <li data-subnav="bpo_04"><a href="/bpo/finance_01.php">재무 BPO Service​</a></li>
    </ul>
</div>

<script>
   
    $(function () {

        var detph = $('#container').data('subnav');
        var nowTxt = $('.sub_nav li[data-subnav="' + detph + '"]').text();
        $('.sub_nav li[data-subnav="' + detph + '"]').addClass('on');
        $('.sub_nav_wrap .now_btn').text(nowTxt);

        $('.sub_nav_wrap .now_btn').on('click', function() {
            $('.now_btn').toggleClass('on')
            $('.sub_nav').toggle();
        })
    });




</script>

