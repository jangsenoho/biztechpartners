<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub bpo  hr" data-depth="bpo" data-menu="bpo_02" data-subnav="bpo_02">
    <div class="inner_1200">
           <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/bpo_nav.php');?>

        <div class="sub_cont" data-depth="imple" data-menu="hr_06" data-hrnav="hr_06">

            <!-- <h2 class="ttl ttl_02">HR BPO Service</h2> -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/hr_nav.php');?> 

            <div class="ttl_box mar_ov m50">
                <h2 class="ttl">채용행정</h2>
                <span class="line"></span>
                <p class="desc_02">채용행정서비스는 <span>채용행정 전반의 교육 운영/위탁서비스</span>를 제공합니다</p>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>채용행정</span> Service</h3>
                </div>
                <ul class="gray_asome_02 ">
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/bpo_hr_21.png" alt=""  class="only_w">
                            <img src="../img/sub/bpo_hr_21_m.png" alt="" class="only_m" style="max-width:49px ">
                        </div>
                        <strong>채용행정</strong>
                        <p>·  LG Careers 운영 (그룹포털)</p>
                        <p>·  채용 정시/수시 관리
                            <span>
                                - 공고별 전형관리 <br>
                                - 인적성 관리 <br>
                                - 검진이력 관리
                            </span>
                        </p>
                        <p>·  면접 OFF-Line 지원</p>
                        <p>·  지원자 문의 응대</p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/bpo_hr_22.png" alt="" class="only_w">
                            <img src="../img/sub/bpo_hr_22_m.png" alt="" class="only_m" style="max-width:43px ">
                        </div>
                        <strong>Relocation</strong>
                        <p>·  채용 VISA 신규 발급 및 연장</p>
                        <p>·  외국인 퇴직/변경 신고 관리</p>
                        <p>·  이주화물 caring/monitoring</p>
                        <p>·  Housing 및 레지던스 안내/제공</p>
                        <p>·  해외법인 초청장 발행</p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/bpo_hr_23.png" alt="" class="only_w">
                            <img src="../img/sub/bpo_hr_23_m.png" alt="" class="only_m" style="max-width:42px ">
                        </div>
                        <strong>인사정보</strong>
                        <p>·  인사기록카드 입력 및 보관</p>
                        <p>·  시스템 data 관리 및 정비
                            <span>
                                - 재직자 인사정보 update <br>
                                - 퇴직자 분류 및 폐기 
                            </span>
                        </p>
                        <p>·  국내/해외 학력검증</p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/bpo_hr_24.png" alt="" class="only_w">
                            <img src="../img/sub/bpo_hr_24_m.png" alt="" class="only_m" style="max-width:51px ">
                        </div>
                        <strong>산학장학생</strong>
                        <p>·  산학장학생 대상자 관리
                            <span>
                                - 신규 등록 및 현황 관리 <br>
                                - 장학금 지급 및 환수 관리
                            </span>
                        </p>
                        <p>·  산학장학생 문의 응대</p>
                        <p>·  입사 후 의무기간 및 비용 관리</p>    
                    </li>
                </ul>
            </div>

            



                
        </div>
            
            

    </div> <!-- inner -->
     
   


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>