<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub bpo bpo_table op_pri " data-depth="bpo" data-menu="bpo_03" data-subnav="bpo_03">
    <div class="inner_1200">

           <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/bpo_nav.php');?>

        <div class="sub_cont" data-depth="imple" data-menu="oper_04" data-opernav="oper_04">

            <!-- <h2 class="ttl ttl_02">경영지원 BPO Service​</h2> -->
            <? include('./oper_nav.php');?> 

            <div class="ttl_box mar_ov">
                <h2 class="ttl">개인정보보호</h2>
                <span class="line"></span>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>Privacy Impact</span> Assessment</h3>
                </div>
                <p class="desc_02">기업운영에 필요한  <span>개인정보사용시 관련법을 준수할 수 있도록 개인정보영향평가를 진행하고 이후 안정적인 관리방안을 제시</span>합니다.</p>
                <div class="dot_wrap">
                    <strong><span>서비스 범위</span></strong>
                </div>

                <ul class="ico_s_33w">
                    <li>
                        <span></span>
                        <strong>운영역할</strong>
                        <p><i> · </i>평가대상내용 식별 및 사전검토/분석</p>
                        <p><i> · </i>내용 진단 및 평가 실시</p>
                        <p><i> · </i>관리프로세스 구축 및 운영기준 제안</p>
                    </li>
                    <li>
                        <span></span>
                        <strong>운영단계</strong>
                        <p><i> · </i>평가대상 식별 및 통지</p>
                        <p><i> · </i>사전검토 및 분석</p>
                        <p><i> · </i>체크리스트작성 및 평가진행</p>
                        <p><i> · </i>개선계획 평가</p>
                        <p><i> · </i>이행점검 및 평가</p>
                        <p><i> · </i>이슈사항 공유 및 조치</p>
                    </li>
                    <li>
                        <span></span>
                        <strong>품질관리</strong>
                        <p><i> · </i>CPPG(개인정보관리사) 자격증보유 </p>
                        <p><i> · </i>관련법 이해 및 적용 <br>(개인정보보호법, 정보통신망법 등)</p>
                        <p><i> · </i>기업의 개인정보영향평가 기준 및 <br>개인정보보호 규칙이해</p>
                        <p><i> · </i>서비스 및 네트웍(서버 · DB)구조 이해, 분석 </p>
                        <p><i> · </i>개인정보수집항목 파악 및 적용  </p>
                    </li>
                </ul>
                
               
               
            </div>

            

                
        </div>
            
            

    </div> <!-- inner -->
     
   


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>