<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub bpo bpo_table " data-depth="bpo" data-menu="bpo_03" data-subnav="bpo_03">
    <div class="inner_1200">

           <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/bpo_nav.php');?>

        <div class="sub_cont" data-depth="imple" data-menu="oper_02" data-opernav="oper_02">

            <!-- <h2 class="ttl ttl_02">경영지원 BPO Service​</h2> -->
            <? include('./oper_nav.php');?> 

            <div class="ttl_box mar_ov">
                <h2 class="ttl">자산관리/IT인프라 공급</h2>
                <span class="line"></span>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>Asset</span> Management</h3>
                </div>
                <p class="desc_02">기업내 PC 등 OA 공급 및 자산운영 관리</p>
                <div class="dot_wrap">
                    <strong><span>서비스 범위</span></strong>
                </div>

                <ul class="ico_s_33w">
                    <li>
                        <span></span>
                        <strong>자산공급</strong> 
                        <p><i> · </i>모델선정 : 신규 표준모델 직군별 제안</p>
                        <p><i> · </i>물량예측·확보</p>
                        <p><i> · </i>PC 등 OA기구 교체 진행</p>
                    </li>
                    <li>
                        <span></span>
                        <strong>자산관리운영</strong>
                        <p><i> · </i>자산관리 운영 정책 설계 및 제안</p>
                        <p><i> · </i>운영 유지보수관리</p>
                        <p><i> · </i>자산실사 대행</p>
                        <p><i> · </i>사용 연장/교체/반납 등 운영</p>
                    </li>
                    <li>
                        <span></span>
                        <strong>품질관리</strong>
                        <p><i> · </i>서비스운영 고객만족도 실시</p>
                        <p><i> · </i>운영프로세스 및 정책 개선 제안</p>
                    </li>
                </ul>
                
               
               
            </div>

            

                
        </div>
            
            

    </div> <!-- inner -->
     
   


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(2).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>