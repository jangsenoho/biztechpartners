<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub bpo  hr_04 hr" data-depth="bpo" data-menu="bpo_02" data-subnav="bpo_02">
    <div class="inner_1200">

        <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/bpo_nav.php');?>

        <div class="sub_cont" data-depth="imple" data-menu="hr_04" data-hrnav="hr_04">

            <!-- <h2 class="ttl ttl_02">HR BPO Service</h2> -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/hr_nav.php');?> 

            <div class="ttl_box mar_ov m50">
                <h2 class="ttl">생애설계 교육/컨설팅</h2>
                <span class="line"></span>
                <p class="desc_02">우리의 삶은 과거 어느 때보다 훨씬 길어졌습니다.</p>
            </div>

            <div class="cont_box">
                <ul class="gray_box">
                    <li> 
                        <i>·</i>  
                        <div>고령화 사회로의 변화는 기업에서도 크게 다르지 않습니다. <span>중/장년층이 증가하고, 조기퇴직 및 정년퇴직 인원이 증가함</span> 에 따라 조직문화 및 구조에 대한 적극적인 고민이 요구되는 시기입니다. </div>
                    </li>
                    <li>  
                        <i>·</i>  
                        <div>
                        나아가 기술과 사회구조의 변화, 근로자보호 중심의 고용정책 등에 맞춰 기업은 <span>사회적 책임, 생산성 향상, 이미지 제고 차원에서의 다양한 역할에 대응</span>해 나가야 합니다. 
                        </div>
                        
                    </li>
                    <li> 
                        <i>·</i>  
                        <div>
                        비즈테크파트너스는 재직자 및 퇴직(예정)자를 대상으로 <span>일, 시간, 여가, 학습을 재구성하는 생애/진로설계</span> 에 대한 전문서비스 및 관련 법령이 제시하는 <span>운영서비스</span>를 제공하고 있습니다.
                        </div>
                    </li>
                </ul>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>재취업 지원서비스</span> 의무화 제도</h3>
                </div>
                <div class="img_wrap">
                    <img src="../img/sub/bpo_hr_17_m.png" alt="" class="only_m">
                    <img src="../img/sub/bpo_hr_17.png" alt="" class="only_w">
                </div>
                <div class="dot_wrap top_off">
                    <strong>재취업 지원서비스 의무화 제도</strong>
                </div>
                <div class="img_wrap">
                    <img src="../img/sub/bpo_hr_18_m.png" alt="" class="only_m">
                    <img src="../img/sub/bpo_hr_18.png" alt="" class="only_w">
                </div>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>행복한동행센터</span> 소개</h3>
                </div>
                <div class="dot_wrap">
                    <strong>Mission</strong>
                </div>
                <p class="desc_02">근로자의 <span>2nd Life/Job 설계</span>를 지원하여 <span>조직 활력에 기여</span>하고, <span>구성원의 변화</span>를 꿈꾸게 한다.</p>
                <div class="img_wrap hr_04">
                    <img src="../img/sub/bpo_hr_19_m.png" alt="" class="only_m">
                    <img src="../img/sub/bpo_hr_19.jpg" alt="" class="only_w">
                </div>
                <div class="call_mail">
                    <a href="tel: 02-2033-1334"> <i></i> <span>서비스 문의 대표번호</span>02-2033-1334</a>
                    <a href="mailto: ccs.help@biztechpartners.co.kr"> <i></i> <span>E-mail</span>ccs.help@biztechpartners.co.kr</a>
                </div>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>행복한동행센터</span> Reference</span></h3>
                </div>

                <ul class="w50_list">
                    <li>서비스 명칭</li>
                    <li>
                        <div>
                            <strong>01 <span>.</span></strong> 임원 전직지원서비스
                        </div>
                        <div>
                            <strong>02 <span>.</span></strong> ***전직지원 컨설팅 (경력 및 재무)
                        </div>
                    </li>
                    <li>
                        <div>
                            <strong>03 <span>.</span></strong> 직급필수 과정_생애설계
                        </div>
                        <div>
                            <strong>04 <span>.</span></strong> 중 · 장년 경력개발 - 생애관리 교육
                        </div>
                    </li>
                    <li>
                        <div>
                            <strong>05 <span>.</span></strong> 정년퇴직 예정자 교육
                        </div>
                        <div>
                            <strong>06 <span>.</span></strong> (**) 공장 Work Smart 과정
                        </div>
                    </li>
                    <li>
                        <div>
                            <strong>07 <span>.</span></strong> (**) 공장 재무설계 교육
                        </div>
                        <div>
                            <strong>08 <span>.</span></strong> (**) 공장 전문기술직 재무관리 교육
                        </div>
                    </li>
                    <li>
                        <div>
                            <strong>09 <span>.</span></strong> 정년퇴직예정자 교육
                        </div>
                        <div>
                            <strong>10 <span>.</span></strong> 리더십센터 신입사원 재무설계특강
                        </div>
                    </li>
                    <li>
                        <div>
                            <strong>11 <span>.</span></strong> (**) 공장 정년퇴직예정자
                        </div>
                        <div>
                            <strong>12 <span>.</span></strong> 생애 설계 과정
                        </div>
                    </li>
                </ul>
                <div class="img_wrap">
                    <img src="../img/sub/bpo_hr_20_m.png" alt="" class="only_m">
                    <img src="../img/sub/bpo_hr_20.png" alt="" class="only_w">
                </div>
            </div>  
        </div>
            
            

    </div> <!-- inner -->
     
   


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>