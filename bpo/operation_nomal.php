<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub bpo nomal bpo_table " data-depth="bpo" data-menu="bpo_03" data-subnav="bpo_03">
    <div class="inner_1200">

           <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/bpo_nav.php');?>

        <div class="sub_cont" data-depth="imple" data-menu="oper_05" data-opernav="oper_05">

            <!-- <h2 class="ttl ttl_02">경영지원 BPO Service​</h2> -->
            <? include('./oper_nav.php');?> 

            <div class="ttl_box mar_ov">
                <h2 class="ttl">총무일반</h2>
                <span class="line"></span>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>General</span> Affair </h3>
                </div>
                <p class="desc_02">기업의 경영관리 분야의 <span>다양한 사무행정 서비스</span> 를 수행합니다.</p>
                <div class="dot_wrap">
                    <strong><span>서비스 범위</span></strong>
                </div>

                <ul class="ico_s_100w">
                    <li>
                        <div class="ico_wrap">
                            <span></span> 
                            <strong>수출입계약행정</strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>제품구매 계약절차 및 행정처리 진행 및 검토 </p>
                            <p><i> · </i>공사 및 설비계약, 유지보수계약, 수출입계약 진행 및 검토</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span></span> 
                            <strong>외주구매</strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>외주구매 및 계약 관련 행정서비스 </p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span></span> 
                            <strong>총무사무</strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>총무일반 사무행정 대행 </p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span></span> 
                            <strong>보건행정</strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>기업임직원 보건관리 기획 및 운영 </p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span></span> 
                            <strong>기타</strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>경영관리 및 지원영역 전반 사무행정  </p>
                        </div>
                    </li>
                    
                </ul>
                
               
               
            </div>

            

                
        </div>
            
            

    </div> <!-- inner -->
     
   


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>