<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub bpo  hr" data-depth="bpo" data-menu="bpo_02" data-subnav="bpo_02">
    <div class="inner_1200">

           <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/bpo_nav.php');?>

        <div class="sub_cont" data-depth="imple" data-menu="hr_05" data-hrnav="hr_05">

            <!-- <h2 class="ttl ttl_02">HR BPO Service</h2> -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/hr_nav.php');?> 

            <div class="ttl_box mar_ov m50">
                <h2 class="ttl">Career Caring Service</h2>
                <span class="line"></span>
                <p class="desc_02">장수혁명으로 과거에 비해 상대적 조기퇴직 현상이 나타나고, 기술과 사회구조의 변화로 직업세계가 빠르게 변하고 있기 때문에 <br class="only_w">
                <span>행복한 삶을 위해서는 커리어개발이 필수적인 시대</span>입니다. 비즈테크파트너스에서는 재/퇴직자를 대상으로 한 <br class="only_w">CCS(Career Caring Service)를 제공하고 있습니다.</p>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>CCS</span> 개발 배경</h3>
                </div>
                <div class="img_wrap">
                    <img src="../img/sub/bpo_hr_25_m.png" alt="" class="only_m">
                    <img src="../img/sub/bpo_hr_25.png" alt="" class="only_w">
                </div>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>CCS</span> 서비스 브랜드</h3>
                </div>
                <div class="img_wrap show">
                    <img src="../img/sub/bpo_hr_26_m.png" alt="" class="only_m">
                    <img src="../img/sub/bpo_hr_26.png" alt="" class="only_w">
                </div>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>서비스</span> 서비스</h3>
                </div>
                <p class="desc_02">중장년의 연령대별 상황/특성을 고려한 <span>맞춤형 서비스</span>를 제공합니다.</p>
                <div class="img_wrap show" >
                    <img src="../img/sub/bpo_hr_27_m.png" alt="" class="only_m">
                    <img src="../img/sub/bpo_hr_27.png" alt="" class="only_w">
                </div>

                <!-- <div class="question_area">
                    <P>※ 상세한 서비스 안내가 필요하시면 아래 <br class="only_m"> 연락처로 문의 바랍니다 ※</P>
                    <div class="question hr_area">
                    
                        <div class="left">
                            <strong>한창훈 과장</strong>
                            <div>
                                <a href="tel:02-6937-7102" class="tel"><i></i>02-6937-7102</a>
                                <a href="mailto:changhoon.han@biztechpartners.co.kr" class="mail"><i></i>changhoon.han@biztechpartners.co.kr</a>
                            </div>
                            
                        </div>
                        <div class="rig">
                            <strong>박새롬 주임</strong>
                            <div>
                                <a href="tel:02-6937-7199" class="tel"><i></i>02-6937-7199</a>
                                <a href="mailto:ka0408@biztechpartners.co.kr" class="mail"><i></i>ka0408@biztechpartners.co.kr</a>
                            </div>
                        </div>
                        
                    </div>

                </div> -->
               

            </div>

          


            

            



                
        </div>
            
            

    </div> <!-- inner -->
     
    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="">
            </div>
           
		</div>
	</div>
     


</section>




<? include('../inc/footer.php');?>


<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>