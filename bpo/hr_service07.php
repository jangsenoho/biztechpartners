<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub bpo  hr" data-depth="bpo" data-menu="bpo_02" data-subnav="bpo_02">
    <div class="inner_1200">

           <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/bpo_nav.php');?>

        <div class="sub_cont" data-depth="imple" data-menu="hr_07" data-hrnav="hr_07">

            <!-- <h2 class="ttl ttl_02">HR BPO Service</h2> -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/hr_nav.php');?> 

            <div class="ttl_box mar_ov m50">
                <h2 class="ttl">연말정산</h2>
                <span class="line"></span>
                <p class="desc_02">임직원의 연말정산 부당공제신고는 기업 이미지에 부정적 영향을 줄 수 있고, 임직원에게 금전적 손실을 가져오게 됩니다. <br>
                    <span>연말정산 아웃소싱은 전문화된 역량과 운영 경험을 보유하여 많은 세금 혜택 정보를 제공</span>하며 <br class="only_w">
                    임직원들에게 보다 만족적인 서비스를 제공합니다.</p>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box ">
                    <h3 class="bor_ttl"><span>연말정산</span> Service</h3>
                </div>
                <!-- 웹 테이블 -->
                <ul class="table_area only_w">
                    <li>
                        <div class="ttl">단계</div>
                        <div class="ttl_asome">사전준비</div>
                        <div class="ttl_asome">연말정산 수행 </div>
                        <div class="ttl_asome">사후관리</div>
                    </li>
                    <li>
                        <div class="ttl">목록</div>
                        <div class="total_txt">
                            <p><i>· </i> 연말정산 일정수립</p>
                            <p><i>· </i> 연말정산 인력 채용</p>
                            <p><i>· </i> 연말정산 교육 및 안내문 작성</p>
                        </div>
                        <div class="total_txt">
                            <p><i>· </i> 연말정산 임직원 설명회</p>
                            <p><i>· </i> 연말정산 상담 </p>
                            <p><i>· </i> 서류접수 및 검토진행  </p>
                            <p><i>· </i> 서류검토 결과 임직원 피드백  </p>
                            <p><i>· </i> 오류검증/추가서류 취합  </p>
                            <p><i>· </i> 임직원 소득공제 반영  </p>
                            <p><i>· </i> 지급조서 검증  </p>
                        </div>
                        <div class="total_txt">
                            <p><i>· </i> 연말정산 Reporting</p>
                            <p><i>· </i> 결과 Review/개선사항 제안 </p>
                            <p><i>· </i> 서비스 만족도 조사  </p>
                        </div>
                    </li>
                </ul>


                <!-- 모바일 테이블 -->
                <ul class="table_area_m only_m">
                    <li>
                        <div class="ttl">단계</div>
                        <div class="ttl">목록</div>
                    <li>
                        <div class="ttl_asome">사전준비</div>
                        <div class="total_txt">
                            <p><i>· </i> 연말정산 일정수립</p>
                            <p><i>· </i> 연말정산 인력 채용</p>
                            <p><i>· </i> 연말정산 교육 및 안내문 작성</p>
                        </div>
                    </li>
                    <li>
                        <div class="ttl_asome">연말정산 수행 </div>
                        <div class="total_txt">
                            <p><i>· </i> 연말정산 임직원 설명회</p>
                            <p><i>· </i> 연말정산 상담 </p>
                            <p><i>· </i> 서류접수 및 검토진행  </p>
                            <p><i>· </i> 서류검토 결과 임직원 피드백  </p>
                            <p><i>· </i> 오류검증/추가서류 취합  </p>
                            <p><i>· </i> 임직원 소득공제 반영  </p>
                            <p><i>· </i> 지급조서 검증  </p>
                        </div>
                    </li>
                    <li>
                        <div class="ttl_asome">사후관리</div>
                        <div class="total_txt">
                            <p><i>· </i> 연말정산 Reporting</p>
                            <p><i>· </i> 결과 Review/개선사항 제안 </p>
                            <p><i>· </i> 서비스 만족도 조사  </p>
                        </div>
                    </li>
                </ul>
            </div>

            



                
        </div>
            
            

    </div> <!-- inner -->
     
   


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>