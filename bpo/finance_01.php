<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub  bpo finance" data-depth="bpo" data-menu="bpo_04" data-subnav="bpo_04">
    <div class="inner_1200">

           <?php include($_SERVER['DOCUMENT_ROOT'].'/bpo/bpo_nav.php');?>

        <div class="sub_cont" data-depth="finance" data-menu="fin_01" data-finnav="fin_01">

            <!-- <h2 class="ttl ttl_02">재무 BPO Service</h2> -->
            <? include('./fin_nav.php');?> 

            <div class="ttl_box mar_ov m50">
                <h2 class="ttl">서비스 영역</h2>
                <span class="line"></span>
                <p class="desc_02">재무(Finance Outsourcing) BPO는 Shared Service 형태로 제공하며, <br class="only_w">
                <span>고객사의 반복적이고 비핵심적인 업무의 서비스 통합과 표준화된 업무 프로세스를 가능</span>하게 합니다.<br>
                기업의 회계업무 및 서비스를 제공하며 주요 서비스를 요약하면 다음과 같습니다.</p>
            </div>

            <div class="cont_box">
                <div class="dot_wrap">
                    <strong><span>Our Service</span></strong>
                </div>
                <ul class="ico_s_100w">
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image:url(../img/sub/bpo_service_04.png)"></span> 
                            <strong>재무·관리결산/ <br>회계 컨설팅</strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>계정별 마감절차 수행 </p>
                            <span><i> - </i>결산 관련 Cut-off 계정관리</span>
                            <p><i> · </i>계정명세서, 주석PKG 및 재무제표(IS/BS) 작성 </p>
                            <p><i> · </i>재무/관리회계 결산 프로세스 구축 및 자문</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image:url(../img/sub/people_03.png)"></span> 
                            <strong>어카운팅 업무</strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>비용 전표입력/관리 : 법인카드 및 종업원 경비</p>
                            <span><i> - </i>거래처, 직원비용 심사(증빙, 문서 등) 및 전표처리</span>
                            <p><i> · </i>비용계정 모니터링 및 Report </p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image:url(../img/sub/bpo_service_05.png)"></span> 
                            <strong>AR/AP 관리</strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>AR(Account Receivable) 제반업무 수행</p>
                            <span><i> - </i>고객사 매출세금계산서 청구관리</span>
                            <p><i> · </i>AP(Account Payable) 제반업무 수행 </p>
                            <span><i> - </i>구매 관련 거래처 매입채무관리</span>
                        </div>
                    </li>
                   
                </ul>
            </div>

            


            

            

                
        </div>
            
            

    </div> <!-- inner -->
     
   


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>