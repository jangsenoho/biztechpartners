<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>
<section id="container" class="sub about">
    <div class="inner_1200">

        <div class="sub_cont">
            <div class="ttl_box">
                <h2 class="ttl">전자공고</h2>
                <p class="desc"><span>BizTechPartners</span>의 투자관련 공시정보를 확인하실 수 있습니다.</p>
            </div>

            <div class="cont_box">
                <div class="list_search">
                    <ul>
                        <li>
                            <strong>검색기간</strong>
                            <div class="datepicker_box">
                                <input type="text" class=" datepicker01" id="list_date"  required readonly="readonly">
                                <span>~</span>
                                <input type="text" class=" datepicker02" id="list_date02"  required readonly="readonly"> 
                            </div>
                        </li>
                        <li>
                            <strong>공고제목</strong>
                            <input type="text">
                            <button type="submit">검색</button>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="cont_box table_b">
                <div class="total">총 <span>10건</span></div>

                <table>
                    <colgroup>
                        <col width="10%">
                        <col width="*">
                        <col width="15%">
                        <col width="12%">
                    </colgroup>
                    <thead>
                        <tr>
                            <th>번호</th>
                            <th>제목</th>
                            <th>등록일자</th>
                            <th>다운로드</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="num">10</td>
                            <td class="ttl"><a href="./investment_view.php">[전자공고] 2020년 비즈테크파트너스 결산공고</a></td>
                            <td class="date">2022-01-02</td>
                            <td class="down"><a href="#">PDF file <i></i></a></td>
                        </tr>
                        <tr>
                            <td class="num">9</td>
                            <td class="ttl"><a href="./investment_view.php">[전자공고] 2019년 비즈테크파트너스 결산공고</a></td>
                            <td class="date">2021-12-24</td>
                            <td class="down"><a href="#">PDF file <i></i></a></td>
                        </tr>
                        <tr>
                            <td class="num">8</td>
                            <td class="ttl"><a href="./investment_view.php">[전자공고] 기준일 및 주식 명의개서 정지 공고</a></td>
                            <td class="date">2021-11-16</td>
                            <td class="down"><a href="#">PDF file <i></i></a></td>
                        </tr>
                        <tr>
                            <td class="num">7</td>
                            <td class="ttl"><a href="./investment_view.php">[전자공고] 2018년 비즈테크파트너스 결산공고</a></td>
                            <td class="date">2021-10-15</td>
                            <td class="down"><a href="#">PDF file <i></i></a></td>
                        </tr>
                        <tr>
                            <td class="num">6</td>
                            <td class="ttl"><a href="./investment_view.php">[전자공고] 기준일 및 주식 명의개서 정지 공고</a></td>
                            <td class="date">2021-09-13</td>
                            <td class="down"><a href="#">PDF file <i></i></a></td>
                        </tr>
                        <tr>
                            <td class="num">5</td>
                            <td class="ttl"><a href="./investment_view.php">[전자공고] 2017년 비즈테크파트너스 결산공고</a></td>
                            <td class="date">2021-08-03</td>
                            <td class="down"><a href="#">PDF file <i></i></a></td>
                        </tr>
                        <tr>
                            <td class="num">4</td>
                            <td class="ttl"><a href="./investment_view.php">[전자공고] 기준일 및 주식 명의개서 정지 공고</a></td>
                            <td class="date">2021-05-22</td>
                            <td class="down"><a href="#">PDF file <i></i></a></td>
                        </tr>
                        <tr>
                            <td class="num">3</td>
                            <td class="ttl"><a href="./investment_view.php">[전자공고] 자본감소 공고(채권자 이의 제출 공고)</a></td>
                            <td class="date">2020-03-12</td>
                            <td class="down"><a href="#">PDF file <i></i></a></td>
                        </tr>
                        <tr>
                            <td class="num">2</td>
                            <td class="ttl"><a href="./investment_view.php">[전자공고] 2016년 비즈테크파트너스 결산공고</a></td>
                            <td class="date">2020-01-02</td>
                            <td class="down"><a href="#">PDF file <i></i></a></td>
                        </tr>
                        <tr>
                            <td class="num">1</td>
                            <td class="ttl"><a href="./investment_view.php">[전자공고] 2015년 비즈테크파트너스 결산공고</a></td>
                            <td class="date">2020-01-02</td>
                            <td class="down"><a href="#">PDF file <i></i></a></td>
                        </tr>

                        <!--검색결과가 없을 경우  -->
                        <tr class="search_no">
                            <td class="ttl" colspan="4">검색결과가 없습니다. <br>다른 검색어를 입력하세요</td>
                        </tr>
                    </tbody>
                </table>

                <div class="pager">
                    <button class="load_more">더보기 <i>+</i></button>
                    <ul>
                        <li class="all_pr"><a href="#"></a></li>
                        <li class="one_pr"><a href="#"></a></li>
                        <li class="num on"><a href="#">1</a></li>
                        <li class="num"><a href="#">2</a></li>
                        <li class="num"><a href="#">3</a></li>
                        <li class="num"><a href="#">4</a></li>
                        <li class="num last"><a href="#">5</a></li>
                        <li class="one_ne"><a href="#"></a></li>
                        <li class="all_ne"><a href="#"></a></li>
                    </ul>
                </div>
            </div>

         
        </div>
    </div>


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(0).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가

    $('.m_header').addClass('about')

        $('.datepicker02').on('click',function(){
            $('#ui-datepicker-div').addClass('date_asome')
        })
        $('.datepicker01').on('click',function(){
            $('#ui-datepicker-div').removeClass('date_asome')
        })
    });
</script>