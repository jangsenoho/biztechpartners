<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="about sub about_off02">
    <div class="inner_1200">

        <div class="sub_cont">
            <div class="ttl_box">
                <h2 class="ttl">오시는 길</h2>
                <p class="desc"><span>BizTechPartners</span>의 사업장을 소개합니다.</p>
            </div>

            <div class="cont_box">
                <div class="img_wrap">
                <img src="../img/sub/about_02.jpg" alt="사업장 이미지" class="only_w">
                    <img src="../img/sub/about_02_m.jpg" alt="사업장 이미지"  class="only_m">
                </div>

                <div class="map_info">
                    <strong>사업장 주소</strong>
                    <p>서울특별시 강서구 마곡중앙10로 10 W6동</p>
                    <a href="http://kko.to/CBlad3Nfj" target="_blank">지도보기 <i></i></a>
                </div>
            </div>

            <div class="cont_box">
                <div class="map_link">
                    <a href="/about/about_office.php" >본사</a>
                    <a href="/about/about_office02.php" class="on">사업장</a>
                </div>
            </div>

        
        </div>

    </div> <!-- inner -->
    
   


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(0).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('about')

    });
</script>