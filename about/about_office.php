<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="about sub about_off">
    <div class="inner_1200">

        <div class="sub_cont">
            <div class="ttl_box">
                <h2 class="ttl">오시는 길</h2>
                <p class="desc"><span>BizTechPartners</span>의 사업장을 소개합니다.</p>
            </div>

            <div class="cont_box">
                <div class="img_wrap">
                    <img src="../img/sub/about_01.jpg" alt="본사 이미지" class="only_w">
                    <img src="../img/sub/about_01_m.jpg" alt="본사 이미지"  class="only_m">
                </div>

                <div class="map_info">
                    <strong>본사 주소</strong>
                    <p>서울특별시 마포구 매봉산로 75, <br class="only_m"> 상암 DDMC  12층</p>
                    <p><span>TEL.</span> 02-2084-6700</p>
                    <a href="http://kko.to/KpaQUO2bW" target="_blank">지도보기 <i></i></a>
                </div>
            </div>

            <div class="cont_box">
                <div class="map_link">
                    <a href="/about/about_office.php" class="on">본사</a>
                    <a href="/about/about_office02.php">사업장</a>
                </div>
            </div>

            <div class="cont_box">
                <div class="transport">
                    <h3>대중교통 안내</h3>
                    <div class="tr_info">
                        <ul>
                            <li><strong>버스</strong></li>
                            <li><span>· 지선</span> - 7013, 7711, 7715, 7730, 7019</li>
                            <li><span>· 간선</span> - 470, 670, 171, 172, 271, 710, 771</li>
                            <li><span>· 광역</span> - 9711</li>
                        </ul>
                        <ul>
                            <li><strong>지하철</strong></li>
                            <li><span>· 6호선</span> - 디지털미디어시티역 (2번 출구)</li>
                            <li><span>· 경의중앙선</span> - 수색역 (1번 출구)</li>
                            <li><span>· 공항철도</span> - 디지털미디어시티역 (9번 출구)</li>
                        </ul>

                    </div>
                   
                </div>
            </div>
            
           

        </div>

    </div> <!-- inner -->
    
   


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(0).addClass('active');


        $('.m_header').addClass('about')
        
    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가

    });
</script>