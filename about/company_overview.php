<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>
<style>
    .cont_box span{
        display: block;
    }
    </style>
<section id="container" class="about sub overview ">
    <div class="inner_1200">
        <div class="sub_cont">
            <div class="ttl_box">
                <h2 class="ttl">기업개요</h2>
                <h2 class="sub_ttl"><span>Global IT 서비스</span> 전문기업</h2>
                <p class="desc">비즈테크파트너스는 Global IT 서비스 전문기업인 LG CNS 자회사로 기술전문성을 가지고 <br class="only_w">
                SAP ERP와 Web Service, 인사/경영지원 분야에서 차별화된 서비스를 제공합니다.​</p>
                <!-- BEGIN 2022 04 26 텍스트 수정  -->  
                <p class="desc">ERP, RPA, 스마트팩토리, 모바일, 클라우드 등 다양한 IT 영역에서 고객에게 차별화된 ​미래 기술변화를 선도하고<br class="only_w"> 명실상부한 최고의 IT 전문기업으로 성장하고 있습니다.​​</p>
                <!-- END 2022 04 26 텍스트 수정  --> 
            </div>

            <div class="cont_box">
                <ul class="his_overview_wrap">
                    <li>
                        <p>설립일</p>
                        <h3>2003<span>년</span></h3>
                    </li>
                    <li>
                        <p>매출액</p>
                        <h3>1,492<span>억</span></h3>
                        <span>(2021년)</span>
                    </li>
                    <li>
                        <p>영업이익</p>
                        <h3>51<span>억</span></h3>  
                        <span>(2021년)</span>
                    </li>
                    <li>
                        <p>임직원</p>
                        <h3>768<span>명</span></h3>
                        <span>(2022년 3월)</span>
                    </li>
                </ul>
            </div>

            <div class="cont_box">
                <h2 class="ttl">사업분야</h2>
                <ul class="business_list">
                    <li>
                        <a href="/sap/implementation.php" target="_blank">
                            <strong>ERP</strong>
                            <!-- BEGIN: ie오류로 인한 마크업 수정 p -> span  -->
                            <span>Global No.1 Solution인 SAP를 활용하여 다양한 Business 환경 및 전략에 부합되는 ​기업경영환경을 제공합니다.​</span>
                            <!-- END: ie오류로 인한 마크업 수정 p -> span  -->
                            <i></i>
                        </a>
                    </li>
                    <li>
                        <a href="/web/si_sm.php" target="_blank">
                            <strong>WEB Solution​</strong>
                            <!-- BEGIN: ie오류로 인한 마크업 수정 p -> span  -->
                            <span>다양한 산업에서 ICBMA 기술을 기반으로​ 고객환경에 최적화된 웹시스템을 제공합니다.</span>
                               <!-- BEGIN: ie오류로 인한 마크업 수정 p -> span  -->
                            <i></i>
                        </a>
                    </li>
                    <li>
                        <a href="/bpo/bpo_outline.php" target="_blank">
                            <strong>BPO</strong>
                            <!-- BEGIN: ie오류로 인한 마크업 수정 p -> span  -->
                            <span>고객의 업무수행을​ 지원하는 아웃소싱 ​서비스를 제공하여 비용절감과 업무효율성을 높입니다.</span>
                             <!-- BEGIN: ie오류로 인한 마크업 수정 p -> span  -->
                            <i></i>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="cont_box">
                <h2 class="ttl">연혁</h2>
                <div class="history_wrap">
                    <div>
                        <h3><span>2021년</span></h3>
                        <ul>
                            <li><i>·</i> SAP APJ Partner Excellence Award 2021 Partner Managed Cloud 수상​​​ ​</li>
                        </ul>
                    </div>
                    <div>
                        <h3><span>2019년</span></h3>
                        <ul>
                            <li><i>·</i> 국내 최초 SAP Conversion Factory 인증 획득​​ ​</li>
                        </ul>
                    </div>
                    <div>
                        <h3><span>2017년</span></h3>
                        <ul>
                            <li><i>·</i> 비앤이파트너스와 비즈테크파트너스 합병 및 사명 변경​ ​</li>
                        </ul>
                    </div>
                    <div>
                        <h3><span>2014년</span></h3>
                        <ul>
                            <li><i>·</i> APJ Partner Excellence Award 2014 Rapid Deployment Innovation 수상​ ​</li>
                        </ul>
                    </div>
                    <div>
                        <h3><span>2013년</span></h3>
                        <ul>
                            <li><i>·</i> ERP RDS Solution “Exprism” – Live 인증 ​</li>
                            <li><i>·</i> SAP BW on HANA Migration RDS Solution 국내 최초 BI RDS 인증​ ​</li>
                            <li><i>·</i> Exprism™상표권 출원​ ​</li>
                        </ul>
                    </div>
                    <div>
                        <h3><span>2011년</span></h3>
                        <ul>
                            <li><i>·</i> SAP APJ 최초 한국유일 Top 10 파트너 진입</li>
                        </ul>
                    </div>
                    <div>
                        <h3><span>2010년</span></h3>
                        <ul>
                            <li><i>·</i> SAP GLOBAL AWARD – pinnacle Award 2010 The Finalist 수상​</li>
                        </ul>
                    </div>
                    <div>
                        <h3><span>2009년</span></h3>
                        <ul>
                            <li><i>·</i> 한국 SAP 파트너 Award 점유율 50% 달성​</li>
                            <li><i>·</i> SAP APJ Top Partner 수상​​</li>
                        </ul>
                    </div>
                    <div>
                        <h3><span>2008년</span></h3>
                        <ul>
                            <li><i>·</i> SAP Business All-in-One Baseline을 기반으로한 Exprism™솔루션 개발​</li>
                            <li><i>·</i> SAP Business All-in-One FSP 공식 파트너 선정​</li>
                        </ul>
                    </div>
                    <div>
                        <h3><span>2007년</span></h3>
                        <ul>
                            <li><i>·</i> Enterprise reporting 선두주자 Actuate 국내 LG그룹 투자유치</li>
                            <li><i>·</i> LG CNS 자회사로 계열 편입</li>
                        </ul>
                    </div>
                    <div>
                        <h3><span>2006년</span></h3>
                        <ul>
                            <li><i>·</i> (주)LG CNS SAP 특화업체 선정</li>
                            <li><i>·</i> SAP Gold Partner 선정</li>
                        </ul>
                    </div>
                    <div>
                        <h3><span>2005년</span></h3>
                        <ul>
                            <li><i>·</i> EIS Package 개발</li>
                            <li><i>·</i> SK C&C(주) 자재/서비스 협력회사 선정</li>
                            <li><i>·</i> 서울지방중소기업청 - 연구기술개발 벤처기업으로 인정</li>
                        </ul>
                    </div>
                    <div>
                        <h3><span>2004년</span></h3>
                        <ul>
                            <li><i>·</i> 독일 arcplan Information Services AG사와 제품공급 계약</li>
                            <li><i>·</i> 독일 arcplan Information Service AH사로부터 DynaSight Certified Company 선정</li>
                        </ul>
                    </div>
                    <div>
                        <h3><span>2003년</span></h3>
                        <ul>
                            <li><i>·</i>  ㈜LG CNS 제품/용역 협력 선정</li>
                            <li><i>·</i>  Hi-Tech 산업 지원을 위한 SAP Based 구축 Solution 발표</li>
                            <li><i>·</i>  SAP Hi-Tech SMB VAR 계약​</li>
                        </ul>
                    </div>
                </div>
            </div>
            
           

        </div>

    </div> <!-- inner -->
    
   


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(0).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가


    $('.m_header').addClass('about')
    });
    
</script>