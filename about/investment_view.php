<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>
<section id="container" class="sub about view">
    <div class="inner_1200"> 

        <div class="sub_cont">
            <div class="ttl_box">
                <h2 class="ttl">전자공고</h2>
            </div>

            <div class="cont_box">
                <div class="view_wrap">
                    <div class="view_top">
                        <h3>[전자공고] 2018년 비즈테크파트너스 결산공고</h3>
                        <div><span>2022-01-02</span></div>
                    </div>
                    <div class="view_board">
                        <span>2018년 비즈테크파트너스 결산공고 입니다.</span>

                        <div class="down">
                            <a href="#">2018년 비즈테크파트너스 결산공고.pdf <i></i></a>
                        </div>
                        
                    </div> 
                    <div class="btn_wrap">
                        <button type="button" class="back_btn" onclick="history.back();">목록으로</button>
                    </div>
                    
                </div>
            </div>

            <div class="cont_box pr_view_wrap">
                <ul class="pr_view">
                    <li class="pr">
                        <a href="#">
                            <div class="red button"><i></i> 이전글</div>
                            <div class="pr_ttl">[전자공고] 기준일 및 주식 명의개서 정지 공고</div>
                            <div class="pr_date">2021-01-05</div>
                        </a>
                    </li>
                    <li class="ne">
                        <a href="#">
                            <div class="button"><i></i> 다음글</div>
                            <div class="pr_ttl">[전자공고] 기준일 및 주식 명의개서 정지 공고</div>
                            <div class="pr_date">2021-07-03</div>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="cont_box">
                <div class="go_center">
                    <h3><span>BizTech</span>에 궁금하신 사항이 있으신가요?</h3>
                    <a href="../business/business_question.php">고객문의 바로가기 <i></i></a>
                </div>
                <div class="img_wrap">
                    <img src="../img/sub/about_view.png" alt="" class="only_w">
                    <img src="../img/sub/about_view_m.jpg" alt="" class="only_m">
                </div>
            </div>

         
        </div>
    </div>


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(0).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가

        $('.m_header').addClass('about')
    });
</script>