<body>
    <div id="wrap">
        <div class="alert_ment">
            <span>차별화된 IT 서비스로 이해관계자와의 소통과 신뢰를 바탕으로 지속 가능한 기업으로 성장하겠습니다.
            </span>
            <button type="button" class="close"></button>
        </div>
        <header id="header">
            <h1 class="logo">
                <a href="/">
                    <img src="../img/common/logo_wh.png" alt="비즈테크">
                </a>
            </h1>
            <nav id="gnb">
                <ul>
                    <li class="active">
                        <a href="#">ERP</a>
                        <ul class="submenu">
                            <li><a href="#">ERP PI/ISP Consulting</a></li>
                            <li><a href="#">Cloud</a></li>
                            <li><a href="#">ERP Inplementation</a></li>
                            <li><a href="#">ERP Maintenance</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">ICT Servicev</a>
                        <ul class="submenu">
                            <li><a href="#">SI/SM</a></li>
                            <li><a href="#">Solution</a></li>
                            <li><a href="#">Special Business</a></li>
                            <li><a href="#">Data Analysis</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">BPO</a>
                        <ul class="submenu">
                            <li><a href="#">HR BPO</a></li>
                            <li><a href="#">경영지원 BPO Service</a></li>
                            <li><a href="#">재무 BPO Service</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Media News</a>
                        <ul class="submenu">
                            <li><a href="#">공지사항</a></li>
                            <li><a href="#">보도자료</a></li>
                            <li><a href="#">브루슈어</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">About Biztech</a>
                        <ul class="submenu">
                            <li><a href="#">회사소개</a></li>
                            <li><a href="#">BizTech 고객사</a></li>
                            <li><a href="#">Vision & Mission</a></li>
                            <li><a href="#">오시는 길</a></li>
                            <li><a href="#">채용정보</a></li>
                            <li><a href="#">고객문의</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Announcement</a>
                        <ul class="submenu">
                            <li><a href="#">전자공고</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
            <div class="hd_right">
                <a href="#" class="hd_btn">BIZ-care<span>3.0</span></a>
                <button class="ham_btn"></button>
            </div>
            <div class="all_menu">
                <ul class="menu_list">
                    <li>
                        <p class="category">ERP</p>
                        <ul class="depth02">
                            <li><a href="#">ERP PI/ISP Consulting</a></li>
                            <li><a href="#">Cloud</a></li>
                            <li><a href="#">ERP Inplementation</a></li>
                            <li><a href="#">ERP Maintenance</a></li>
                        </ul>
                    </li>
                    <li>
                        <p class="category">ICT Servicev</p>
                        <ul class="depth02">
                        <li><a href="#">SI/SM</a></li>
                            <li><a href="#">Solution</a></li>
                            <li><a href="#">Special Business</a></li>
                            <li><a href="#">Data Analysis</a></li>
                        </ul>
                    </li>
                    <li>
                        <p class="category">BPO</p>
                        <ul class="depth02">
                            <li><a href="#">HR BPO</a></li>
                            <li><a href="#">경영지원 BPO Service</a></li>
                            <li><a href="#">재무 BPO Service</a></li>
                        </ul>
                    </li>
                    <li>
                        <p class="category">Media News</p>
                        <ul class="depth02">
                        <li><a href="#">공지사항</a></li>
                            <li><a href="#">보도자료</a></li>
                            <li><a href="#">브루슈어</a></li>
                        </ul>
                    </li>
                    <li>
                        <p class="category">About Biztech</p>
                        <ul class="depth02">
                        <li><a href="#">회사소개</a></li>
                            <li><a href="#">BizTech 고객사</a></li>
                            <li><a href="#">Vision & Mission</a></li>
                            <li><a href="#">오시는 길</a></li>
                            <li><a href="#">채용정보</a></li>
                            <li><a href="#">고객문의</a></li>
                        </ul>
                    </li>
                    <li>
                        <p class="category">Announcement</p>
                        <ul class="depth02">
                        <li><a href="#">전자공고</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </header>