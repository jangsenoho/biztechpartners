<body>
    <div id="wrap">
        <div class="alert_ment">
            <div class="inner">
                <span class="only_w">차별화된 IT 서비스로 이해관계자와의 소통과 신뢰를 바탕으로 지속 가능한 기업으로 성장하겠습니다.</span>
                <MARQUEE class="only_m"  scrollamount=40 scrolldelay=1000   behavior=alternate>차별화된 IT 서비스로 이해관계자와의 소통과 신뢰를 바탕으로 지속 가능한 기업으로 성장하겠습니다.</MARQUEE>
                <button type="button" class="close" onclick="closeBanner();">
                    <img src="../img/btn/al_close.png" alt="닫기" class="only_w">
                    <img src="../img/btn/top_pop_close.png" alt="닫기" class="only_m">
                </button>
            </div>
        </div>
        <header id="header">
            <!-- header_gnb -->
            <div class="heaer_top">
                <div class="inner">
                    <h1 class="logo">
                        <a href="/">
                            <img src="../img/common/logo_bk.png" alt="비즈테크 로고">
                        </a>
                    </h1>
                    <nav id="gnb">
                        <ul>
                            <li> 
                                <a href="/about/company_overview.php">About Biztech</a>
                                <ul class="submenu">
                                    <li><a href="/about/company_overview.php">기업개요</a></li>
                                    <li><a href="/about/about_office.php">오시는 길</a></li>
                                    <li><a href="/about/investment_list.php">전자공고</a></li>
                                    <li><a href="/about/business_customer01.php">고객사</a></li>
                                </ul>
                            </li>
                            <li class="">
                                <a href="/sap/sap_consulting.php">비즈니스</a>
                                <ul class="submenu">
                                    <li><a href="/sap/sap_consulting.php">SAP</a></li>
                                    <li><a href="/web/si_sm.php">WEB</a></li>
                                    <li><a href="/bpo/bpo_outline.php">BPO</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="/people/people.php">인재채용</a>
                                <ul class="submenu">
                                    <li><a href="/people/people.php">인재상</a></li>
                                    <li><a href="/people/people_system01.php">인사제도</a></li>
                                    <li><a href="/people/it_part.php">직무소개</a></li>
                                    <li><a href="/people/people_notice.php">채용공고</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="/business/business_question.php">고객문의</a>
                            </li>
                        </ul>
                    </nav>
                    <div class="hd_right">
                          <!-- BEGIN 2022 04 26 텍스트 수정  --> 
                        <a href="https://bizcare.biztechpartners.co.kr/login/form" class="hd_btn" target="_blank">BIZ-care<span>3.0</span></a>
                        <!-- END 2022 04 26 텍스트 수정  -->  
                        <!-- <form action="">
                            <fieldset>
                                <input type="text">
                                <input type="button">
                            </fieldset>
                        </form> -->
                        
                        <button class="ham_btn"></button>
                    </div>
                </div>
            </div>
            <!-- ------------------------------------------------ // header_gnb -->
            
            <!-- 웹 전체메뉴 -->
            <div class="all_menu">
                <ul class="menu_list">
                    <li> 
                        <p class="category"><span>About Biztech</span></p>
                        <ul class="depth02">
                            <li class="dep_02"><a href="/about/company_overview.php">기업개요</a></li>
                            <li class="dep_02"><a href="/about/about_office.php">오시는 길</a></li>
                            <li class="dep_02"><a href="/about/investment_list.php">전자공고</a></li>
                            <li class="dep_02"><a href="/about/business_customer01.php">고객사</a>
                                <ul class="depth03">
                                    <li class="dep_03"><a href="/about/business_customer01.php">LG 그룹사</a></li>
                                    <li class="dep_03"><a href="/about/business_customer02.php">기업</a></li>
                                    <li class="dep_03"><a href="/about/business_customer03.php">공공 및 기타</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="">
                        <p class="category"><span>비즈니스</span></p>
                        <ul class="depth02">
                            <li class="dep_02">
                                <a href="/sap/sap_consulting.php">SAP</a>
                                <ul class="depth03">
                                    <li class="dep_03"><a href="/sap/sap_consulting.php">SAP PI/ISP Consulting</a></li>
                                    <li class="dep_03"><a href="/sap/cloud.php">Cloud</a></li>
                                    <li class="dep_03"><a href="javascript:;" class="bold_dep">SAP Implementation</a>
                                        <ul class="depth04">
                                            <li class="dep_04"><a href="/sap/implementation.php">Exprism ERP</a></li>
                                            <li class="dep_04"><a href="/sap/sap_conversion.php">SAP S/4HANA Conversion</a></li>
                                            <li class="dep_04"><a href="/sap/biz_res.php">Biz-RES</a></li>
                                            <li class="dep_04"><a href="/sap/biz_yts.php">Biz-YTS​</a></li>
                                            <li class="dep_04"><a href="/sap/u4a_platform.php">U4A IDE Platform​</a></li>
                                            <li class="dep_04"><a href="/sap/sap_bw.php">SAP BW​</a></li>
                                            <li class="dep_04"><a href="/sap/next_hr.php">NEXT-HR​</a></li>
                                        </ul>
                                    </li>
                                    <li class="dep_03"><a href="javascript:;" class="bold_dep">SAP Maintenance</a>
                                        <ul class="depth04">
                                            <li  class="dep_04"><a href="/sap/biz_care.php">BIZcare <i>3.0</i></a></li>
                                            <li  class="dep_04"><a href="/sap/biz_rms.php">BiZ-RMS​</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="dep_02">
                                <a href="/web/si_sm.php">WEB</a>
                                <ul class="depth03">
                                    <li class="dep_03"><a href="/web/si_sm.php">SI/SM</a></li>
                                    <li class="dep_03"><a href="javascript:;" class="bold_dep">RPA</a>
                                        <ul class="depth04">
                                            <li class="dep_04"><a href="/web/rpa_01.php">RPA</a></li>
                                            <li class="dep_04"><a href="/web/rpa_02.php">RPA 대표사례</a></li>
                                        </ul>
                                    </li>
                                    <li class="dep_03"><a href="javascript:;" class="bold_dep">Smart Factory</a>
                                        <ul class="depth04">
                                            <li class="dep_04"><a href="/web/smart_factory_01.php">Over view</a></li>
                                            <li class="dep_04"><a href="/web/smart_factory_02.php">스마트 물류</a></li>
                                        </ul>
                                    </li>
                                    <li class="dep_03"><a href="javascript:;" class="bold_dep">Data Analysis</a>
                                        <ul class="depth04">
                                            <li class="dep_04"><a href="/web/data_analysis_01.php">Big Data</a></li>
                                            <li class="dep_04"><a href="/web/data_analysis_02.php">DW</a></li>
                                            <li class="dep_04"><a href="/web/data_analysis_03.php">Dashboard</a></li>
                                        </ul>
                                    </li>
                                    <li class="dep_03"><a href="javascript:;" class="bold_dep">Cloud /Infra / IT보안</a>
                                        <ul class="depth04">
                                            <li class="dep_04"><a href="/web/cloud_infra.php">Cloud /Infra</a></li>
                                            <li class="dep_04"><a href="/web/it_security.php">IT 보안 </a></li>
                                        </ul>
                                    </li>
                                    <li class="dep_03"><a href="/web/eap.php">EAP</a></li>
                                </ul>
                            </li>
                            <li class="dep_02">
                                <a href="/bpo/bpo_outline.php">BPO</a>
                                <ul class="depth03">
                                    <li class="dep_03"><a href="/bpo/bpo_outline.php">BPO 개요</a></li>
                                    <li class="dep_03"><a href="javascript:;" class="bold_dep">HP BPO Service</a>
                                        <ul class="depth04">
                                            <li class="dep_04"><a href="/bpo/hr_service01.php">개요</a></li>
                                            <li class="dep_04"><a href="/bpo/hr_service02.php">급여/복리 후생</a></li>
                                            <li class="dep_04"><a href="/bpo/hr_service03.php">교육운영</a></li>
                                            <li class="dep_04"><a href="/bpo/hr_service04.php">생애설계 교육/컨설팅</a></li>
                                            <li class="dep_04"><a href="/bpo/hr_service05.php">CCS</a></li>
                                            <li class="dep_04"><a href="/bpo/hr_service06.php">채용행정</a></li>
                                            <li class="dep_04"><a href="/bpo/hr_service07.php">연말정산</a></li>
                                        </ul>
                                    </li>
                                    <li class="dep_03"><a href="javascript:;" class="bold_dep">경영지원 BPO Service</a>
                                        <ul class="depth04">
                                            <li class="dep_04"><a href="/bpo/operation_service.php">서비스 영역</a></li>
                                            <li class="dep_04"><a href="/bpo/operation_it.php">자산관리/IT인프라 공급</a></li>
                                            <li class="dep_04"><a href="/bpo/operation_material.php">구매/자재관리</a></li>
                                            <li class="dep_04"><a href="/bpo/operation_privacy.php">개인정보보호</a></li>
                                            <li class="dep_04"><a href="/bpo/operation_nomal.php">총무일반</a></li>
                                        </ul>
                                    </li>
                                    <li class="dep_03"><a href="javascript:;" class="bold_dep">재무 BPO Service</a>
                                        <ul class="depth04">
                                            <li class="dep_04"><a href="/bpo/finance_01.php">서비스 영역</a></li>
                                            <li class="dep_04"><a href="/bpo/finance_02.php">재무.관리결산/회계컨설팅</a></li>
                                            <li class="dep_04"><a href="/bpo/finance_03.php">어카운팅 업무</a></li>
                                            <li class="dep_04"><a href="/bpo/finance_04.php">AR/AP 업무</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <p class="category"><span>인재채용</span></p>
                        <ul class="depth02">
                            <li class="dep_02"><a href="/people/people.php">인재상</a></li>
                            <li class="dep_02"><a href="/people/people_system01.php">인사제도</a>
                                <ul class="depth03">
                                    <li class="dep_03"><a href="/people/people_system01.php">인사원칙</a></li>
                                    <li class="dep_03"><a href="/people/people_system02.php">복리후생</a></li>
                                    <li class="dep_03"><a href="/people_system03.php">교육체계</a></li>
                                </ul>
                            </li>
                            <li class="dep_02"><a href="/people/it_part.php">직무소개</a>
                                <ul class="depth03">
                                    <li class="dep_03"><a href="/people/it_part.php">IT</a></li>
                                    <li class="dep_03"><a href="/people/bpo_part.php">BPO</a></li>
                                    <li class="dep_03"><a href="/people/staff_part.php">STAFF</a></li>
                                </ul>
                            </li>
                            <li class="dep_02"><a href="/people/people_notice.php">채용공고</a></li>
                        </ul>
                    </li>
                    <li>
                        <p class="category"><span>고객문의</span></p>
                        <ul class="depth02">
                            <li class="dep_02"><a href="/business/business_question.php">사업문의</a></li>
                           
                        </ul>
                    </li>
                    <li>
                        <p class="category"><span>Footer</span></p>
                        <ul class="depth02">
                            <li class="dep_02"><a href="/main/privacy_policy.php">개인정보처리방침</a></li>
                            <li class="dep_02"><a href="/main/conditions.php">이용약관</a></li>
                            <li class="dep_02"><a href="#">Family Site</a>
                                <ul class="depth03">
                                    <li class="dep_03"><a href="https://www.btpp.co.kr/lpp/lppLogin " target="_blank">Partner Plus</a></li>
                                    <li class="dep_03"><a href="https://www.lgcns.co.kr" target="_blank">LG CNS</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
        
                </ul>
                
            </div>
            <!-- ----------------------------- //웹 전체메뉴 -->

            <!-- 모바일 헤더 -->
            <div class="m_header">
                <div class="top_wrap">
                    <div class="m_h_top">
                        <h1 class="logo">
                            <a href="/"><img src="../img/common/logo_bk_m.png" alt=""></a>
                        </h1>
                        <button class="now_txt"></button>
                        <div class="top_rigth">
                            <!-- <button class="search" ></button> -->
                            <button class="m_menu"></button>
                        </div>
                        <!-- <div class="search_area">
                            <form action="">
                                <fieldset>
                                    <input type="text" placeholder="검색어를 입력해주세요">
                                    <input type="button">
                                </fieldset>
                            </form>
                        </div> -->
                        
                    </div>
                </div>
                <ul class="m_h_bot">
                    <li><a href="/about/company_overview.php">About Biztech</a></li>
                    <li><a href="/sap/sap_consulting.php">비즈니스</a></li>
                    <li><a href="/people/people.php">인재채용</a></li>
                    <li><a href="/business/business_question.php">고객문의​</a></li>
                </ul>
              
            </div>

            <!-- 모바일 전체메뉴 -->
            <div class="m_all_menu">
                <div class="all_top">
                    <h1 class="logo logo_show ">
                        <a href="/"><img src="../img/common/logo_bk_m.png" alt=""></a>
                    </h1>
                    <button class="btn_close"></button>
                </div>
                <ul class="m_gnb">
                    <li> 
                        <p>About Biztech</p>
                        <ul class="submenu">
                            <li><a href="/about/company_overview.php">기업개요</a></li>
                            <li>
                                <a href="/about/about_office.php" class="m_dep02">오시는 길</a>
                                <ul class="m_dep_03">
                                    <li><a href="/about/about_office.php">본사</a></li>
                                    <li><a href="/about/about_office02.php">사업장</a></li>
                                </ul>
                            </li>
                            <li><a href="/about/investment_list.php">전자공고</a></li>
                            <li>
                                <a href="/about/business_customer01.php" class="m_dep02">고객사</a>
                                <ul class="m_dep_03">
                                    <li><a href="/about/business_customer01.php">LG 그룹사</a></li>
                                    <li><a href="/about/business_customer02.php">기업</a></li>
                                    <li><a href="/about/business_customer03.php">공공 및 기타</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="">
                        <p>비즈니스</p>
                        <ul class="submenu">
                            <li>
                                <a href="/sap/sap_consulting.php" class="m_dep02">SAP</a>
                                <ul class="m_dep_03">
                                    <li><a href="/sap/sap_consulting.php">SAP PI/ISP Consulting</a></li>
                                    <li><a href="/sap/cloud.php">Cloud</a></li>
                                    <li><a href="/sap/implementation.php">SAP Implementation</a></li>
                                    <li><a href="/sap/biz_care.php">SAP Maintenance</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="/web/si_sm.php" class="m_dep02">WEB</a>
                                <ul class="m_dep_03">
                                    <li><a href="/web/si_sm.php">SI/SM</a></li>
                                    <li><a href="/web/rpa_01.php">RPA</a></li>
                                    <li><a href="/web/smart_factory_01.php">Smart Factory</a></li>
                                    <li><a href="/web/data_analysis_01.php">Data Analysis</a></li>
                                    <li><a href="/web/cloud_infra.php">Cloud /Infra / IT보안</a></li>
                                    <li><a href="/web/eap.php">EAP</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="/bpo/bpo_outline.php"  class="m_dep02">BPO</a>
                                <ul class="m_dep_03">
                                    <li><a href="/bpo/bpo_outline.php">BPO 개요</a></li>
                                    <li><a href="/bpo/hr_service01.php">HP BPO Service</a></li>
                                    <li><a href="/bpo/operation_service.php">경영지원 BPO Service</a></li>
                                    <li><a href="/bpo/finance_01.php">재무 BPO Service</a> </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <p>인재채용</p>
                        <ul class="submenu">
                            <li><a href="/people/people.php">인재상</a></li>
                            <li>
                                <a href="/people/people_system01.php"  class="m_dep02">인사제도</a>
                                <ul class="m_dep_03">
                                    <li><a href="/people/people_system01.php">인사원칙</a></li>
                                    <li><a href="/people/people_system02.php">복리후생</a></li>
                                    <li><a href="/people_system03.php">교육체계</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="/people/it_part.php"  class="m_dep02">직무소개</a>
                                <ul class="m_dep_03">
                                    <li><a href="/people/it_part.php">IT</a></li>
                                    <li><a href="/people/bpo_part.php">BPO</a></li>
                                    <li><a href="/people/staff_part.php">STAFF</a></li>
                                </ul>
                            </li>
                            <li><a href="/people/people_notice.php">채용공고</a></li>
                        </ul>
                    </li>
                    <li>
                        <p>고객문의</p>
                        <ul class="submenu">
                            <li><a href="/business/business_question.php">사업문의</a></li>
                          
                        </ul>
                    </li>
                    <li>
                        <p><a href="https://bizcare.biztechpartners.co.kr/login/form" class="hd_btn" target="_blank">BIzcare<sup>3.0</sup></a></p>
                       
                    </li>
                </ul>
            </div>
        </header>

<script>

    $(function(){

        //--------------------전체메뉴 오픈 
        $('header .m_menu').on('click', function() {
            $('.m_all_menu').fadeIn()

        });

        $('.btn_close').on('click', function() {
            
            $('header .m_all_menu').fadeOut();
            $('.m_all_menu.site').show();
          
          
        })

  
        $('.m_dep02').click(function(e){ 
            e.preventDefault();  
      })


    // -------------------------- 모바일 아코디언 메뉴
    $('.m_all_menu .m_gnb > li .m_dep02').on('click',function(){
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $(this).siblings('.m_dep_03').slideUp();
        } else {
            $('.m_all_menu .m_gnb .m_dep_03').slideUp();
            $('.m_all_menu .m_gnb > li .m_dep02').removeClass('active');
            $(this).addClass('active');
            $(this).siblings('.m_dep_03').slideDown();
        }
    });

    $('.m_header .top_rigth .search').on('click',function(){
        $(this).toggleClass('se_open');
        $('.m_h_top .search_area').toggle()
    })


    
    })
  

</script>

       