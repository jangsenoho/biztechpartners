<!DOCTYPE html>
<html lang="ko">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="format-detection" content="telephone=no, address=no, email=no">
    <meta content="비즈테크" name="title">
    <meta content="비즈테크" name="Keyword">
    <meta content="비즈테크" name="Description">
    <meta property="og:title" content="비즈테크">
    <meta property="og:description" content="비즈테크">
    <meta property="og:url" content="http://biztech.uwaldev.com">
    <meta property="og:image" content="http://biztech.uwaldev.com/img/common/biztech_og.png">
    <link href="../css/reset.css" rel="stylesheet" type="text/css">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
    <link href="../css/sub.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet"href="https://unpkg.com/swiper/swiper-bundle.min.css" />

    <link href="../img/common/favicon_bizt.ico" rel="icon" type="image/x-icon">
    
    <script src="../js/jquery-2.1.4.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../js/swiper.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-rwdImageMaps/1.6/jquery.rwdImageMaps.js"></script>
    <script src="../js/script.js" type="text/javascript"></script>
    <title>비즈테크</title>
</head>