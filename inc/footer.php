<ul class="m_h_bot f_nav">
    <li><a href="/about/company_overview.php">About Biztech</a></li>
    <li><a href="/sap/sap_consulting.php">비즈니스</a></li>
    <li><a href="/people/people.php">인재채용</a></li>
    <li><a href="/business/business_question.php">고객문의​</a></li>
</ul>

<footer>
    <div class="ft_top">
        <ul class="inner_1200">
            <li>
                <a href="/main/privacy_policy.php">개인정보 처리방침</a>
            </li>
            <li>
                <a href="/main/conditions.php">이용약관</a>
            </li>
        </ul>
    </div>
    <div class="ft_bot inner_1200">

        <h1 class="logo only_w">
            <a href="/"><img src="../img/common/logo_ft.png" alt="비즈테크"></a>
        </h1>
        <div class="info">
            <ul class="info_top">
                <li><a href="/main/site_map.php">사이트맵</a></li>
                <li><a href="/business/business_question.php">고객문의</a></li>
                <li><a href="/about/about_office.php">오시는길</a></li>
               
            </ul>
            <address>
            <div>
                <!-- BEGIN 2022 04 26 텍스트 수정  -->  
                <span>(주)비즈테크파트너스 </span> <br class="only_m">
                <!-- END 2022 04 26 텍스트 수정  -->  
                <span>서울시 마포구 상암동 매봉산로 75 DDMC 12층</span>
            </div>
            <div>
                <span>대표이사 권주오</span>
                <span>사업자번호 220-86-55078</span> <br class="only_m">
                <span>TEL 02-2084-6700</span>
                <span>FAX 02-2084-6701</span>
            </div>
            </address>
            <p class="copy">copyright© BiztechPartners. All Rights Reserved.</p>
        </div>
        <div class="m_f_logo only_m">
            <h1 class="logo ">
                <a href="/"><img src="../img/common/logo_ft_m.png" alt="비즈테크"></a>
            </h1>
        </div>
        
        <div class="sel_box">
            <select class="family_site" onchange="window.open(value,'_blank');">
                <option value="#">Family Site </option>
                <option value="https://www.btpp.co.kr/lpp/lppLogin">Partner Plus </option>
                <option value="https://www.lgcns.co.kr/">LG CNS </option>
            </select>
        </div>
        
    </div>
</footer>
</div> <!-- wrap -->

</body>

</html>