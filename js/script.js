/* $(document).ready(function(){

}) */

// 얼럿팝업
function closeBanner() {
  $('.alert_ment').hide()
  $('.m_all_menu').addClass('none');
}

$(window).scroll(function(){ 
    var height = $(document).scrollTop(); //실시간으로 스크롤의 높이를 측정
    if(height > 120){ 
    $('.f_nav').addClass('fixed'); 
    } else if(height < 120){ 
    $('.f_nav').removeClass('fixed'); 
    } 
});





$(function () {

  


  // 전체메뉴
  $('#gnb ul li').on('mouseenter', function () {
    $(this).addClass('on')
  })
  $('#gnb ul li').on('mouseleave', function () {
    $(this).removeClass('on')
  })
  $('.hd_right .ham_btn').on('click', function () {
    $('.all_menu').toggleClass('open');
    $('.alert_ment').toggle();
    $('header').toggleClass('all_open');
    $('.hd_right .ham_btn').toggleClass('open');
    $('html, body').toggleClass('hidden');
  })

//   $('.hd_right .ham_btn.open').on('click', function () {
//     $('.alert_ment').show();
//     $('html, body').removeClass('hidden');
//   })


  $('.all_menu .bold_dep').on('click',function(){
      $(this).parent('.dep_03').find('.depth04').slideToggle()
    })



  // 메인스와이프
  var myArray = ["About Biztech", "인재채용", "SAP ERP", "수상내역", "RPA", "Cloud/Infra", "BPO"];

  var mainSlide = new Swiper(".main_viaual", {
    slidesPerView: 1,
    spaceBetween: 0,
    loop: true,
    autoplay: {
      delay: 7000,
      disableOnInteraction: false,
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
      renderBullet: function (index, className) {
        return '<div class="' + className + '_wrap' + '">' + '<div class="' + className + '"></div><span>' + (myArray[index]) + '</span>' + '</div>';
      }
    },
  });

  // 스와이프 정지버튼 
  $('.sw_play').on('click', function () {
    if ($(this).hasClass('on') == true) {
      mainSlide.autoplay.start();
      $(this).removeClass('on');

    } else {
      mainSlide.autoplay.stop();
      $(this).addClass('on');
    };
  });

  // datepick
  $(".datepicker01").datepicker({
    dateFormat: 'yy-mm-dd',
    showOn: "both",
    showMonthAfterYear: true,
    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
    dayNames: ['일', '월', '화', '수', '목', '금', '토'],
    dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
    dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
    ignoreReadonly: true
  });



  $('.datepicker01').datepicker('setDate', 'today');
  //(-1D:하루전, -1M:한달전, -1Y:일년전), (+1D:하루후, -1M:한달후, -1Y:일년후)           

  $(".datepicker02").datepicker({
    dateFormat: 'yy-mm-dd',
    showOn: "both",
    showMonthAfterYear: true,
    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
    dayNames: ['일', '월', '화', '수', '목', '금', '토'],
    dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
    dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
    ignoreReadonly: true
  });



  $('.datepicker02').datepicker('setDate', 'today');
  //(-1D:하루전, -1M:한달전, -1Y:일년전), (+1D:하루후, -1M:한달후, -1Y:일년후)            




  // -----------------------모바일 이미지 팝업

  if ($(window).width() < 850) {
    // 	이미지 클릭시 해당 이미지 모달
    $(".img_wrap.show").click(function () {
      $(".modal").show();
      // 해당 이미지 가겨오기
      var imgSrc = $(this).children("img").attr("src");
      var imgAlt = $(this).children("img").attr("alt");
      $(".modalBox img").attr("src", imgSrc);
      $(".modalBox img").attr("alt", imgAlt);

      // 해당 이미지 텍스트 가져오기
      var imgTit = $(this).children("p").text();
      $(".modalBox p").text(imgTit);

      // 해당 이미지에 alt값을 가져와 제목으로
      //$(".modalBox p").text(imgAlt);
    });

    //.modal안에 button을 클릭하면 .modal닫기
    $(".modal button").click(function () {
      $(".modal").hide();
    });

    //.modal밖에 클릭시 닫힘
    $(".modal").click(function (e) {
      if (e.target.className != "modal") {
        return false;
      } else {
        $(".modal").hide();
      }
    });

  }else {
    $(".modal").hide();
  }


});