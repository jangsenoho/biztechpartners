<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>
<section id="container" class="main">
    <div class="swiper-container main_viaual">
        <div class="swiper-wrapper">

            <!-- about biz -->
            <div class="swiper-slide">
                <span style="background-image:url(../img/main/main_visual_02_bg.jpg)" class="only_w">
                    <div class="main_text">
                        <img src="../img/main/main_visual_02_txt.png" alt="" class="text_01">
                        <a href="/about/company_overview.php">
                            <img src="../img/main/main_visua_more.png" alt="">
                        </a>
                    </div>
                </span>
                <span style="background-image:url(../img/main/main_visual_01_m.png)" class="only_m">
                    <div class="main_text">
                       <h3>고객의 혁신과 성장을 <br>선도하는 최고의 IT파트너</h3>
                        <a href="/about/company_overview.php">
                            자세히 보기 <i></i>
                        </a>
                    </div>
                </span>
            </div>

            <!-- 인재채용 -->
            <div class="swiper-slide">
                <span style="background-image:url(../img/main/main_visual_03_bg.png)"  class="only_w">
                    <div class="main_text">
                        <img src="../img/main/main_visual_03_txt.png" alt="" class="text_01">
                        <a href="/people/people.php">
                            <img src="../img/main/main_visua_more.png" alt="">
                        </a>
                    </div>
                </span>
                <span style="background-image:url(../img/main/main_visual_02_m.png)" class="only_m">
                    <div class="main_text">
                        <h3>최고의 인재들이 모여<br>일등의 꿈을 실현합니다</h3>
                        <a href="/people/people.php">
                             자세히 보기 <i></i>
                        </a>
                    </div>
                </span>
            </div>

              <!-- SAP -->
              <div class="swiper-slide">
                <span style="background-image:url(../img/main/main_visual_06_bg.png)" class="only_w">
                    <div class="main_text">
                        <img src="../img/main/main_visual_06_txt.png" alt="" class="text_01">
                        <img src="../img/main/main_visual_06-1_txt.png" alt="" class="text_02">
                        <a href="/sap/sap_consulting.php">
                            <img src="../img/main/main_visua_more.png" alt="">
                        </a>
                    </div>
                </span>
                <span style="background-image:url(../img/main/main_visual_07_bg_m.png)" class="only_m">
                    <div class="main_text">
                        <h3>성공적인 비즈니스 <br>혁신을 위한 최고의 선택!</h3>
                        <a href="/sap/sap_consulting.php">
                             자세히 보기 <i></i>
                        </a>
                    </div>
                </span>
            </div>

             <!-- 수상내역 -->
             <div class="swiper-slide">
                <span style="background-image:url(../img/main/main_visual_08_bg.jpg)" class="only_w">
                    <div class="main_text">
                        <img src="../img/main/main_visual_08_txt.png" alt="" class="text_01">
                        <img src="../img/main/main_visual_08-01_txt.png" alt="" class="text_02">
                        <a href="/about/company_overview.php">
                            <img src="../img/main/main_visua_more.png" alt="">
                        </a>
                    </div>
                </span>
                <span style="background-image:url(../img/main/main_visual_08_bg_m.png)" class="only_m">
                    <div class="main_text">
                        <h3>ERP 서비스 <br>인증 및 수상</h3>
               
                        <a href="/about/company_overview.php">
                             자세히 보기 <i></i>
                        </a>
                    </div>
                </span>
            </div>

            


            <!-- RPA -->
            <div class="swiper-slide">
                <span style="background-image:url(../img/main/main_visual_04_Bg.png)" class="only_w">
                    <div class="main_text">
                        <img src="../img/main/main_visual_04_txt.png" alt="" class="text_01">
                        <img src="../img/main/main_visual_04-1_txt.png" alt="" class="text_02">
                        <a href="/web/rpa_01.php">
                            <img src="../img/main/main_visua_more.png" alt="">
                        </a>
                    </div>
                </span>
                <span style="background-image:url(../img/main/main_visual_03_m.png)" class="only_m">
                    <div class="main_text">
                        <h3>최고의 RPA <br>서비스 제공</h3>
                        <a href="/web/rpa_01.php">
                             자세히 보기 <i></i>
                        </a>
                    </div>
                </span>
            </div>

            <!-- 클라우드 / 인프라 -->
            <div class="swiper-slide">
                <span style="background-image:url(../img/main/main_visual_05_bg.png)" class="only_w">
                    <div class="main_text">
                        <img src="../img/main/main_visual_05_txt.png" alt="" class="text_01">
                        <img src="../img/main/main_visual_05-1_txt.png" alt="" class="text_02">
                        <a href="/web/cloud_infra.php">
                            <img src="../img/main/main_visua_more.png" alt="">
                        </a>
                    </div>
                </span>
                <span style="background-image:url(../img/main/main_visual_04_m.png)" class="only_m">
                    <div class="main_text">
                        <h3>디지털 혁신을 위한 <br>Cloud / Infra</h3>
                        <a href="/web/cloud_infra.php">
                             자세히 보기 <i></i>
                        </a>
                    </div>
                </span>
            </div>

          
            <!-- BPO -->
            <div class="swiper-slide">
                <span style="background-image:url(../img/main/main_visual_07_bg.png) " class="only_w">
                    <div class="main_text">
                        <img src="../img/main/main_visual_07_txt.png" alt="" class="text_01">
                        <img src="../img/main/main_visual_07-01_txt.png" alt="" class="text_02">
                        <a href="/bpo/bpo_outline.php">
                            <img src="../img/main/main_visua_more.png" alt="">
                        </a>
                    </div>
                </span>
                <span style="background-image:url(../img/main/main_visual_05_m.png)" class="only_m">
                    <div class="main_text">
                        <h3>고객의 업무혁신을 지원하는<br>BPO Service</h3>
                        <a href="/bpo/bpo_outline.php">
                             자세히 보기 <i></i>
                        </a>
                    </div>
                </span>
            </div>
           




        </div>

        <div class="btn_wrap">
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>

        <div class="swiper_bot">
            <div class="swiper-pagination"></div>
            <button type="play" class="sw_play"></button>
        </div>

    </div>

    <div class="main_pop">
        <div class="main_bg">
            <div class="pop_wrap">
                <div class="main_p_t">
                    <p>Homepage renewal</p> 
                    <button class="btn_close"></button>
                </div>
                <div class="pop_cont">
                    <div>
                        <img src="../img/sub/popup.png" alt="" class="only_w">
                        <img src="../img/sub/popup_m.png" alt="" class="only_m">
                    </div>
                </div>
                <div class="today_btn">
                    <div>
                        <input type="checkbox" name="today" id="today">
                        <label for="today">오늘 하루 이창 열지 않기</label>
                    </div>
                    <button class="btn_close">닫기</button>
                   
                </div>
            </div>
           

        </div>
       
    </div>



</section>

<? include('../inc/footer.php');?>

<script>
    $(function () {
        $('.main_viaual .swiper-pagination-bullet-active span').width();
        $('.main_viaual .swiper-pagination-bullet-active').addClass('marign_r');

        $('.m_header .logo').addClass('logo_show');
        $('.m_header .m_h_bot').addClass('m_gnb_show');
        $('.f_nav').addClass('none');

        $('.main_pop .btn_close').on('click', function(){
            $('.main_pop').hide()
        })

        $('footer').addClass('main')
    })
</script>