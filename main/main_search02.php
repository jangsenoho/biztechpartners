<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>
<section id="container" class="sub main">
    <div class="inner_1200"> 
        <div class="sub_cont">

            <div class="cont_box">
                <div class="search_wrap">
                    <strong>Contents 검색결과</strong> <span>0</span>건
                    <ul class="search_list">
                        <li>
                            <div class="list_ttl_no">검색 결과가 없습니다. <br>
                                다른 검색어를 입력해주세요.
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            


            <div class="cont_box">
                <div class="go_center">
                    <h3><span>BizTech</span>에 궁금하신 사항이 있으신가요?</h3>
                    <a href="../business/business_question.php" target="_blank">고객문의 바로가기 <i></i></a>
                </div>
                <div class="img_wrap">
                    <img src="../img/sub/about_view.png" alt="">
                </div>
            </div>

         
        </div>
    </div>


</section>

<? include('../inc/footer.php');?>

<script>

    $(function(){
        $('.m_header .logo').addClass('logo_show');

    })
</script>