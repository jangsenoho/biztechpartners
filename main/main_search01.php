<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>
<section id="container" class="sub main">
    <div class="inner_1200"> 
        <div class="sub_cont">

            <div class="cont_box">
                <div class="search_wrap">
                    <strong>Contents 검색결과</strong> <span>5</span>건
                    <ul class="search_list">
                        <li>
                            <a href="#">
                                <div class="ttl_top">
                                    <span class="gray_txt">About Biztech </span>
                                    <span class="page_location">회사소개</span>
                                </div>
                                <div class="list_ttl">비즈테크파트너스 회사소개</div>
                            </a>
                            
                        </li>
                        <li>
                            <a href="#">
                                <div class="ttl_top">
                                    <span class="gray_txt">SAP </span>
                                    <span class="page_location">Cloud</span>
                                </div>
                                <div class="list_ttl">AP Gold 파트너이자 AWS, AZURE 파트너이며 LG그룹사 SAP ERP 부문 Cloud 전문 역량을 바탕으로 SAP의 HANA In-Memory Database를 사용하는 SAP</div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="ttl_top">
                                    <span class="gray_txt">ICT Service </span>
                                    <span class="page_location">SI/SM</span>
                                </div>
                                <div class="list_ttl">BizTech Partners는 다양한 Industry의 고객을 대상으로 서비스를 제공 하고 있으며, 이를 위해 사전 IT Consulting에서 시스템 구축 및 유지보수 서비스에 이르는 Business Life Cycle Service를 제공하고 있습니다.</div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="ttl_top">
                                    <span class="gray_txt">BPO</span>
                                    <span class="page_location">HR BPO</span>
                                </div>
                                <div class="list_ttl">전문적인 서비스의 역량과 철저한 보안 관리를 기반으로 하여, 지속적으로 고객사를 확대해 가고 있습니다.</div>
                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div class="ttl_top"> 
                                    <span class="gray_txt">인재채용</span>
                                    <span class="page_location">채용공고</span>
                                </div>
                                <div class="list_ttl">2022년 1분기 OOO팀 공채 진행</div>
                            </a>
                        </li>
                    </ul>
                    <a href="#" class="more_list">더보기</a>
                </div>
            </div>
            


            <div class="cont_box">
                <div class="go_center">
                    <h3><span>BizTech</span>에 궁금하신 사항이 있으신가요?</h3>
                    <a href="../business/business_question.php" target="_blank">고객문의 바로가기 <i></i></a>
                </div>
                <div class="img_wrap">
                    <img src="../img/sub/about_view.png" alt="">
                </div>
            </div>

         
        </div>
    </div>


</section>

<? include('../inc/footer.php');?>

<script>

    $(function(){
        $('.m_header .logo').addClass('logo_show');

    })
</script>