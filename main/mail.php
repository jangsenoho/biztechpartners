<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>

<style>
    * { font-family: 'Noto Sans KR', sans-serif;}
    .mail_wrap {max-width: 770px; width:100% ; margin: 0 auto;  }
    .mail_wrap .line {width:100%; height: 8px; background:#c00c3f; }
    .mail_wrap .mail_box {
        padding:  40px;
    }
    .mail_wrap .mail_box > div {
        margin-top: 50px;
        font-size: 24px;
        font-weight: 700;
        line-height: 1.6
    }
    .mail_wrap .mail_box > div span {
        color: #c00c3f;
    }
</style>

<div class="mail_wrap">
    <div class="line"></div>
        <div class="mail_box">
            <h1 class="logo"><img src="../img/common/logo_bk.png" alt="비즈테크 로고"></h1>

            <div>비즈테크파트너스 홈페이지에 아래 내용으로 <br>
                <span>고객문의가 등록</span>되었습니다.
            </div>

            <table>
                <tbody>
                    <tr>
                        <td>구분</td>
                        <td>ERP</td>
                    </tr>
                    <tr>
                        <td>이름</td>
                        <td>홍길동</td>
                    </tr>
                    <tr>
                        <td>회사명</td>
                        <td>유월커뮤니케이션</td>
                    </tr>
                    <tr>
                        <td>직급</td>
                        <td>사원</td>
                    </tr>
                    <tr>
                        <td>연락처</td>
                        <td>010-1234-1234</td>
                    </tr>
                    <tr>
                        <td>이메일</td>
                        <td>asdf123@1234.com</td>
                    </tr>
                    <tr>
                        <td>제목</td>
                        <td>업무 제휴문의 드립니다.</td>
                    </tr>
                    <tr>
                        <td>문의내용</td>
                        <td>안녕하세요. 주식회사 우리나라 영업 담당을 하고 있는 홍길동이라고 <br>
                            합니다. ERP 업무제휴 관련하여 담당자와 통화하고 싶습니다. <br>
                            연락 부탁드립니다. <br>
                            감사합니다. <br>
                            <br>
                            홍길동 드림.
                        </td>
                    </tr>
                </tbody>
            </table>
        
        </div>

    <div class="line"></div>
</div>