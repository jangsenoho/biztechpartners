<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>
<section id="container" class=" err">
    <div class=" main_err">

        <div class="main_txt">
            <h2><strong>4<span>0</span>4</strong> ERROR</h2>
            <p>페이지를 찾을 수 없습니다.</p>
            <div>
                <span>일시적인 오류이거나 없는 주소입니다.</span> 
                <span>입력하신 주소를 다시 한번 확인해주세요.</span>
            </div>

            <div class="button_wrap ">
                <a href="/main/main.php">홈으로</a>
            </div>
           
        </div>
    </div>








</section>

<? include('../inc/footer.php');?>

<script>
    $(function () {
        $('.m_header .logo').addClass('logo_show');
        $('.m_header .m_h_bot').addClass('m_gnb_show');
    })
</script>