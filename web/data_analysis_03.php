<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub web data_an" data-depth="web" data-menu="web_04" data-subnav="web_04">
    <div class="inner_1200">
                      <?php include($_SERVER['DOCUMENT_ROOT'].'/web/web_nav.php');?>

        <div class="sub_cont" data-depth="data" data-menu="data_03" data-datanav="data_03">

            <!-- <h2 class="ttl ttl_02">Data Analysis</h2> -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/web/data_nav.php');?>

            <div class="ttl_box     m50">
                <h2 class="ttl">Dashboard</h2>
                <span class="line"></span>
                <p class="desc_02">기업 내 가장 기본이 되는 의사결정지원 시스템으로 <span>경영층이 업무를 수행</span>하고 <span>경영목적을 달성</span>하는데 필요한 <br class="only_w">경영과 관련된 <span>주요정보를 정확 신속하게 조회 할 수 있도록 지원하는 정보시스템</span>을 의미합니다.</p>
            </div>

            <!-- 사업내용 -->
            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>사업 </span>내용</h3>
                </div>
                <p class="desc_02">가장 명확하고 효과적인 정보 전달하기 위해 <span>다양한 유형의 화면으로 대시보드를 구현</span>합니다.</p>
                
                <div class="img_wrap show">
                    <img src="../img/sub/web_an_11_m.jpg" alt="" class="only_m">
                    <img src="../img/sub/web_an_11.jpg" alt="" class="only_w">
                </div>
                
            </div>
             <!-- ---------------------------------------------------- // 사업내용 -->

            <!-- Dashboard -->
            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl">About <span>Dashboard</span></h3>
                </div>
                <ul class="service_list">
                    <li>
                        <strong><span>U</span>sability</strong>
                        <ul>
                            <li><i>·</i>  좋은 성능과 사용 용이성을 함께 제공하여 누구나 질문에 대해 답변을 쉽게 찾을 수 있도록 해주는 것</li>
                        </ul>
                    </li>
                    <li>
                        <strong><span>F</span>lexibility</strong>
                        <ul>
                            <li><i>·</i>  고객의 다양한 요구사항 반영 구현</li>
                            <li><i>·</i>  고객 업무 환경에 맞는 비즈니스 로직 기반 통합 리포팅 기법 제공</li>
                        </ul>
                    </li>
                    <li>
                        <strong><span>C</span>hart Support</strong>
                        <ul>
                            <li><i>·</i>  대화형 각종 차트, 게이지, 표시기 등 지원</li>
                        </ul>
                    </li>
                    <li> 
                        <strong><span>A</span>uthority Management</strong>
                        <ul>
                            <li><i>·</i> 각 관리 시스템의 구성, 권한에 따른 맞춤형  대시보드 구현</li>
                        </ul>
                    </li>
               </ul>
            </div>


             <!-- -------------------------------------- //  Dashboard -->


            <!-- Benefit -->
            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl">Benefit</h3>
                </div>
                <p class="desc_02">기업은 Dashboard를 사용하여 경영환경에 대한 빠르고 직관적인 이해를 통해 <span>잠재적인 Insight의 도출 및 <br  class="only_w">보다 효율적인 의사결정을 수행</span> 할 수 있습니다.</p>
                <div class="img_wrap show">
                    <img src="../img/sub/web_an_12_m.jpg" alt="" class="only_m">
                    <img src="../img/sub/web_an_12.jpg" alt="" class="only_w">
                </div>
            </div>


             <!-- -------------------------------------- //  Benefit -->



        </div>

    </div> <!-- inner -->

    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="">
            </div>
           
		</div>
	</div>


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>