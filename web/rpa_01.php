<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub web" data-depth="web" data-menu="web_02" data-subnav="web_02">
    <div class="inner_1200">

                      <?php include($_SERVER['DOCUMENT_ROOT'].'/web/web_nav.php');?>

        <div class="sub_cont">

            <!-- <h2 class="ttl ttl_02">RPA​</h2> -->
            <ul class="sub_nav_02">
                <li class="on"><a href="/web/rpa_01.php">RPA 개요</a></li>
                <li class=""><a href="/web/rpa_02.php">RPA 대표사례</a></li>
            </ul>

            <div class="ttl_box mar_ov  m50">
                <h2 class="ttl">RPA 개요</h2>
                <span class="line"></span>
                <strong ><span>RPA</span>의 도입으로 기업의 업무 자동화를 시작하세요.​</strong>
                <strong><span>BizTechPartners</span>가 도와드리겠습니다.​</strong>
            </div>

            <div class="cont_box">

                <!-- 파트너가 제안하는 -->
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>BizTechPartners</span>가 제안하는 <span>RPA</span>란?​</h3>
                </div>
                <div class="gray_w50">
                    <ul class="ico_50w">
                        <li>
                            <div class="ico_and_txt">
                                <div class="ico_img">
                                    <img src="../img/sub/web_si_04.png" alt="" class="only_w">
                                    <img src="../img/sub/web_si_04_m.png" alt="" class="only_m">
                                </div>
                                <span>업무 프로세스 개선</span>
                            </div>
                            <div class="ico_txt">
                                <p><i>1. </i> 기능 구현(타 시스템 간의 정보교환 용이)​</p>
                                    <span><i>ㆍ</i>기존에 불가능했던 업무 프로세스를 기능 <br> 구현을​ 통해 가능하도록 개선​</span>
                                <p><i>2. </i> 처리 속도 개선</p>
                                <!--BEGIN: 2022 04 25 텍스트 수정   -->
                                    <span><i>ㆍ</i>반복적이고 불필요한 업무 개선​​</span>
                                      <!--END: 2022 04 25 텍스트 수정   -->
                                <p><i>3. </i> 업무 품질 개선​</p>
                                    <span><i>ㆍ</i>휴먼 에러를 줄여 업무의 지속성, 신뢰성 개선​</span>
                            </div>
                        </li>
                        <li>
                            <div class="ico_and_txt">
                                <div class="ico_img">
                                    <img src="../img/sub/web_si_05.png" alt="" class="only_w">
                                    <img src="../img/sub/web_si_05_m.png" alt="" class="only_m">
                                </div>
                                <span>비용 절감</span>
                            </div>
                            <div class="ico_txt">
                                <p><i>1. </i> 기존 시스템 활용​​</p>
                                    <span><i>ㆍ</i>기존 시스템의 교체 작업이 아닌 활용을 통해 ​<br>비용 절감​​</span>
                                <p><i>2. </i> 신속한 구축 및 효과 실현</p>
                                    <span><i>ㆍ</i>개발 기간이 짧고, 빠른 적용 가능​​​</span>
                                <p><i>3. </i> 피크 시즌 추가 인력 비용 절감​ ​</p>
                            </div>
                        </li>

                        <li>
                            <div class="ico_and_txt">
                                <div class="ico_img">
                                    <img src="../img/sub/web_si_06.png" alt="" class="only_w">
                                    <img src="../img/sub/web_si_06_m.png" alt="" class="only_m">
                                </div>
                                <span>생산성 증가</span>
                            </div>
                            <div class="ico_txt">
                                <p><i>1. </i> 적은 인력으로 더 많은 업무 처리​​</p>
                                    <span><i>ㆍ</i>24시간 365일 무휴일 근무​​</span>
                                <p><i>2. </i> 신속 정확한 업무 처리​</p>
                                <p><i>3. </i> 업무 영역 확장​ ​</p>
                                    <span><i>ㆍ</i>신기술을 통한 업무영역의 확장(OCR, API 등)​​​</span>
                            </div>
                        </li>

                        
                        <li>
                            <div class="ico_and_txt">
                                <div class="ico_img">
                                    <img src="../img/sub/web_si_07.png" alt="" class="only_w">
                                    <img src="../img/sub/web_si_07_m.png" alt="" class="only_m">
                                </div>
                                <span>근무환경 개선</span>
                            </div>
                            <div class="ico_txt">
                                <p><i>1. </i> 업무 만족도 개선​​</p>
                                    <span><i>ㆍ</i>고부가가치 업무로의 전환​​</span>
                                <p><i>3. </i> 보안 환경 개선​ ​</p>
                                    <span><i>ㆍ</i>민감한 데이터에 대한 인적자원의 접근 제한</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!--       ------------------------------------------------ // 파트너가 제안하는 -->

            <!-- 이미지맵 -->
            <div class="cont_box">
                <div class="bor_ttl_box img_map">
                    <h3 class="bor_ttl"><span>RPA</span>에 적합한 업무</h3>
                </div>
                <div class="img_wrap only_w ">
                    <img src="/img/sub/web_si_03.png" usemap="#worksMap" name="map">
                </div>

                <map name="worksMap" class="only_w">
                    <area target="_self" alt="규칙 기반 업무" title="규칙 기반 업무" href="" onmouseover="map.src='/img/sub/web_si_03_01.png'" onmouseout="map.src='/img/sub/web_si_03.png'" coords="348,512,297,594,905,594,854,516" shape="poly">
                    <area target="_self" alt="다중 시스템 업무" title="다중 시스템 업무" href="" onmouseover="map.src='/img/sub/web_si_03_02.png'" onmouseout="map.src='/img/sub/web_si_03.png'"  coords="472,299,358,503,592,502" shape="poly">
                    <area target="_self" alt="독립적 업무" title="독립적 업무" href="" onmouseover="map.src='/img/sub/web_si_03_03.png'" onmouseout="map.src='/img/sub/web_si_03.png'" coords="477,291,718,294,604,491,599,494" shape="poly">
                    <area target="_self" alt="오류 발생 업무" title="오류 발생 업무" href="" onmouseover="map.src='/img/sub/web_si_03_04.png'" onmouseout="map.src='/img/sub/web_si_03.png'" coords="730,300,607,502,845,503,844,502,845,503" shape="poly">
                    <area target="_self" alt="도움 필요 업무" title="도움 필요 업무" href="" onmouseover="map.src='/img/sub/web_si_03_05.png'" onmouseout="map.src='/img/sub/web_si_03.png'" coords="602,78,605,70,478,284,720,283" shape="poly">
                </map>


                           
                
                <div class="m_img_map only_m">
                    <div class="img_wrap">
                        <img src="/img/sub/web_si_03_m_1.png" alt="" usemap="#image-map" name="map02">
                    </div>

                    <map name="image-map">
                        <area target="_self" alt="규칙 기반 업무" title="규칙 기반 업무" href="javascript:;" coords="52,430,2,509,623,507,575,430" shape="poly" class="img_map_01">
                        <area target="_self" alt="다중 시스템 업무" title="다중 시스템 업무" href="javascript:;" coords="178,218,54,419,300,419" shape="poly" class="img_map_02">
                        <area target="_self" alt="독립적 업무" title="독립적 업무" href="javascript:;" coords="191,212,432,212,313,414" shape="poly" class="img_map_03"> 
                        <area target="_self" alt="오류 발생 업무" title="오류 발생 업무" href="javascript:;" coords="446,220,325,420,570,418" shape="poly" class="img_map_04">
                        <area target="_self" alt="도움 필요 업무" title="도움 필요 업무" href="javascript:;" coords="312,0,190,199,436,199" shape="poly" class="img_map_05">
                    </map>
                </div>

                   

            </div>
            <!-- --------------------------------------------------//이미지맵 -->

            <!-- 왜 파트너인가  -->
            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl">왜 BizTechPartners인가?</h3>
                </div>
                <strong class="ttl_asome">고객과 Knowledge를 공유하며 <br class="only_m">쉽게 자동화 구현​</strong>
                <p class="desc_02">사용의 편의성 및 재사용을 통한 개발 생산성 향상, 운영 효율성을 확보할 수 있는 다양한 컨설팅 서비스를 제공합니다​</p>
                
                <ul class="service_list">
                    <li>
                        <strong><i>01. </i>풍부한 <span>RPA 경험</span></strong>
                        <ul>
                            <!-- BEGIN 2022 04 26 텍스트 수정  --> 
                            <li><i>·</i>  LG그룹, GS그룹 내 다양한 산업의 업무를 경험​</li> 
                            <li><i>·</i>  고객사의 규모에 맞는 RPA 도입 전략 컨설팅 가능</li>
                            <!-- END 2022 04 26 텍스트 수정  -->  
                            <li><i>·</i>  국내 최대 RPA 전문가 보유(약 70명)​</li>
                            <li><i>·</i>  다양한 RPA경험​</li>
                            <li class="sub"><span>CheckMate, Uipath, Kapow 솔루션, ​<br>AutomateOne, MSPowerPlatform​</span></li>
                        </ul>
                    </li>
                    <li>
                        <strong><i>02. </i>RPA 개발/<span>운영 센터</span></strong>
                        <ul>
                            <li><i>·</i>  가격 경쟁력​</li>
                            <li><i>·</i>   소규모의 프로세스 개발 및 운영 가능​</li>
                            <li><i>·</i>   365일 24시간 안정적인 운영​</li>
                            <li><i>·</i>   원격 개발, 운영 가능​</li>
                        </ul>
                    </li>
                    <li>
                    <strong><i>03. </i>RPA 사내 <span>기술력 내재화</span></strong>
                        <ul>
                            <li><i>·</i>  포괄적인 제품 교육 제공​</li>
                            <li><i>·</i>  다양한 RPA Tool 노하우 전수​</li>
                            <li class="sub"><span>Ex&#41; LG 그룹사 강의, GS 코칭서비스, 비대면 코칭 서비스 ​</span></li>
                        </ul>
                    </li>
                    <li> 
                    <strong><i>04. </i>RPA 토탈<span>서비스 제공</span></strong>
                        <ul>
                            <li><i>·</i>  체크메이트 총판​</li>
                            <li class="sub"><span>계약의 편리성 & 적합한 제품 구성 제안 가능</span></li>
                            <li><i>·</i>  RPA 포탈 서비스​</li>
                            <li><i>·</i> 자동화 업무 인벤토리 및 Asset​</li>
                        </ul>
                    </li>
               </ul>
                            
            </div>
             <!--  ------------------------------------------------ //왜 파트너인가  -->

             <!-- 실적 회사 -->
            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>RPA</span> 주요 수행 실적</h3>
                </div>

                <ul class="cust_list">
                    <li>
                        <img src="../img/sub/logo_01.png" alt="">
                    </li>
                    <li>
                        <img src="../img/sub/logo_03.png" alt="">
                    </li>
                    <li>
                        <img src="../img/sub/logo_04.png" alt="">
                    </li>
                    <li>
                        <img src="../img/sub/logo_05.png" alt="">
                    </li>
                    <li>
                        <img src="../img/sub/logo_06.png" alt="">
                    </li>
                    <li>
                        <img src="../img/sub/logo_02.png" alt="">
                    </li>
                    <li>
                        <img src="../img/sub/logo_48.jpg" alt="">
                    </li>
                    <li>
                        <img src="../img/sub/logo_49.jpg" alt="">
                    </li>
                    <li>
                        <img src="../img/sub/logo_51.jpg" alt="">
                    </li>
                    <li >
                        <img src="../img/sub/logo_63.jpg" alt=""> 
                    </li>
                    <li >
                        <img src="../img/sub/logo_50.jpg" alt="">
                    </li>
                    <li >
                        <img src="../img/sub/logo_12.png" alt="">
                    </li>
                    <li >
                        <img src="../img/sub/logo_52.jpg" alt="">
                    </li>
                    <li >
                        <img src="../img/sub/logo_53.jpg" alt="">
                    </li>

                    <li >
                        <img src="../img/sub/logo_54.jpg" alt="">
                    </li>
                    <li >
                        <img src="../img/sub/logo_17.png" alt="">
                    </li>
                    <li >
                        <img src="../img/sub/logo_55.jpg" alt="">
                    </li>

                    <li >
                        <img src="../img/sub/logo_56.jpg" alt="">
                    </li>
                    <li >
                        <img src="../img/sub/logo_57.jpg" alt="">
                    </li>
                    <li >
                        <img src="../img/sub/logo_58.jpg" alt="">
                    </li>

                    <li class="last">
                        <img src="../img/sub/logo_59.jpg" alt="">
                    </li>
                    <li class="last">
                        <img src="../img/sub/logo_60.jpg" alt="">
                    </li>
                    <li class="last">
                        <img src="../img/sub/logo_61.jpg" alt="">
                    </li>
                    <li class="last">
                        <img src="../img/sub/logo_62.jpg" alt="">
                    </li>
                </ul>
            </div>
            <!--        -------------------------------------------// 실적 회사 -->

            <div class="cont_box">
                <div class="banner">
                    <strong>도움이 필요하신가요?</strong>
                    <p>BizTech가 도와드리겠습니다.</p>
                    <div class="biz_tel">
                        <a href="javascript:;" class="more">문의하기 <i></i></a>

                        <div class="call_mail">
                            <strong>RPA 담당자</strong>
                            <a href="tel: 02-2084-6700" class="tel"> <i></i> <span>TEL</span>02-2084-6700</a>
                            <a href="mailto: rpa2@Biztechpartners.co.kr "  class="mail"> <i></i> <span>E-mail</span>rpa2@Biztechpartners.co.kr</a>
                            <a href="/business/business_question.php" class="more more02" target="_blank">문의하기 <i></i></a>
                        </div>
                    </div>
                    
                    <a href="javascript:;" class="link"  onclick="alert('준비중입니다.');" >유튜브</a>
                </div>
            </div>

        </div>













    </div> <!-- inner -->



</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가

    });
   
    $(function () {
        $('.m_header').addClass('sub')




        $('img[usemap]').rwdImageMaps();
        $("#img").width("100%");
        $('.img_map_01').on('click', function () {
            $('img[name=map02]').attr('src' , '/img/sub/web_si_03_m_1.png')
        });
        $('.img_map_02').on('click', function () {
            $('img[name=map02]').attr('src' , '/img/sub/web_si_03_m_2.png')
        });
        $('.img_map_03').on('click', function () {
            $('img[name=map02]').attr('src' , '/img/sub/web_si_03_m_3.png')
        });
        $('.img_map_04').on('click', function () {
            $('img[name=map02]').attr('src' , '/img/sub/web_si_03_m_4.jpg')
        });
        $('.img_map_05').on('click', function () {
            $('img[name=map02]').attr('src' , '/img/sub/web_si_03_m_5.jpg')
        });
    });

</script>