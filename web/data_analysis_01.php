<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub web data_an" data-depth="web" data-menu="web_04" data-subnav="web_04">
    <div class="inner_1200">

                      <?php include($_SERVER['DOCUMENT_ROOT'].'/web/web_nav.php');?>

        <div class="sub_cont" data-depth="data" data-menu="data_01" data-datanav="data_01">

            <!-- <h2 class="ttl ttl_02">Data Analysis</h2> -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/web/data_nav.php');?>

            <div class="ttl_box     m50">
                <h2 class="ttl">Big Data</h2>
                <span class="line"></span>
                <p class="desc_02">기존 데이터베이스 관리도구, 관리시스템의 능력을 넘어 대량의 정형, 반정형, 비정형 데이터 셋, 이를 포함한 <br  class="only_w">
                <span>데이터로부터 의미 있는 가치를 추출하고 원하는 결과를 분석하는 기술</span>입니다.</p>
            </div>

            <!-- 사업내용 -->
            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>사업 </span>방향</h3>
                </div>
                
                <ul class="gray_asome">
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/web_an_01.png" alt=""  class="only_w">
                            <img src="../img/sub/web_an_01_m.png" alt=""  class="only_m" style="max-width: 43px">
                        </div>
                        <strong><span>빅데이터</span> 구축 서비스</strong>
                        <p> <i>·</i>  고객에게 ROI와 적용 효과를 보장하는 <br class="only_w">다양한 솔루션을 제공합니다.</p> 
                        <p> <i>·</i>  빅데이터의 수집, 저장, 처리 시스템을 <br class="only_w"> 구축하고, 새로운 빅데이터 비즈니스를 <br> 구현합니다.</p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/web_an_02.png" alt=""  class="only_w">
                            <img src="../img/sub/web_an_02_m.png" alt=""  class="only_m"  style="max-width: 50px">
                        </div>
                        <strong><span>빅데이터</span> 분석 서비스 </strong>
                        <p> <i>·</i>   데이터의 분석 방법론 및 <br class="only_w"> 모델을 개발하며, 이를 활용한 <br> 분석 서비스를 제공합니다.</p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/web_an_03.png" alt="" class="only_w">
                            <img src="../img/sub/web_an_03_m.png" alt=""  class="only_m"  style="max-width: 63px">
                        </div>
                        <strong><span>컨설팅</span> 및 <span>진단</span> 서비스</strong>
                        <p><i>·</i> 비즈니스 문제를 도출하고 빅데이터 <br class="only_w">기술을 이용한 해결 방안과 사업 모델을 <br class="only_w">제시합니다.</p>
                    </li>
                </ul>
                
            </div>
             <!-- ---------------------------------------------------- // 사업내용 -->

             <!-- about big data -->
            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl">About <span>Big Data</span></h3>
                </div>
                <div class="img_wrap show">
                    <img src="../img/sub/web_an_04_m.png" alt="" class="only_m">
                    <img src="../img/sub/web_an_04.png" alt=""  class="only_w">
                </div>

                <ul class="ico_s_100w">
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image:url(../img/sub/web_an_05.jpg)"  class="only_w"></span> 
                            <span style="background-image:url(../img/sub/web_an_05_M.jpg)"  class="only_m"></span> 
                            <strong>규모 <span>(Volume)</span></strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>기술적인 발전과 IT의 일상화에 따라 디지털 정보량이 기하급수적으로 폭증</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image:url(../img/sub/web_an_06.jpg)"  class="only_w"></span> 
                            <span style="background-image:url(../img/sub/web_an_06_m.png)"  class="only_m"></span> 
                            <strong>속도 <span>(Velocity)</span></strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>사물 정보, 스트리밍 정보 등 실시간 성 정보 증가</p>
                            <p><i> · </i>대규모 데이터 처리 및 가치 있는 현재 정보 활용을 위해 데이터를 처리 및 분석 속도 중요</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image:url(../img/sub/web_an_07.jpg)"  class="only_w"></span> 
                            <span style="background-image:url(../img/sub/web_an_07_m.jpg)"  class="only_m"></span> 
                            <strong>다양성 <span>(Variety)</span></strong>
                        </div>
                        <div class="ico_desc">
                            <!-- BEGIN 2022 04 26 텍스트 수정  -->  
                            <p><i> · </i>로그 기록, 소셜, 위치, 소비, 현실 데이터 등 데이터 종류의 증가</p>
                            <!-- END 2022 04 26 텍스트 수정  -->  
                            <p><i> · </i>텍스트 이외의 멀티미디어 등 비정형화된 데이터 유형의 다양화 </p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image:url(../img/sub/web_an_08.jpg)" class="only_w"></span> 
                            <span style="background-image:url(../img/sub/web_an_08_m.jpg)"  class="only_m"></span> 
                            <strong>정확성 <span>(Veracity)</span></strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>빅데이터의 특성상 방대한 데이터를 기반으로 분석을 수행</p>
                            <p><i> · </i>데이터 분석에서 질이 높은 데이터를 활용하는 것이 분석의 정확도에 영향을 줌 </p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image:url(../img/sub/web_an_09.jpg)" class="only_w"></span> 
                            <span style="background-image:url(../img/sub/web_an_09_m.jpg)"  class="only_m"></span> 
                            <strong>가치 <span>(Value)</span></strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i> 빅데이터가 추구하는 것은 가치 창출</p>
                            <p><i> · </i>기업이 현재 당면하고 있는 문제를 해결하는데 통찰력 있는 유용한 정보를 제공 </p>
                        </div>
                    </li>
                </ul>
            
            </div>
            <!-- ------------------------------------------- // about big data -->

            <!-- Architecture -->
            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl">Architecture</h3>
                </div>
                <p class="desc_02">복잡한 데이터의 수집 및 분석, 처리를 위한 가장 중요한 프레임 워크이며 기업의 경영층 및 실무진에게 <br class="only_w"><span>고도화된 정보체계를 제공하기 위하여 효율적이고 최적화된 인프라를 구성</span>합니다.</p>
                <div class="img_wrap show">
                    <img src="../img/sub/web_an_10_m.jpg" alt="" class="only_m">
                    <img src="../img/sub/web_an_10.jpg" alt=""  class="only_w">
                </div>
            </div>


             <!-- -------------------------------------- //  Architecture -->



        </div>

    </div> <!-- inner -->


    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="">
            </div>
           
		</div>
	</div>


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>