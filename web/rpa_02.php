<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub web rpa2" data-depth="web" data-menu="web_02" data-subnav="web_02">
    <div class="inner_1200">
        <?php include($_SERVER['DOCUMENT_ROOT'].'/web/web_nav.php');?>

        <div class="sub_cont">

            <!-- <h2 class="ttl ttl_02">RPA​</h2> -->
            <ul class="sub_nav_02">
                <li class=""><a href="/web/rpa_01.php">RPA 개요</a></li>
                <li class="on"><a href="/web/rpa_02.php">RPA 대표사례</a></li>
            </ul>

            <div class="ttl_box mar_no    m50">
                <div class="a">
                    <a href="javascript:;" class="down more_pop">더 많은 <span>자료 보기</span></a>
                    <a href="javascript:;" class="down" onclick="alert('준비중입니다.');">동영상 <span>데모 보기</span></a>
                </div>
                <h2 class="ttl">RPA 대표사례</h2>
                <span class="line"></span>
            </div>

            <!-- 대표사례 -->
            <div class="cont_box">
                <ul class="w50_cont">
                    <li>
                        <p><i>·</i>백화점 EDI 일일매출현황</p>
                        <div class="main_case">
                            <p>“백화점 EDI 시스템에 접속하여 매일 온/오프라인 매출 실적 자료를 다운로드 하여 필요한 정보를 내부 시스템에 업로드 하는 업무＂​</p>
                            <ul class="casc_ico">
                                <li>
                                    <div class="ico_wrap">
                                        <img src="../img/ico/pra_ico_01.png" alt="" class="only_w">
                                        <img src="../img/ico/pra_ico_01_m.png" alt="" class="only_m">
                                    </div>
                                    <strong>정보 검증 가능</strong>
                                    <span>매장과 온라인 일일매출 정보 <br class="only_ms"> 교차검증 가능</span>
                                </li>
                                <li>
                                    <div class="ico_wrap">
                                        <img src="../img/ico/pra_ico_02.png" alt="" class="only_w">
                                        <img src="../img/ico/pra_ico_02_m.png" alt="" class="only_m">
                                    </div>
                                    <strong>매출정보 활용</strong>
                                    <span>마케팅, 제품개선을 통한<br class="only_ms"> 매출 증가</span>
                                </li>
                            </ul>
                            <strong>· Process</strong>
                            <span>온/오프라인 매출 실적 자료 다운로드 → 매출 정보 가공(백화점,매장,제품별로 구분하고 필요정보만 가공) → 내부 DB에 업데이트​</span>
                        </div>
                    </li>
                    <li>
                        <p><i>·</i>전자세금계산서 국세청전송대사</p>
                        <div class="main_case">
                            <p>“ERP(SAP)의 전자세금계산서와 국세청(Home Tax)의 전자세금계산서정보를 대사 하는 업무＂​</p>
                            <ul class="casc_ico">
                                <li>
                                    <div class="ico_wrap">
                                        <img src="../img/ico/pra_ico_03.png" alt="" class="only_w">
                                        <img src="../img/ico/pra_ico_03_m.png" alt="" class="only_m">
                                    </div>
                                    <strong>절감 시간</strong>
                                    <span>연간 약 2천 5백 시간 <br class="only_ms"> 이상 절감 </span>
                                </li>
                                <li>
                                    <div class="ico_wrap">
                                        <img src="../img/ico/pra_ico_04.png" alt="" class="only_w">
                                        <img src="../img/ico/pra_ico_04_m.png" alt="" class="only_m">
                                    </div>
                                    <strong>단순 반복 작업 감소</strong>
                                    <span>핵심 인력의 단순<br class="only_ms"> 수작업 시간 감소</span>
                                </li>
                            </ul>
                            <strong>· Process</strong>
                            <span>SAP 작업월의 세금계산서 목록 다운 → 홈텍스 작업 월의 세금계산서 목록 다운 → 과제/면세 분리 및 피벗 시트 생성 → 통합 대사 자료 생성 → 대사
                                불일치 목록 추출 및 표기 → 메일발송​</span>
                        </div>
                    </li>
                    <li>
                        <p><i>·</i> 법인카드 오사용 모니터링</p>
                        <div class="main_case">
                            <p>“ERP(SAP)의 사원별 법인카드 사용 내역을 다운로드 받아 오사용을 모니터링한다.＂​​</p>
                            <ul class="casc_ico">
                                <li>
                                    <div class="ico_wrap">
                                        <img src="../img/ico/pra_ico_05.png" alt="" class="only_w">
                                        <img src="../img/ico/pra_ico_05_m.png" alt="" class="only_m">
                                    </div>
                                    <strong>빠른 작업 결과</strong>
                                    <span>담당자 출근전 전일 Data<br class="only_ms"> 확인 후 결과 제공 가능</span>
                                </li>
                                <li>
                                    <div class="ico_wrap">
                                        <img src="../img/ico/pra_ico_06.png" alt="" class="only_w">
                                        <img src="../img/ico/pra_ico_06_m.png" alt="" class="only_m">
                                    </div>
                                    <strong>비용 투명성 확보 </strong>
                                    <span>비용 사용 투명성<br class="only_ms"> 확보 가능</span>
                                </li>
                            </ul>
                            <strong>· Process</strong>
                            <span>SAP 법인,계정, 조건그룹별 법인카드 모니터링 조회 → 사원별로 상세내역을 다운로드 → 상세내역 다운로드 파일을 하나로 취합 → 템플릿 파일 적용해서
                                피벗 → 메일발송​​</span>
                        </div>
                    </li>
                    <li>
                        <p><i>·</i> 물류 추적 관리</p>
                        <div class="main_case">
                            <p>“ 인터넷 웹 화면에서 물류추적 조회 후 ERP 시스템에 주기적으로 업로드 하는 업무＂​</p>
                            <ul class="casc_ico">
                                <li>
                                    <div class="ico_wrap">
                                        <img src="../img/ico/pra_ico7.png" alt="" class="only_w">
                                        <img src="../img/ico/pra_ico7_m.png" alt="" class="only_m">
                                    </div>
                                    <strong>시간 확보</strong>
                                    <span>변경사항에 대한 인지 및 대응에 <br class="only_ms"> 필요한 시간 확보 </span>
                                </li>
                                <li>
                                    <div class="ico_wrap">
                                        <img src="../img/ico/pra_ico8.png" alt="" class="only_w">
                                        <img src="../img/ico/pra_ico8_m.png" alt="" class="only_m">
                                    </div>
                                    <strong>고부가가치 집중 </strong>
                                    <span>고부가가치의 업무에 <br class="only_ms"> 집중</span>
                                </li>
                            </ul>
                            <strong>· Process</strong>
                            <span>웹 사이트 접속 → Cargo Tracing 조회 → 물류 추적 조회 →  물류 변경 확인 → Tracing정보 수정 →  데이터베이스 업데이트  → 결과 메일발송​​​</span>
                        </div>
                    </li>
                </ul>

            </div>
            <!--       ------------------------------------------------ // 대표사례 -->





        </div>





    </div> <!-- inner -->

    <div class="rpa_pop">
        <div class="rpa_bg">

            <div class="pop_cont_wrap">
                <div class="pop_top">
                    <p>더 많은 자료 보기</p>
                    <button class="clo_btn"></button>
                </div>

                <div class="cont">
                    <div class="pop_tab_cont">
                        <ul class="pop_tab">
                            <li><a href="#pop_tab_01">인사/재무(공통)</a></li>
                            <li><a href="#pop_tab_02">금융/서비스</a></li>
                            <li><a href="#pop_tab_03">제조/건설</a></li>
                            <li><a href="#pop_tab_04">기타</a></li>
                        </ul>
                    </div>



                    <div class="tab_content">
                        <div id="pop_tab_01" style="">
                            <table>
                                <colgroup>
                                    <col width="8%">
                                    <col width="5%">
                                    <col width="23%">
                                    <col width="*">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th>영역</th>
                                        <th>번호</th>
                                        <th>과제명</th>
                                        <th>업무 개요</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="category">회계</td>
                                        <td class="num">10</td>
                                        <td class="ttl">부가가치세 대사</td>
                                        <td class="desc">
                                            <div class="m_ttl_area">
                                                <span>10</span> 
                                                <span>회계</span> 
                                                <span>부가가치세 대사</span>
                                            </div>
                                            국세청에 신고되어 있는 사업자번호 별 매출/매입 세금계산서 합계표를 다운로드한 후, 하나로 합쳐서 GTAX 시스템에 업로드 하여 신고 대사 처리함. 사업자번호별 불부합이 발생하면 신고불성실, 매입세액 불공제 등의 Risk 존재.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="category">회계</td>
                                        <td class="num">9</td>
                                        <td class="ttl">부가가치세 폐사업자 확인</td>
                                        <td class="desc">
                                            <div class="m_ttl_area">
                                                <span>9</span> 
                                                <span>회계</span> 
                                                <span>부가가치세 폐사업자 확인</span>
                                            </div> 
                                            AR Customer에 있는 사업자번호를 국세청 사이트에서 건건히 조회 후 폐업 여부 확인. 폐사업자에게 매출이 될 경우 가산세 등의 Risk 존재
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="category">회계</td>
                                        <td class="num">8</td>
                                        <td class="ttl">B2B본부 관리지표 자동화</td>
                                        <td class="desc">
                                            <div class="m_ttl_area">
                                                <span>8</span> 
                                                <span>회계</span> 
                                                <span>B2B본부 관리지표 자동화</span>
                                            </div>    
                                            B2B 본부에서 관리 중인 지표에 대한 리포트 작성의 수작업을 제거함
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="category">회계</td>
                                        <td class="num">7</td>
                                        <td class="ttl">IT비용 지불품의</td>
                                        <td class="desc">
                                            <div class="m_ttl_area">
                                                <span>7</span> 
                                                <span>회계</span> 
                                                <span>IT비용 지불품의</span>
                                            </div>
                                            ITMS 시스템을 통해 월 비용 현황을 지불품의 진행함
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="category">회계</td>
                                        <td class="num">6</td>
                                        <td class="ttl">Auto AR cash application</td>
                                        <td class="desc">
                                            <div class="m_ttl_area">
                                                <span>6</span> 
                                                <span>회계</span> 
                                                <span>Auto AR cash application</span>
                                            </div>
                                            Auto AR cash application thru auto generated payment advice
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="category">회계</td>
                                        <td class="num">5</td>
                                        <td class="ttl">풀링 신청프로세스 자동화</td>
                                        <td class="desc">
                                            <div class="m_ttl_area">
                                                <span>5</span>
                                                <span>회계</span> 
                                                <span>풀링 신청프로세스 자동화</span>
                                            </div>
                                            K-SURE 풀링 신청프로세스
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="category">회계</td>
                                        <td class="num">4</td>
                                        <td class="ttl">SVC Warranty 인보이스 AP/AR 상계 프로세스 자동화</td>
                                        <td class="desc">
                                            <div class="m_ttl_area">
                                                <span>4</span> 
                                                <span>회계</span> 
                                                <span>SVC Warranty 인보이스 AP/AR 상계 프로세스 자동화</span>
                                            </div>
                                            ASC warranty 인보이스에 첨부된 상계 요청금액을 바탕으로 GERP 넷팅 시스템에 AP인보이스건별 해당하는
                                            AR인보이스 금액을 입력하여 상계함.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="category">회계</td>
                                        <td class="num">3</td>
                                        <td class="ttl">소비자 소송 계좌 가압류건 고정성보증금 등록 자동화 </td>
                                        <td class="desc">
                                            <div class="m_ttl_area">
                                                <span>3</span> 
                                                <span>회계</span> 
                                                <span>소비자 소송 계좌 가압류건 고정성보증금 등록 자동화</span>
                                            </div>
                                            은행 원장을 통해 인지된 Bank Block에 대해 상세 정보(소송번호, 소송인, 은행명 등)를 확인 후 GEVS
                                            및 Manual A/R을 이용하여 고정성보증금으로 전환하는 업무
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="category">회계</td>
                                        <td class="num">2</td>
                                        <td class="ttl">거래선 Invoice, Payment 정보 입력</td>
                                        <td class="desc">
                                            <div class="m_ttl_area">
                                                <span>9</span> 
                                                <span>회계</span> 
                                                <span>거래선 Invoice, Payment 정보 입력</span>
                                            </div>
                                            거래선 Portal에서 Invoice상태 및 Payment 관련 정보 확인하여 GERP A/R Worksheet (Invoice) 업데이트
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="category">회계</td>
                                        <td class="num">1</td>
                                        <td class="ttl">상품재고 총평단가 과다증감 건 사전검증</td>
                                        <td class="desc">
                                            <div class="m_ttl_area">
                                                <span>1</span> 
                                                <span>회계</span> 
                                                <span>상품재고 총평단가 과다증감 건 사전검증</span>
                                            </div>
                                            월중에 입고되는 상품재고의 구매단가를 기준으로, 마감 전 총평단가의 증감을 사전에 Simulation하여 결산 이슈를 사전에 점검하는 업무
                                        </td>
                                    </tr>
                                </tbody>

                            </table>

                        </div>

                        <!-- 예시를 위해 하나씩만 남겨놓았습니다 -->
                        <div id="pop_tab_02" style="">
                            <table>
                                <colgroup>
                                    <col width="8%">
                                    <col width="5%">
                                    <col width="16%">
                                    <col width="*">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th>영역</th>
                                        <th>번호</th>
                                        <th>과제명</th>
                                        <th>업무 개요</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="category">회계</td>
                                        <td class="num">10</td>
                                        <td class="ttl">부가가치세 대사</td>
                                        <td class="desc">
                                            <div class="m_ttl_area">
                                                <span>10</span> 
                                                <span>회계</span> 
                                                <span>부가가치세 대사</span>
                                            </div>
                                            국세청에 신고되어 있는 사업자번호 별 매출/매입 세금계산서 합계표를 다운로드한 후, 하나로 합쳐서 GTAX 시스템에 업로드 하여 신고 대사 처리함. 사업자번호별 불부합이 발생하면 신고불성실, 매입세액 불공제 등의 Risk 존재.
                                        </td>
                                    </tr>
                                </tbody>

                            </table>

                        </div>

                        <div id="pop_tab_03" style="">
                            <table>
                                <colgroup>
                                    <col width="8%">
                                    <col width="5%">
                                    <col width="16%">
                                    <col width="*">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th>영역</th>
                                        <th>번호</th>
                                        <th>과제명</th>
                                        <th>업무 개요</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="category">회계</td>
                                        <td class="num">10</td>
                                        <td class="ttl">부가가치세 대사</td>
                                        <td class="desc">
                                            <div class="m_ttl_area">
                                                <span>10</span> 
                                                <span>회계</span> 
                                                <span>부가가치세 대사</span>
                                            </div>
                                            국세청에 신고되어 있는 사업자번호 별 매출/매입 세금계산서 합계표를 다운로드한 후, 하나로 합쳐서 GTAX 시스템에 업로드 하여 신고 대사 처리함. 사업자번호별 불부합이 발생하면 신고불성실, 매입세액 불공제 등의 Risk 존재.
                                    </tr>
                                </tbody>

                            </table>

                        </div>

                        <div id="pop_tab_04" style="">
                            <table>
                                <colgroup>
                                    <col width="8%">
                                    <col width="5%">
                                    <col width="16%">
                                    <col width="*">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th>영역</th>
                                        <th>번호</th>
                                        <th>과제명</th>
                                        <th class="desc">업무 개요</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="category">회계</td>
                                        <td class="num">10</td>
                                        <td class="ttl">부가가치세 대사</td>
                                        <td class="desc">
                                            <div class="m_ttl_area">
                                                <span>10</span> 
                                                <span>회계</span> 
                                                <span>부가가치세 대사</span>
                                            </div>
                                            국세청에 신고되어 있는 사업자번호 별 매출/매입 세금계산서 합계표를 다운로드한 후, 하나로 합쳐서 GTAX 시스템에 업로드 하여 신고 대사 처리함. 사업자번호별 불부합이 발생하면 신고불성실, 매입세액 불공제 등의 Risk 존재.
                                        </td>
                                    </tr>
                                </tbody>

                            </table>

                        </div>





                    </div>

                    <div class="m_more">
                        <button>더보기</button>
                    </div>

                    <div class="pager">
                        <ul>
                            <li class="all_pr"><a href="#"></a></li>
                            <li class="one_pr"><a href="#"></a></li>
                            <li class="num on"><a href="#">1</a></li>
                            <li class="num"><a href="#">2</a></li>
                            <li class="num"><a href="#">3</a></li>
                            <li class="num"><a href="#">4</a></li>
                            <li class="num last"><a href="#">5</a></li>
                            <li class="one_ne"><a href="#"></a></li>
                            <li class="all_ne"><a href="#"></a></li>
                        </ul>
                    </div>

                    


                </div>







            </div>
        </div>

    </div>



</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">
    $(document).ready(function () {
        $('#gnb ul').children().eq(1).addClass('active');

        // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가

    });




    $(function () {
        $('.m_header').addClass('sub')
        //  -------------------------- 탭 메뉴
        $('.rpa_pop .tab_content > div').hide();
        $('.pop_tab a').click(function () {
            $('.rpa_pop .tab_content > div').hide().filter(this.hash).show();
            $('.pop_tab a').removeClass('on');
            $(this).addClass('on');
            return false;
        }).filter(':eq(0)').click();


        $('.more_pop').on('click',function(){
            $('.rpa_pop').show()
        })

        $('.rpa_pop .clo_btn').on('click',function(){
            $('.rpa_pop').hide()
        })


   


    })
</script>