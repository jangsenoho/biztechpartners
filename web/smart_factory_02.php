<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub web infra" data-depth="web" data-menu="web_03" data-subnav="web_03">
    <div class="inner_1200">
                      <?php include($_SERVER['DOCUMENT_ROOT'].'/web/web_nav.php');?>

        <div class="sub_cont">
<!-- 
        <h2 class="ttl ttl_02">Smart Factory</h2> -->
            <ul class="sub_nav_02">
                <li class=""><a href="/web/smart_factory_01.php">Overview</a></li>
                <li class="on"><a href="/web/smart_factory_02.php">스마트물류</a></li>
            </ul>

            <div class="ttl_box mar_ov  m50">
                <h2 class="ttl">스마트 물류</h2>
                <span class="line"></span>
                <p class="desc_02">다양한 물류 센터의 기본적인 운영/관리를 넘어서 운영 계획 및 실행 능력을 극대화 하기 위한, <br class="only_w">최신의 설비 자동화 플랫폼으로 WCS 기반 하에 최적화 알고리즘, 디지털 트윈, 예지보전 및 자동화 설비 제어를 포함</p>
            </div>


            <div class="cont_box">
                <div class="img_wrap show">
                    <img src="../img/sub/over_view_pro01_m.png" alt="" class="only_m">
                    <img src="../img/sub/over_view_pro01.png" alt="" class=" only_w"> 
                </div>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>구축 현황 </span></h3>
                </div>
                <p class="desc_02">당사는 모회사인 LG CNS와 협업하여 물류 사업에 참여하고 있습니다</p>
            </div>

            <div class="w50_table">
                <div class="img_wrap">
                    <img src="../img/sub/web_it_02.png" alt="" class="only_w">
                    <img src="../img/sub/web_it_02_m.jpg" alt="" class="only_m">
                </div>
                <ul class="table_cont">
                    <li>
                        <strong class="location_ttl">경기초월물류단지</strong>
                        <div class="location_txt">
                            <p><i> · </i>아워홈 동서울 물류센터 자동화설비 도입사업</p>
                            <p><i> · </i>CJ대한통운 수도권 메가허브터미널 구축사업</p>
                        </div>
                    </li>
                    <li>
                        <strong class="location_ttl">용인물류단지</strong>
                        <div class="location_txt">
                            <p><i> · </i>올리브영 온오프라인 통합센터</p>
                            <p><i> · </i>CJ대한통운 양지 Flagship Center</p>
                        </div>
                    </li>
                    <li>
                        <strong class="location_ttl">화성동탄물류단지<br>+ 평택물류단지</strong>
                        <div class="location_txt">
                            <p><i> · </i>이베이코리아 메가물류센터</p>
                            <p><i> · </i>CJ대한통운 양지 Flagship Center</p>
                            <p><i> · </i>신세계푸드 H1평택물류센터 자동화설비 도입</p>
                        </div>
                    </li>
                    <li>
                        <strong class="location_ttl">인천공항물류단지</strong>
                        <div class="location_txt">
                            <p><i> · </i>CJ대한통운 특송센터 구축 사업</p>
                            <p><i> · </i>한진 특송장 자동분류기 구축</p>
                        </div>
                    </li>
                    <li>
                        <strong class="location_ttl">천안풍세산업단지</strong>
                        <div class="location_txt">
                            <p><i> · </i>이랜드리테일 천안센터 자동화설비 구축</p>
                            <p><i> · </i>이랜드리테일 천안유통센터 설비 증설 사업</p>
                        </div>
                    </li>
                    <li>
                        <strong class="location_ttl">대구 경남</strong>
                        <div class="location_txt">
                            <p><i> · </i>대구우편집중국 소포물류자동화</p>
                            <p><i> · </i>롯데슈퍼 부산센터 자동화설비 공급 (AutoStore)</p>
                            <p><i> · </i>홈플러스 함안상온물류센터</p>
                            <p><i> · </i>CJ프레시웨이 동부RDC 자동화 물류기기 구축</p>
                            <p><i> · </i>쿠팡 양산 크로스벨트 소터 납품</p>
                        </div>
                    </li>
                    <li>
                        <strong class="location_ttl">롯데 슈퍼(의왕)/<br>롯데마트</strong>
                        <div class="location_txt">
                            <p><i> · </i>롯데슈퍼 의왕센터 자동화설비 구축(AutoStore)</p>
                            <p><i> · </i>롯데마트 매장 Fulfillment 구축 사업</p>
                            <!-- BEGIN 2022 04 26 텍스트 수정  -->  
                            <p><i> · </i>롯데마트 세미다크스토어 구축 사업</p>
                            <!-- END 2022 04 26 텍스트 수정  -->  
                        </div>
                    </li>
                    <li>
                        <strong class="location_ttl">경기 서부권</strong>
                        <div class="location_txt">
                            <p><i> · </i>쿠팡 고양FC C/B 프로젝트</p>
                            <p><i> · </i>컬리 김포 통합 Mega FC 구축</p>
                        </div>
                    </li>
                    <li>
                        <strong class="location_ttl">경기 인천</strong>
                        <div class="location_txt">
                            <p><i> · </i>쿠팡 부천 FC휠소터 프로젝트</p>
                            <p><i> · </i>쿠팡 부천 FC1 휠소터 개선, CV 신규공급</p>
                            <p><i> · </i>쿠팡 인천4FC AGV Robot 프로젝트</p>
                        </div>
                    </li>
                    <li>
                        <strong class="location_ttl">경기 남부권</strong>
                        <div class="location_txt">
                            <p><i> · </i>판토스 시화MTV 미니로드 자동창고 구축사업</p>
                            <p><i> · </i>쿠팡 안산2FC C/B Sorter 사업</p>
                            <p><i> · </i>쿠팡 안성5FC CB Sorter 및 휠소터 사업</p>
                        </div>
                    </li>
                    <li>
                        <strong class="location_ttl">충북</strong>
                        <div class="location_txt">
                            <p><i> · </i>LG 생활건강 청주 화장품 용기 자동창고</p>
                            <p><i> · </i>LG화학 청주 양극제 자동창고</p>
                        </div>
                    </li>
                </ul>
            </div>

        </div>











    

    </div> <!-- inner -->


    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="">
            </div>
           
		</div>
    </div>
    
</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>