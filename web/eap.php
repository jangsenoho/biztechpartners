<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub web eap" data-depth="web" data-menu="web_06" data-subnav="web_06">
    <div class="inner_1200">

               <?php include($_SERVER['DOCUMENT_ROOT'].'/web/web_nav.php');?>

        <div class="sub_cont">

        

            <div class="ttl_box   mar_ov  m50">
                <h2 class="ttl">Enterprise Application Platform</h2>
                <span class="line"></span>
                <p class="desc_02">BizTech Partners는 <span>NCD(No Coding Development) Platform을 ERP, MES</span> 등<br class="only_w">다양한 시스템에 적용하여 결함을 최소화한 고품질의 시스템을 개발 및 운영하고 있습니다.</p>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl">Enterprise Application Platform <span>(1/2)</span></h3>
                </div>
                
                <div class="img_wrap show">
                    <img src="../img/sub/web_eap_01_m.jpg" alt="" class="only_m">
                    <img src="../img/sub/web_eap_01.jpg" alt="" class="only_w"> 
                </div>
               
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl">Enterprise Application Platform <span>(2/2)</span></h3>
                </div>
                <p class="desc_02">NCD Platform은 초기 단계부터 결함을 제거할 수 있도록 테스트 기반의 개발을 할 수 있으며, <br>
                    다양한 도구를 활용하여 고품질 시스템 구현이 가능하게 합니다. 또한 다양한 프레임워크 뿐만 아니라 <br>
                    다양한 UI 기술로도 구성이 가능하며 상용 UI 기술을 사용하더라도, Java 코딩은 제거할 수 있습니다.
                </p>
                
                <div class="img_wrap show">
                    <img src="../img/sub/web_eap_02_m.jpg" alt="" class="only_m">
                    <img src="../img/sub/web_eap_02.jpg" alt="" class="only_w">
                </div>
               
            </div>

          

          
           

             
      










        </div>

    </div> <!-- inner -->


    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="">
            </div>
           
		</div>
	</div>
     


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>