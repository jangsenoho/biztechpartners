<ul class="sub_gnb">
    <li><a href="../sap/implementation.php">SAP</a></li>
    <li class='on'><a href="../web/si_sm.php">WEB</a></li>
    <li  ><a href="/bpo/bpo_outline.php">BPO</a></li>
</ul>


<div class="sub_nav_wrap">
    <button type="button" class="now_btn only_m"></button>
    <ul class="sub_nav web">
        <li data-subnav="web_01"><a href="/web/si_sm.php">SI/SM</a></li>
        <li data-subnav="web_02"><a href="/web/rpa_01.php">RPA</a></li>
        <li data-subnav="web_03"><a href="/web/smart_factory_01.php">Smart Factory​</a></li>
        <li data-subnav="web_04"><a href="/web/data_analysis_01.php">Data Analysis​</a></li>
        <li data-subnav="web_05"><a href="/web/cloud_infra.php">Cloud/Infra/IT보안​</a></li>
        <li data-subnav="web_06"><a href="/web/eap.php">EAP</a></li>
    </ul>
</div>


<script>
   
    $(function () {

        var detph = $('#container').data('subnav');
        var nowTxt = $('.sub_nav li[data-subnav="' + detph + '"]').text();
        $('.sub_nav li[data-subnav="' + detph + '"]').addClass('on');
        $('.sub_nav_wrap .now_btn').text(nowTxt);

        $('.sub_nav_wrap .now_btn').on('click', function() {
            $('.now_btn').toggleClass('on')
            $('.sub_nav').toggle();
        })
    });




</script>

