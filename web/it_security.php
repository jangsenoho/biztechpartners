<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub web security" data-depth="web" data-menu="web_05" data-subnav="web_05">
    <div class="inner_1200">

                      <?php include($_SERVER['DOCUMENT_ROOT'].'/web/web_nav.php');?>

        <div class="sub_cont">

            <!-- <h2 class="ttl ttl_02">Cloud/Infra/IT보안</h2> -->
            <ul class="sub_nav_02">
                <li class=""><a href="/web/cloud_infra.php">Cloud/Infra</a></li>
                <li class="on"><a href="/web/it_security.php">IT보안</a></li>
            </ul>

            <div class="ttl_box mar_ov  m50">
                <h2 class="ttl">IT보안</h2>
                <span class="line"></span>
                <p class="desc_02">디지털 트랜스포메이션을 통해 비즈니스를 전환하는 기업이 증가하고 있습니다. 디지털 트랜스포메이션으로의 전환 과정에서 <br>랜섬웨어, 해킹 등의 보안이슈가 실질적인 고민 사항입니다. <br>안전한 시스템 도입과 활용을 위해 기업에서는 사이버 위협과 보안 사고에 대비할 수 있는 보안 전략이 필수입니다. <br>비즈테크는 보안시스템 구축부터 운영, 관제까지 사이버 보안의 전 영역을 서비스합니다.​</p>
            </div>


            <div class="cont_box">
                <div class="img_wrap show">
                    <img src="../img/sub/web_it_03_m.jpg" alt="" class="only_m">
                    <img src="../img/sub/web_it_03.jpg" alt="" class="only_w">
                </div>
                <p class="desc_02">비즈테크는 고객의 비즈니스전략과 환경에 최적화된 보안 시스템구축, 운영 및 보안 관제까지 보안에 관한 모든 것을 제공합니다​</p>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>제공 보안 서비스 </span></h3>
                </div>

                <ul class="ico_w25">
                    <li>
                        <div class="color_wrap">
                            <span class="pi" style=" background:#c00c3f "><div><i></i>보안SI</div></span>
                        </div>
                        <p><i>·</i>IT 보안 시스템 구축</p>
                        <p><i>·</i>클라우드 보안시스템 설계 <br>(AWS, Azure,GCP등)</p>
                        <p><i>·</i>IoT보안 시스템구축</p>
                    </li>
                    <li>
                        <div class="color_wrap">
                            <span class="gr" style=" background:#b0b0b0 "><div><i></i>보안운영</div></span>
                        </div>
                        <p><i>·</i>보안솔루션 통합 운영</p>
                        <p><i>·</i>보안 솔루션 전문가 그룹</p>
                        <p><i>·</i>기술지원</p>
                    </li>
                    <li>
                        <div class="color_wrap">
                            <span class="dgr" style=" background:#707070 "><div><i></i>보안관제</div></span>
                        </div>
                        <p><i>·</i>24x365 보안사고 모니터링</p>
                        <p><i>·</i>해킹사고 분석/복구지원</p>
                        <p><i>·</i>원인분석 및 보고</p>
                        <p><i>·</i>취약점 진단</p>
                    </li>
                </ul>
               
            </div>

            

        </div>











        </div>

    </div> <!-- inner -->

    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="">
            </div>
           
		</div>
	</div> 



</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
     $('.m_header').addClass('sub')
    });
</script>