<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub web over_fa" data-depth="web" data-menu="web_03" data-subnav="web_03">
    <div class="inner_1200">

                      <?php include($_SERVER['DOCUMENT_ROOT'].'/web/web_nav.php');?>

        <div class="sub_cont">

            <!-- <h2 class="ttl ttl_02">Smart Factory</h2> -->
            <ul class="sub_nav_02">
                <li class="on"><a href="/web/smart_factory_01.php">Overview</a></li>
                <li class=""><a href="/web/smart_factory_02.php">스마트물류</a></li>
            </ul>

            <div class="ttl_box     m50">
                <h2 class="ttl">Business Coverage</h2>
                <span class="line"></span>
                <strong>Smart Factory 사업은 제조경쟁력 강화를 위해 공장기획 단계에 <span>최적화 설계를 서비스</span>하고, <br class="only_w"><span>IT 솔루션 및 지능화 설비를 공급</span>하여 <span>현장 전체를 Integration</span>하는 엔지니어링 사업</strong>
            </div>

            <div class="cont_box">
                
                <div class="img_wrap show">
                    <img src="../img/sub/over_view_pro_m.jpg" alt="" class="only_m">
                    <img src="../img/sub/over_view_pro.png" alt="" class="only_w">
                </div>
               
            </div>

            <div class="cont_box ttl_box">
                <h2 class="ttl">Big Picture</h2>
                <span class="line"></span>
                <div class="img_wrap show">
                    <img src="../img/sub/over_view_pro02_m.png" alt="" class="only_m">
                    <img src="../img/sub/over_view_pro02.png" alt="" class="only_w">
                </div>
               
            </div>

            <div class="cont_box ttl_box">
                <h2 class="ttl">솔루션</h2>
                <span class="line"></span>
                <div class="img_wrap show">
                    <img src="../img/sub/over_view_pro03_m.png" alt="" class="only_m">
                    <img src="../img/sub/over_view_pro03.png" alt="" class="only_w">
                </div>
               
            </div>

            <div class="cont_box ttl_box">
                <h2 class="ttl">지원 사업장</h2>
                <span class="line"></span>
                <div class="img_wrap show">
                    <img src="../img/sub/web_s_f_04_m.png" alt="" class="only_m">
                    <img src="../img/sub/web_s_f_04.png" alt=""class="only_w">
                </div> 
               
            </div>

          
           

             
      










        </div>

    </div> <!-- inner -->

    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="">
            </div>
           
		</div>
	</div>
     


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
  $('.m_header').addClass('sub')
    });
</script>