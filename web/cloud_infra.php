<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub web infra" data-depth="web" data-menu="web_05" data-subnav="web_05">
    <div class="inner_1200">

                      <?php include($_SERVER['DOCUMENT_ROOT'].'/web/web_nav.php');?>

        <div class="sub_cont infra">

            <!-- <h2 class="ttl ttl_02">Cloud/Infra/IT보안</h2> -->
            <ul class="sub_nav_02">
                <li class="on"><a href="/web/cloud_infra.php">Cloud/Infra</a></li>
                <li class=""><a href="/web/it_security.php">IT보안</a></li>
            </ul>

            <div class="ttl_box mar_ov  m50">
                <h2 class="ttl">Cloud/Infra</h2>
                <span class="line"></span>
                <p class="desc_02">IT 인프라는 엔터프라이즈 IT 환경에서 서비스와 자원을 개발, 운영, 관리하기 위한 기반이며, <br class="only_w">
                이러한 구성 요소에는 하드웨어, 소프트웨어, 서버, 스토리지, 네트워크, 가상화, 운영체제(OS), 미들웨어, DB 등이<br class="only_w">
                핵심 요소로서, 모두 IT 서비스 및 솔루션을 제공하는데 사용됩니다.
                </p>

                <p class="desc_02">Biztech Partners는 On-Premise 및 클라우드 환경에서 IT인프라의 설계 / 구축 / 운영 / 유지보수를 <br class="only_w">
                위한 사업과 기술력을 강화하고 있습니다.</p>
            </div>


            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>사업내용</span></h3>
                </div>
                <div class="dash_box">
                    <strong>분야</strong>
                    <ul>
                        <li>
                            <div class="img_wrap">
                                <img src="../img/sub/infra_01.jpg" alt="" class="only_w">
                                <img src="../img/sub/infra_01_m.jpg" alt="" class="only_m">
                            </div>
                            <span>On-Premise <br>Data Center</span>
                        </li>
                        <li>
                            <div class="img_wrap">
                                <img src="../img/sub/infra_02.jpg" alt="" class="only_w">
                                <img src="../img/sub/infra_02_m.jpg" alt="" class="only_m">
                            </div>
                            <span>VDI <br>클라우드 PC</span>
                        </li>
                        <li>
                            <div class="img_wrap">
                                <img src="../img/sub/infra_03.jpg" alt="" class="only_w">
                                <img src="../img/sub/infra_03_m.jpg" alt="" class="only_m">
                            </div>
                            <span>Cloud<br>Public/Private/Multi</span>
                        </li>
                    </ul>
                </div>

                <div class="dash_box txt">
                    <strong>요소기술</strong>
                    <ul>
                        <li>서버</li>
                        <li>스토리지</li>
                        <li>네트워크</li>
                        <li>미들웨어</li>
                        <li>DB</li>
                        <li>VDI</li>
                        <li>AWS</li>
                        <li>Azure</li>
                        <li>GCP</li>
                        <li>Open Source</li>
                    </ul>
                </div>

                <div class="dash_box">
                    <strong>분야</strong>
                    <ul>
                        <li>
                            <div class="img_wrap">
                                <img src="../img/sub/infra_04.jpg" alt="" class="only_w">
                                <img src="../img/sub/infra_04_m.jpg" alt="" class="only_m">
                            </div>
                            <span>설계 / 제안 / 구축</span>
                        </li>
                        <li>
                            <div class="img_wrap">
                                <img src="../img/sub/infra_05.jpg" alt="" class="only_w">
                                <img src="../img/sub/infra_05_m.jpg" alt="" class="only_m">
                            </div>
                            <span>운영</span>
                        </li>
                        <li>
                            <div class="img_wrap">
                                <img src="../img/sub/infra_06.jpg" alt="" class="only_w">
                                <img src="../img/sub/infra_06_m.jpg" alt="" class="only_m">
                            </div>
                            <span>유지보수 기술 지원 </span>
                        </li>
                    </ul>
                </div>
            </div>

           

        </div>











    </div> <!-- inner -->



</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>