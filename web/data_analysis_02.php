<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub web data_an an_area" data-depth="web" data-menu="web_04" data-subnav="web_04">
    <div class="inner_1200">
                      <?php include($_SERVER['DOCUMENT_ROOT'].'/web/web_nav.php');?>

        <div class="sub_cont" data-depth="data" data-menu="data_02" data-datanav="data_02">

            <!-- <h2 class="ttl ttl_02">Data Analysis</h2> -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/web/data_nav.php');?>

            <div class="ttl_box     m50">
                <h2 class="ttl">DW</h2>
                <span class="line"></span>
                <p class="desc_02">고객 요구사항의 명확한 이해와 <span>정확한 데이터 제공을 통한 기업의 의사결정</span> 최적 지원 시스템을 구축합니다.</p>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>사업</span> 내용</h3>
                </div>
                
                <ul class="gray_asome">
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/dw_01.png" alt="" class="">
                        </div>
                        <strong>ETL</strong>
                        <p> <i>·</i> 대용량 데이터 변환, 정제, 검증 수행을 <br class="only_w">통한 데이터 정합성 보장</p>
                        <p> <i>·</i>  작업 프로세스에 대한 자동화, 최적화, <br class="only_w">효율화 수행</p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/dw_02.png" alt="" class="">
                        </div>
                        <strong>데이터 모델링</strong>
                        <p> <i>·</i> 데이터 모델링 및 품질검증을 통한 <br class="only_w">효율적 데이터 관리 모델 구현</p>
                        <p> <i>·</i> 성능최적화를 위한 모델 관리 방안 제시</p>
                    </li>
                    <li class="dw_03">
                        <div class="ico_wrap">
                            <img src="../img/sub/dw_03.png" alt=" " class="">
                        </div>
                        <strong>OLAP, 시각화 구현</strong>
                        <p> <i>·</i> 다양한 관점에 대한 분석 가능한 <br class="only_w">의사 결정 분석 레포트 제공</p>
                        <p> <i>·</i> 데이터 생성 및 관리를 통한<br class="only_w">모니터링을 수행</p>
                    </li>
                </ul>

                <ul class="line_box_wrap w1000">
                    <li class="red">
                        <!-- BEGIN 2022 04 26 텍스트 수정  --> 
                        <strong>고객에게 보다 가치있고 만족스러운 서비스 제공</strong>
                         <!-- END 2022 04 26 텍스트 수정  -->  
                        <p>정보 통합 및 분석 활용을 위한 DW 아키텍처 및 구축에 대한 컨설팅 수행으로 고객에게 보다 가치있는 최적의 서비스를 제공합니다.</p>
                    </li>
                    <li >
                        <strong>정보 활용의 전략적 가치 상승</strong>
                        <p>DW 시스템 도입에 대한 정보화 전략 계획 및 현 정보계 시스템 진단/평가를 통해 정보 활용의 전략적 가치를 높일 수 있도록 합니다.</p>
                    </li>
                </ul>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl">About <span>DW</span></h3>
                </div>
                <div class="img_wrap show">
                    <img src="../img/sub/dw_04_m.jpg" alt="" class="only_m">
                    <img src="../img/sub/dw_04.png" alt="" class="only_w" >
                </div>

                <ul class="gray_asome_02 ">
                    
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/dw_05.png" alt="" class="only_w">
                            <img src="../img/sub/dw_05_m.jpg" alt="" class="only_m" style="max-width:60px ">
                        </div>
                        <p><b>신속하고 정확한</b> 경영의사 결정을 지원해 주는 <span>정보환경을 구축</span></p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/dw_06.png" alt="" class="only_w">
                            <img src="../img/sub/dw_06_m.jpg" alt="" class="only_m" style="max-width:60px ">
                        </div>
                        <p><b>원활한 의사소통</b> <br class="only_m"><span>환경을 제공</span></p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/dw_07.png" alt="" class="only_w">
                            <img src="../img/sub/dw_07_m.jpg" alt="" class="only_m" style="max-width:60px ">
                        </div>
                        <p><b>디테일하고 정확한 데이터 기반으로</b> 보다 진화된 업무를 진행함으로써 <span>업무 효율성 증대</span></p>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <img src="../img/sub/dw_08.png" alt=""  class="only_w">
                            <img src="../img/sub/dw_08_m.jpg" alt="" class="only_m" style="max-width:60px ">
                        </div>
                       <p><b>데이터의 통합에 따른</b> 데이터 정합성 보장 및 <span>통합 관점 데이터 제공</span></p>
                    </li>
                </ul>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl">수행영역</h3>
                </div>
                <p class="desc_02">다양한 유형의 원천 데이터를 기반으로 효율적인 데이터의 수집, 가공, 리포팅을 위한 데이터 파이프라인을 구성하고 <br class="only_w">
                    데이터를 통한 기업내 문제해결을 위해 <span>데이터 분석가가 필요로 하는 가치있는 데이터를 제공</span>합니다. </p>

                <div class="dot_wrap an_area">
                    <strong><span>DW 영역</span>(데이터 수집/통합/요약/리포팅) <br class="only_m"> - <span>Data Engineer</span></strong>
                </div>
                <div class="img_wrap show">
                    <img src="../img/sub/over_view_pro04_m.png" alt="" class="only_m">
                    <img src="../img/sub/over_view_pro04.png" alt="" class="only_w">
                </div>
            </div>



        </div>

    </div> <!-- inner -->

    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="">
            </div>
           
		</div>
	</div>


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>