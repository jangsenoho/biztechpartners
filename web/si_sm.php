<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub web sism" data-depth="web" data-menu="web_01" data-subnav="web_01">
    <div class="inner_1200">

               <?php include($_SERVER['DOCUMENT_ROOT'].'/web/web_nav.php');?>

        <div class="sub_cont">

            <!-- <h2 class="ttl ttl_02">SI/SM​</h2>
            <ul class="sub_nav_02">
                <li class="on"><a href="/web/si_sm.php">SI/SM</a></li>
            </ul> -->

            <div class="ttl_box mar_ov  m50">
                <h2 class="ttl">시스템 통합(SI: System Integration) 사업 영역</h2>
                <span class="line"></span>
                <p class="desc_02">저희 <span>Biztechpartners는 LG CNS의 자회사로 LG그룹 계열사, 관계사 및 Open 고객들을 대상으로 시스템 구축 통합 서비스를 제공</span>하고 있습니다. ​</p>
                <p class="desc_02 m20">고객들의 시스템 구축 요구 특성에 맞춰 전통적인 Monolithic Architecture 기반의 Waterfall 이행 방식 및 Global System Development Methodology <br class="only_w">Trend를 반영한 MSA 기반의 Agile 방법론을 상황에 맞게 활용하여 고객 만족도 높은 시스템 구축 서비스를 제공하고 있습니다. </p>
            </div>

            <!--  Waterfall 이행방식 -->
            <div class="cont_box"> 
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>Monolithic Architecture 기반</span>의 Waterfall 이행방식​</h3>
                </div>
                <div class="img_wrap show">
                    <img src="../img/sub/web_si_01_m.jpg" alt="" class="only_m">
                    <img src="../img/sub/web_si_01.jpg" alt="" class="only_w">
                </div>
            </div>
            <!-- -----------------------------------------   // Waterfall 이행방식 -->

            <!-- Agile 이행방식 -->
            <div class="cont_box">
                <div class="bor_ttl_box"> 
                    <h3 class="bor_ttl"><span>Micro-Service Architecture 기반</span>의 Agile 이행방식​</h3>
                </div>
                <div class="img_wrap show">
                    <img src="../img/sub/web_si_02_m.jpg" alt="처리 프로세스"  class="only_m">
                    <img src="../img/sub/sism_01.jpg" alt="처리 프로세스"  class="only_w">
                </div>
            </div>
            <!-- -----------------------------------------   // Agile 이행방식 -->

            <!-- 시스템 유지관리 사업영역 -->
            <div class="cont_box">
                <div class="ttl_box mar_ov  m0">
                    <h2 class="ttl">시스템 유지관리 (SM:System Maintenance)<span> 사업영역​</span></h2>
                    <span class="line"></span>
                </div>

                <ul class="top_line w1000">
                    <li>
                        <h3><i>01.</i> <span>운영 및</span> 유지관리</h3>
                        <!-- BEGIN 2022 04 26 텍스트 수정  -->
                        <p> LG그룹 Family, 관계사 및 Open 고객에 대한 시스템 운영(Operation) 및 유지관리(Maintenance) 서비스를 제공합니다. </p>
                        <!-- END 2022 04 26 텍스트 수정  -->  
                    </li>
                    <li>
                        <h3><i>02.</i> <span>Business Life Cycle Service</span> 제공</h3>
                        <p>우수한 업무 수행 및 개발 능력을 바탕으로 고객 업무에 대한 만족도 높은 Business Life Cycle Service를 제공하고 있습니다. </p>
                    </li>
                    
                </ul>
            </div>
            <!-- -------------------------------------  //시스템 유지관리 사업영역 -->


        </div>

    </div> <!-- inner -->

    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="">
            </div>
           
		</div>
	</div> 


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(1).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('sub')
    });
</script>