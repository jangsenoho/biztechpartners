<ul class="sub_gnb">
    <li ><a href="/people/people.php">인재상</a></li></li>
    <li ><a href="/people/people_system01.php">인사제도</a></li>
    <li  class='on'><a href="/people/it_part.php">직무소개​</a></li>
    <li  ><a href="/people/people_notice.php">채용공고​</a></li>
</ul>

<div class="sub_nav_wrap">
    <button type="button" class="now_btn only_m"></button>
    <ul class="sub_nav_02 j_c sub_toggle">
        <li data-partnav="part_01"><a href="/people/it_part.php">IT</a></li>
        <li data-partnav="part_02"><a href="/people/bpo_part.php">BPO​</a></li>
        <li data-partnav="part_03"><a href="/people/staff_part.php">STAFF</a></li>
    </ul>
</div>

<script>
   
    $(function () {

        var detph = $('.sub_cont').data('partnav');
        var nowTxt = $('.sub_nav_02 li[data-partnav="' + detph + '"]').text();
        $('.sub_nav_02 li[data-partnav="' + detph + '"]').addClass('on');
        $('.sub_nav_wrap .now_btn').text(nowTxt);

        $('.sub_nav_wrap .now_btn').on('click', function() {
            $('.now_btn').toggleClass('on')
            $('.sub_nav_02').toggle();
        })
    });




</script>