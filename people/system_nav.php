<ul class="sub_gnb">
    <li ><a href="/people/people.php">인재상</a></li></li>
    <li class='on'><a href="/people/people_system01.php">인사제도</a></li>
    <li  ><a href="/people/it_part.php">직무소개​</a></li>
    <li  ><a href="/people/people_notice.php">채용공고​</a></li>
</ul>

<div class="sub_nav_wrap">
    <button type="button" class="now_btn only_m"></button>
    <ul class="sub_nav_02 j_c sub_toggle">
        <li data-stynav="system_01"><a href="/people/people_system01.php">인사원칙</a></li>
        <li data-stynav="system_02"><a href="/people/people_system02.php">복리후생​</a></li>
        <li data-stynav="system_03"><a href="/people/people_system03.php">교육체계​</a></li>

    </ul>
</div>

<script>
   
    $(function () {

        var detph = $('.sub_cont').data('stynav');
        var nowTxt = $('.sub_nav_02 li[data-stynav="' + detph + '"]').text();
        $('.sub_nav_02 li[data-stynav="' + detph + '"]').addClass('on');
        $('.sub_nav_wrap .now_btn').text(nowTxt);

        $('.sub_nav_wrap .now_btn').on('click', function() {
            $('.now_btn').toggleClass('on')
            $('.sub_nav_02').toggle();
        })
    });





</script>