<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>
<section id="container" class="sub people notice" data-depth="people" data-menu="peo_04" data-subnav="peo_04" >
    <div class="inner_1200">
    <?php include($_SERVER['DOCUMENT_ROOT'].'/people/peo_nav.php');?>

            <ul class="sub_gnb">
                <li ><a href="/people/people.php">인재상</a></li></li>
                <li ><a href="/people/people_system01.php">인사제도</a></li>
                <li  ><a href="/people/it_part.php">직무소개​</a></li>
                <li  class='on'><a href="/people/people_notice.php">채용공고​</a></li>
            </ul>


        <div class="sub_cont" >
            
            <!-- <h2 class="ttl ttl_02">채용공고</h2>
            <ul class="sub_nav_02">
                <li class="on"><a href="/bpo/operation_service.php">채용 절차</a></li>
            </ul> -->


            <div class="ttl_box ">
                <h2 class="ttl">채용절차</h2>
                <span class="line"></span>
                <p class="desc_02"> 최고의 인재들이 모여 일등의 꿈을 실현합니다.​</p>
                <div class="img_wrap">
                    <img src="../img/sub/people_01_m.png" alt="" class="only_m">
                    <img src="../img/sub/people_01.png" alt="" class="only_w">
                </div>
            </div>
            


            <div class="cont_box">
                <h2 class="ttl">진행중인 공고</h2>

                <table>
                    <colgroup>
                        <col width="13%">
                        <col width="*">
                        <col width="10%">
                    </colgroup>
                    <thead>
                        <tr>
                            <th>지원구분</th>
                            <th>공고제목</th>
                            <th>지원가능</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><span class="new_career">신입/경력</span></td>
                            <td class="ttl">
                                <a href="./people_view.php">
                                    <span class="new_career only_m">[신입/경력]</span>
                                    프로젝트 청구 빌링 및 Admin 업무 담당자 채용
                                </a>
                            </td>
                            <td class="progress">접수중</td>
                        </tr>
                        <tr>
                            <td><span class="new_career">신입/경력</span></td>
                            <td class="ttl">
                                <a href="./people_view.php">
                                    <span class="new_career only_m">[신입/경력]</span>
                                    청구 빌링 및 광고비 관리 업무 담당자 채용
                                </a>
                            </td>
                            <td class="progress">접수중</td>
                        </tr>
                        <tr>
                            <td><span class="new_career">신입/경력</span></td>
                            <td class="ttl">
                                <a href="./people_view.php">
                                    <span class="new_career only_m">[신입/경력]</span>
                                    교육운영 업무 담당자 채용
                                </a>
                            </td>
                            <td class="progress">접수중</td>
                        </tr>
                        <tr>
                            <td><span class="new_career">경력</span></td>
                            <td class="ttl">
                                <a href="./people_view.php">
                                    <span class="new_career only_m">[경력]</span>
                                    VDI운영/IT보안/IT유지보수 엔지니어 채용
                                </a>
                            </td>
                            <td class="progress">접수중</td>
                        </tr>
                        <tr>
                            <td><span class="new_career">신입/경력</span></td>
                            <td class="ttl">
                                <a href="./people_view.php">
                                    <span class="new_career only_m">[신입/경력]</span>
                                    개인정보 관리 업무 담당자 채용
                                </a>
                            </td>
                            <td class="progress">접수중</td>
                        </tr>
                        <tr>
                            <td><span class="new_career">신입</span></td>
                            <td class="ttl">
                                <a href="./people_view.php">
                                    <span class="new_career only_m">[신입]</span>
                                    스마트팩토리(PLM/PMS, MES/ECS/QMS, 유통물류, 안전환경) 채용
                                </a>
                            </td>
                            <td class="progress">접수중</td>
                        </tr>
                        <tr>
                            <td><span class="new_career">경력</span></td>
                            <td class="ttl">
                                <a href="./people_view.php">
                                    <span class="new_career only_m">[경력]</span>
                                    RPA, 은행 및 금융IT, Front End, 모바일, 챗봇, MSA 개발자 채용
                                </a>
                            </td>
                            <td class="progress">접수중</td>
                        </tr>
                        <tr>
                            <td><span class="new_career">경력</span></td>
                            <td class="ttl">
                                <a href="./people_view.php">
                                    <span class="new_career only_m">[경력]</span>
                                    JAVA기반 프로젝트 PL/분석/설계/개발자 인재 채용
                                </a>
                            </td>
                            <td class="progress">접수중</td>
                        </tr>
                        <tr>
                            <td><span class="new_career">경력</span></td>
                            <td class="ttl">
                                <a href="./people_view.php">
                                    <span class="new_career only_m">[경력]</span>
                                    BI/DW 분야 경력자 모집
                                </a>
                            </td>
                            <td class="progress">접수중</td>
                        </tr>
                        <tr>
                            <td ><span class="new_career">경력</span></td>
                            <td class="ttl">
                                <a href="./people_view.php">
                                    <span class="new_career only_m">[경력]</span>
                                    BI/DW 분야 경력자 모집
                                </a>
                            </td>
                            <td class="progress">접수중</td>
                        </tr>
                    </tbody>
                </table>

                <div class="people_link">
                    <a href="https://apply.lg.com/app/job/RetrieveJobNotices.rpi?requestMenuId=1075&sCompanyCode=BIZ" class="go_link" target="_blank">LG Careers 바로가기 <i></i></a>
                    <a href="https://www.jobkorea.co.kr/Search/?stext=%EB%B9%84%EC%A6%88%ED%85%8C%ED%81%AC%ED%8C%8C%ED%8A%B8%EB%84%88%EC%8A%A4" class="go_link job" target="_blank">잡코리아 바로가기 <i></i></a>
                    <a href="https://www.saramin.co.kr/zf_user/search?search_area=main&search_done=y&search_optional_item=n&searchType=search&searchword=%EB%B9%84%EC%A6%88%ED%85%8C%ED%81%AC%ED%8C%8C%ED%8A%B8%EB%84%88%EC%8A%A4" class="go_link peo" target="_blank">사람인 바로가기 <i></i></a>
                </div>

             
            
            </div>

         
        </div>
    </div>


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(2).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('peo')
    });
</script>