<?php include($_SERVER['DOCUMENT_ROOT'] . '/inc/head_inc.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/inc/header_m.php'); ?>

<section id="container" class="sub people " data-depth="people" data-menu="peo_03" data-subnav="peo_03">
    <div class="inner_1200">
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/people/peo_nav.php'); ?>

        <div class="sub_cont" data-depth="people" data-menu="part_02" data-partnav="part_02">

            <!-- <h2 class="ttl ttl_02">직무소개</h2> -->
            <?php include($_SERVER['DOCUMENT_ROOT'] . '/people/part_nav.php'); ?>

            <div class="ttl_box mar_ov  m0 ">
                <h2 class="ttl">BPO</h2>
                <span class="line"></span>
            </div>

            <div class="cont_box">
                <ul class="part_list">
                    <li class="part_top">
                        <div class="part_ttl">직무</div>
                        <div class="part_desc">내용</div>
                    </li>
                    <li>
                        <div class="part_ttl">P&B <br class="only_m">(Payroll & Benefit) </div>
                        <div class="part_desc">
                            <span>[급여]</span>
                            <!-- BEGIN 2022 04 26 텍스트 수정  -->  
                            <p><i>·</i> 근로기준법 및 세법 등의 법적 근거와 고객사의 HR제도, 규칙 파악, 매월 급/상여를 정확하게 계산 · 지급 </p>
                            <!-- END 2022 04 26 텍스트 수정  -->  
                            <p><i>·</i> 임금과 관련한 법규와 개정사항 확인 및 이해, 업무 반영 </p>
                            <p><i>·</i> 사회보험(고용보험, 건강보험, 국민연금, 산재보험) 관련 법령에 의거 보험료 계산 · 납부 </p>
                            <p><i>·</i> 임직원의 근무 변경(발령)사항을 확인, 사회보험료 산출 반영 </p>
                            <p><i>·</i> 정기 보험료 계산, 납부 외 각 정부 지원금 관리 항목 모니터링 및 환급 진행 </p>
                        </div>
                    </li>
                    <li>
                        <div class="part_ttl">P&B <br class="only_m">(Payroll & Benefit)</div>
                        <div class="part_desc">
                            <span>[복리후생]</span>
                             <!-- BEGIN 2022 04 26 텍스트 수정  -->  
                            <p><i>·</i>고객사의 복리후생 (학자금, 본인의료비, 가족의료비, 휴무지원비 등) 규정에 의거하여 제도의 운영이 원활히 이루어지도록 관리 </p>
                             <!-- END 2022 04 26 텍스트 수정  -->  
                            <p><i>·</i>각 기준에 부합하는 증빙 서류 등을 검토, 시스템 반영 </p>
                            <p><i>·</i>고객사의 복리후생 기준 이해, 임직원 문의에 대응 </p>
                        </div>
                    </li>
                    <li>
                        <div class="part_ttl">교육운영</div>
                        <div class="part_desc">
                            <p><i>·</i>고객사 집체교육, 온라인(비대면) 교육 환경 운영 </p>
                            <p><i>·</i>교육 운영과 관련한 일반 행정 업무 수행 </p>
                        </div>
                    </li>
                    <li>
                        <div class="part_ttl">경영지원</div>
                        <div class="part_desc">
                            <p><i>·</i>인사/재무/총무/구매 분야 등 경영지원 직무 </p>
                            <div>
                                <span>[PO]</span>
                                <p><i>·</i>고객사 해외 A/S센터 서비스 부품 수요 예측 </p>
                                <p><i>·</i>서비스 부품 발주 진행 및 점검 </p>
                                <p><i>·</i>해외 서비스 법인별 재고금액 관리(재고 회전율 관리) </p>
                                <!-- BIGIN: 2022 04 25 텍스트 수정  -->
                                <p><i>·</i>서비스 품질 관리 - 필요부품 적기 공급을 위한 모니터링 및 관리 </p>
                                <!-- END: 2022 04 25 텍스트 수정  -->
                            </div>
                            <div>
                                <span>[사무행정]</span>
                                <!-- BEGIN 2022 04 26 텍스트 수정  -->
                                <p><i>·</i>기업 내 PC 등 OA 공급 및 자산운영 관리</p>
                                <!-- END 2022 04 26 텍스트 수정  -->  
                                <p><i>·</i>총무/사무 일반 행정 업무 대행 </p>
                                <p><i>·</i>기업 임직원 보건 관리 기획 및 운영 </p>
                                <p><i>·</i>사무 행정 자동화 관련 지원 업무 </p>
                            </div>
                            <div>
                                <span>[개인정보 영향평가]</span>
                                <p><i>·</i>영향평가 진단 및 평가 실시 관리프로세스 구축 및 운영기준 제안 </p>
                                <p><i>·</i>영향평가 개선계획 이행점검 및 평가 이슈사항 공유 및 조치 </p>
                                <p><i>·</i>CPPG(개인정보관리사) 자격증 보유 및 관련법 이해 및 적용 </p>
                                <!-- BIGIN: 2022 04 25 텍스트 수정  -->
                                <p><i>·</i>기업의 개인정보보호 규칙에 맞는 서비스 및 네트워크(서버·DB)구조, 분석 및 개인정보수집항목 파악 및 적용 </p>
                                <!-- END: 2022 04 25 텍스트 수정  -->
                            </div>

                        </div>
                    </li>
                </ul>
            </div>






        </div>

    </div> <!-- inner -->




</section>

<?php include($_SERVER['DOCUMENT_ROOT'] .'/inc/footer.php'); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#gnb ul').children().eq(2).addClass('active');

        // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
        $('.m_header').addClass('peo')
    });
</script>