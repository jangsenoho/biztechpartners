<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub people people_01" data-depth="people" data-menu="peo_01" data-subnav="peo_01">
    <div class="inner_1200">

    <?php include($_SERVER['DOCUMENT_ROOT'].'/people/peo_nav.php');?>

        <ul class="sub_gnb">
            <li class='on'><a href="/people/people.php">인재상</a></li></li>
            <li ><a href="/people/people_system01.php">인사제도</a></li>
            <li  ><a href="/people/it_part.php">직무소개​</a></li>
            <li  ><a href="/people/people_notice.php">채용공고​</a></li>
        </ul>


        <div class="sub_cont" data-depth="people" data-menu="system_03" data-stynav="system_03">

            <!-- <h2 class="ttl ttl_02">인재상</h2>
            <ul class="sub_nav_02">
                <li class="on"><a href="/people/people.php">인재상</a></li>
            </ul> -->

            <div class="ttl_box mar_ov  m0 ">
                <h2 class="ttl">인재상</h2>
                <span class="line"></span>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>LG WAY</span>를 위한 신념과 실행력을 겸비한 사람</h3>
                </div>
                <p class="desc_02">LG WAY는 LG 고유의 경영철학이자, LG 전 임직원이 지키고 <br class="only_m">  실천해야 할 사고와 행동의 기반입니다.​ </p>
                <p class="desc_02">LG WAY는 경영이념인 “고객을 위한 가치창조”와 <br class="only_m"> “인간존중의 경영”을 LG의 행동방식인 “정도경영”으로 ​<br>실천함으로써 LG의 비전인 “일등LG”를 달성하자는 것입니다.​</p>
                <div class="dot_wrap m40">
                    <strong><span>LG WAY</span></strong>
                </div>
                <div class="img_wrap show">
                    <img src="../img/sub/people_p_m.png" alt="" class="only_m" >
                    <img src="../img/sub/people_p.png" alt=""  class="only_w">
                </div>

                <ul class="ico_s_33w ta">
                    <li>
                      
                        <strong>비전</strong>
                        <p><i> · </i> LG의 궁극적인 지향점으로 시장에서 <br class="only_w">
                            인정받으며 시장을 리드하는<br class="only_w">
                            선도기업이 되는 것
                        </p>
                    </li>
                    <li>
                       
                        <strong>행동방식</strong>
                        <p><i> · </i> 윤리경영을 기반으로 꾸준히 실력을<br class="only_w">
                            배양해 정정당당하게  승부하는<br class="only_w">
                            LG만의 행동방식
                        ​</p>
                    </li>
                    <li>
                        
                        <strong>경영이념</strong>
                        <p><i> · </i> 기업의 존립근거이자 <br class="only_w">
                            회사운영의 원칙
                        </p>
                    </li>
                </ul>

            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>비즈테크파트너스</span>가 원하는 인재상</h3>
                </div>
                <ul class="service_list">
                    <li>
                        <strong><span>P</span>ASSION</strong>
                        <ul>
                            <li><i>·</i>  꿈과 열정을 가지고 세계 최고에 도전하는 사람</li>
                        </ul>
                    </li>
                    <li>
                        <strong><span>I</span>NNOVATION</strong>
                        <ul>
                            <li><i>·</i> 고객을 최우선으로 생각하고 끊임없이 혁신하는 사람</li>
                        </ul>
                    </li>
                    <li>
                        <strong><span>O</span>RIGINALITY</strong>
                        <ul>
                            <li><i>·</i>  팀워크를 이루며 자율적이고 창의적으로 일하는 사람</li>
                        </ul>
                    </li>
                    <li> 
                        <strong><span>C</span>OMPETITION</strong>
                        <ul>
                            <li><i>·</i>  꾸준히 실력을 배양하여 정정당당하게 경쟁하는 사람</li>
                        </ul>
                    </li>
               </ul>
            </div>

           
            
            
           

        </div>

    </div> <!-- inner -->
     
    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="">
            </div>
           
		</div>
	</div>


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(2).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('peo')
    });
</script>