<?php include($_SERVER['DOCUMENT_ROOT'] . '/inc/head_inc.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/inc/header_m.php'); ?>

<section id="container" class="sub people " data-depth="people" data-menu="peo_03" data-subnav="peo_03">
    <div class="inner_1200">

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/people/peo_nav.php'); ?>

        <div class="sub_cont" data-depth="people" data-menu="part_03" data-partnav="part_03">

            <!-- <h2 class="ttl ttl_02">직무소개</h2> -->
            <?php include($_SERVER['DOCUMENT_ROOT'] . '/people/part_nav.php'); ?>

            <div class="ttl_box mar_ov  m0 ">
                <h2 class="ttl">STAFF</h2>
                <span class="line"></span>
            </div>

            <div class="cont_box ">
                <ul class="part_list ">
                    <li class="part_top">
                        <div class="part_ttl">직무</div>
                        <div class="part_desc">내용</div>
                    </li>
                    <li>
                        <div class="part_ttl">인사</div>
                        <div class="part_desc">
                            <p><i>·</i> LG WAY를 근간으로 핵심인재의 조기확보, 유지, 교육/육성​</p>
                            <p><i>·</i> 인사제도 기획, 조직구도 설계/운영, 평가, 보상​​</p>
                            <p><i>·</i> 구성원 Pain Point 발굴, 노경 관련 Risk 관리​​</p>
                            <p><i>·</i> 조직문화 구축 및 홍보/교육​​</p>
                        </div>
                    </li>
                    <li>
                        <div class="part_ttl">재무/기획</div>
                        <div class="part_desc">
                            <p><i>·</i> 경영방침 및 결산방향 수립​ </p>
                            <p><i>·</i> 당사 경영성과에 대한 분석/예측​</p>
                            <!-- BIGIN: 2022 04 25 텍스트 수정  -->
                            <p><i>·</i> 구축된 회계 및 영업상의 데이터를 이용하여 결산공고 수행​ </p>
                            <!-- END: 2022 04 25 텍스트 수정  -->
                            <p><i>·</i> 경영 및 투자에 필요한 Data 제공​​ </p>
                        </div>
                    </li>
                </ul>
            </div>




        </div>

    </div> <!-- inner -->




</section>

<?php include($_SERVER['DOCUMENT_ROOT'] .'/inc/footer.php'); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#gnb ul').children().eq(2).addClass('active');

        // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
        $('.m_header').addClass('peo')
    });
</script>