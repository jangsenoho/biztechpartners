<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub people system02" data-depth="people" data-menu="peo_02" data-subnav="peo_02">
    <div class="inner_1200">

    <?php include($_SERVER['DOCUMENT_ROOT'].'/people/peo_nav.php');?>

        <div class="sub_cont" data-depth="people" data-menu="system_02" data-stynav="system_02">

            <!-- <h2 class="ttl ttl_02">인사제도​</h2> -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/people/system_nav.php');?>

            <div class="ttl_box mar_ov  m50 ">
                <!-- <h2 class="ttl">복리후생</h2>
                <span class="line"></span> -->
                <p class="desc_02 ">
                구성원 개인의 발전과 더 나은 삶을 위해 다양한 복리후생 제도를 지원하고 있습니다.​
                </p>
            </div>

            <div class="cont_box">
                <div class="dot_wrap">
                    <strong><span>회사생활</span></strong>
                </div>

                <ul class="ico_s_100w dash">
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image: url(../img/sub/people_05.png)" class="only_w"></span> 
                            <span style="background-image: url(../img/sub/people_05_m.jpg)" class="only_m"></span> 
                            <strong>출퇴근 <br class="only_m">유연제도</strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>유연하게 출퇴근 시간을 조절하여 자기계발, 자녀 통학 등을 위한 시간에 활용할 수 있습니다.​</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image: url(../img/sub/people_06.png)" class="only_w"></span> 
                            <span style="background-image: url(../img/sub/people_06_m.jpg)" class="only_m"></span> 
                            <strong>선택적  <br class="only_m"> 복리후생</strong>
                        </div>
                        <div class="ico_desc">
                            <!-- BIGIN: 2022 04 25 텍스트 수정  -->
                            <p><i> · </i>매년 복리후생 포인트가 제공되며, 개인의 Needs에 따라 다양한 항목에 사용이 가능합니다.​</p>
                            <!-- END: 2022 04 25 텍스트 수정  -->
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image: url(../img/sub/people_07.png)" class="only_w"></span> 
                            <span style="background-image: url(../img/sub/people_07_m.jpg)" class="only_m"></span> 
                            <strong>기념일  <br class="only_m">선물</strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>임직원의 소중한 기념일을 위해 선물을 드립니다.​</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image: url(../img/sub/people_08.png)" class="only_w"></span> 
                            <span style="background-image: url(../img/sub/people_08_m.jpg)" class="only_m"></span> 
                            <strong>사내  <br class="only_m">동호회활동</strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>일과 삶의 균형을 위해 사내 동호회 활동비를 지원합니다.​</p>
                        </div>
                    </li>
                   
                </ul>
              
              
            </div>

            <div class="cont_box">
                <div class="dot_wrap">
                    <strong><span>가족 및 건강</span></strong>
                </div>

                <ul class="ico_s_100w dash">
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image: url(../img/sub/people_09.png)" class="only_w"></span> 
                            <span style="background-image: url(../img/sub/people_09_m.jpg)" class="only_m"></span> 
                            <strong>자녀  <br class="only_m">학자금</strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>임직원의 취학 자녀에 대한 학자금을 지원합니다. (초/중/고/대학교)​</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image: url(../img/sub/people_10.png)" class="only_w"></span> 
                            <span style="background-image: url(../img/sub/people_10_m.jpg)" class="only_m"></span> 
                            <strong>경조금</strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>임직원 본인과 가족의 결혼, 회갑 등 각종 경조사 시 경조금 및 경조 휴가를 지원합니다.​​</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image: url(../img/sub/people_11.png)" class="only_w"></span> 
                            <span style="background-image: url(../img/sub/people_11_m.jpg)" class="only_m"></span> 
                            <strong>휴가제도</strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>법정휴가, 창립기념일휴가 등을 통하여 구성원들의 Work & Life-Balance를 지원합니다.​</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image: url(../img/sub/people_13.png)" class="only_w"></span> 
                            <span style="background-image: url(../img/sub/people_13_m.jpg)" class="only_m"></span> 
                            <strong>건강검진  <br class="only_m">지원</strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>임직원 본인과 배우자에 대한 주기적인 종합 건강검진을 실시하고 있습니다.​​</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image: url(../img/sub/people_14.png)" class="only_w"></span> 
                            <span style="background-image: url(../img/sub/people_14_m.jpg)" class="only_m"></span> 
                            <strong>심리상담</strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i> 임직원 스트레스 관리 및 심리적 안정을 통한 건강한 생활을 위해 심리상담을 지원합니다.​​​</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image: url(../img/sub/people_15.png)" class="only_w"></span> 
                            <span style="background-image: url(../img/sub/people_15_m.jpg)" class="only_m"></span> 
                            <strong>단체보험</strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>임직원 본인의 중대 질병, 장애 발생 시 보험금을 지급합니다.​​​</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image: url(../img/sub/people_16.png)" class="only_w"></span> 
                            <span style="background-image: url(../img/sub/people_16_,.jpg)" class="only_m"></span> 
                            <strong>주거안전</strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>지방근무 임직원의 안정적 주거생활을 위해 사택 및 기숙사를 지원합니다.​​​</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image: url(../img/sub/people_17.png)" class="only_w"></span> 
                            <span style="background-image: url(../img/sub/people_17_m.jpg)" class="only_m"></span> 
                            <strong>휴양시설</strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>임직원 본인과 가족의 편안한 휴식을 위해 법인콘도를 운영합니다.​</p>
                        </div>
                    </li>
                </ul>
            </div>

            
            
           

        </div>

    </div> <!-- inner -->
     
   


</section>

<?php include($_SERVER['DOCUMENT_ROOT'] .'/inc/footer.php'); ?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(2).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('peo')
    });
</script>