<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub people system03" data-depth="people" data-menu="peo_02" data-subnav="peo_02">
    <div class="inner_1200">

    <?php include($_SERVER['DOCUMENT_ROOT'].'/people/peo_nav.php');?>

        <div class="sub_cont" data-depth="people" data-menu="system_03" data-stynav="system_03">

            <!-- <h2 class="ttl ttl_02">인사제도​</h2> -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/people/system_nav.php');?>

            <div class="ttl_box mar_ov  m50 ">
                <!-- <h2 class="ttl">교육체계</h2>
                <span class="line"></span> -->
                <p class="desc_02 ">
                ㈜비즈테크파트너스는 직무능력과 역량을 강화하기 위해 다양한 교육 프로그램을 운영하고 있으며, <br>
                임직원들이 전문가와 리더로 성장할 수 있도록 지원하고 있습니다.​
                </p>
            </div>

            <div class="cont_box">
                <div class="dot_wrap">
                    <strong><span>교육 프로그램</span></strong>
                </div>

                <ul class="ico_s_100w dash">
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image: url(../img/sub/people_18.png)" class="only_w"></span> 
                            <span style="background-image: url(../img/sub/people_18_m.jpg)" class="only_m"></span> 
                            <strong>신규입사자​ <br class="only_m"> 교육</strong>
                        </div>
                        <div class="ico_desc">
                            <p>온보딩교육 및 멘토링제도를 운영하여 신규입사자가 조직에 빠르게 적응할 수 있도록 돕습니다.​​</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image: url(../img/sub/people_06.png)"class="only_w" ></span> 
                            <span style="background-image: url(../img/sub/people_06_m.jpg)" class="only_m"></span> 
                            <strong>직무역량 <br class="only_m">강화 교육​</strong>
                        </div>
                        <div class="ico_desc">
                            <p>직무에 따른 필요 역량을 체계적으로 개발할 수 있도록​ 다양한 사내/외 Skill별 전문역량 강화 교육 및 자격증 지원제도를 운영하고 있으며,​ PM/PL 양성과정을 통하여 프로젝트 리더로 성장할 수 있도록 돕습니다</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image: url(../img/sub/people_19.png)" class="only_w"></span> 
                            <span style="background-image: url(../img/sub/people_19_m.jpg)" class="only_m"></span> 
                            <strong>리더역량​ <br class="only_m">강화 교육​</strong>
                        </div>
                        <div class="ico_desc">
                            <p>승진자 교육, 신임리더 교육, 리더역량 강화 교육, 리더 특강 등을 통하여​ 리더로서의 성장 및 역량을 강화하도록 체계적인 리더십 교육을 진행합니다.​</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image: url(../img/sub/people_20.png)" class="only_w"></span> 
                            <span style="background-image: url(../img/sub/people_20_m.jpg )" class="only_m"></span> 
                            <strong>온라인 <br class="only_m">교육</strong>
                        </div>
                        <div class="ico_desc">
                            <p>다양한 외부 온라인 교육채널과 연계하여 임직원들이 자유롭게​ 직무/비즈니스Skill/외국어/인문학/교양 수업을 수강할 수 있도록 지원합니다.</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image: url(../img/sub/people_21.png)" class="only_w"></span> 
                            <span style="background-image: url(../img/sub/people_21_m.jpg)" class="only_m"></span> 
                            <strong>교육 <br class="only_m">플랫폼</strong>
                        </div>
                        <div class="ico_desc">
                            <p>웹과 모바일 앱을 통해 업무관련 컨탠츠 및 Live방송을​ 시청하고 학습할 수 있도록 교육플랫폼을 운영하고 있습니다.​</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image: url(../img/sub/people_22.png)" class="only_w"></span> 
                            <span style="background-image: url(../img/sub/people_22_m.jpg)" class="only_m"></span> 
                            <strong>전자도서관</strong>
                        </div>
                        <div class="ico_desc">
                            <p>교보문고와 연계하여 임직원들이 자기개발을 위해 자유롭게 E-Book을 볼 수 있도록 지원합니다.​</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image: url(../img/sub/people_23.png)" class="only_w"></span> 
                            <span style="background-image: url(../img/sub/people_23_m.jpg)" class="only_m"></span> 
                            <strong>법정필수 <br class="only_m">교육과정</strong>
                        </div>
                        <div class="ico_desc">
                            <p>건강한 조직문화를 위하여 성희롱 예방 과정, 직장 내 괴롭힘 예방 과정,​ 직장 내 장애인 인식개선 과정, 정보보안 과정을 운영하고 있습니다.​</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image: url(../img/sub/people_24.png)" class="only_w"></span> 
                            <span style="background-image: url(../img/sub/people_24_m.jpg)" class="only_m"></span> 
                            <strong>교육문화</strong>
                        </div>
                        <div class="ico_desc">
                            <p>사내 임직원 강사로 구성된 직무 및 비즈니스Skill 교육이 활발하게 진행되고 있으며,​ 교육 소모임을 지원하여 자율적인 학습문화가 정착되어 있습니다.​</p>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>다양한 산업 경험</span>을 통해 분야별 최고 전문가로 성장</h3>
                </div>
                <div class="dot_wrap">
                    <strong><span>Career Path</span></strong>
                </div>
                <div class="img_wrap show">
                    <img src="../img/sub/people_25_m.jpg" alt="" class="only_m">
                    <img src="../img/sub/people_25.png" alt="" class="only_w">
                </div>
                <ul class="top_line">
                    <li>
                        <p class="list_cont"><i>·</i> 개발자 입사 후 체계적인 육성을 통해 기술전문가(TM)로 성장하거나 프로젝트리더(PL/PM), 사업리더로 성장가능</p>
                        <p class="list_cont"><i>·</i> 기술전문가로 인증될 경우, 그에 따른 보상 적용</p>
                        <p class="list_cont"><i>·</i> 육성면담, CDF 등을 통해 본인의 경력개발계획에 따라 직무를 변경할 수 있는 기회 제공</p>
                    </li>
                    
                </ul>
            </div>

           
            
            
           

        </div>

    </div> <!-- inner -->
     
    <div class="modal">
		
		<div class="modalBox">
            <button>&times;</button>
            <div class="img_box">
                <img src="" alt="">
            </div>
           
		</div>
	</div>


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(2).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('peo')
    });
</script>