<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub people system01" data-depth="people" data-menu="peo_02" data-subnav="peo_02">
    <div class="inner_1200">

    <?php include($_SERVER['DOCUMENT_ROOT'].'/people/peo_nav.php');?>

        <div class="sub_cont" data-depth="people" data-menu="system_01" data-stynav="system_01">

            <!-- <h2 class="ttl ttl_02">인사제도​</h2> -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/people/system_nav.php');?>

         

            <div class="cont_box t_box_no">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span> 성장·발전</span>의 원동력 ‘사람’</h3>
                </div>
                    <p class="desc_02 system_line_h">
                    최고의 IT 기업으로 성장·발전해 나가는 데 있어 <span class="border_line">가장   </span><span class="border_line">중요한 </span> <span class="border_line">원동력은</span><span class="border_line"> ‘사람’</span>이라고 생각합니다.</p>
                    <p class="desc_02">비즈테크파트너스는 고객가치 혁신과 미래 준비의 근본적 주체인 ‘사람’의 가치를 높이고자 노력합니다. </p>
                    <!--BEGIN: 2022 04 26 수정  -->
                    <p class="desc_02">임직원의 창의와 자율을 존중하고, 능력을 중시하며 성과에 걸맞는 대우를 보장하는 것을 인사원칙으로 삼고 있습니다.​</p>
                    <!--END: 2022 04 26 수정  -->

                <div class="dot_wrap">
                    <strong><span>보상제도</span></strong>
                </div>
                <p class="desc_02">매년 객관적이고 공정한 평가를 바탕으로  구성원들의 성과 향상을 위해 <br class="only_w">
                자발적 동기를 부여할 수 있는 보상체계를 운영하고 있습니다.  </p>

                <ul class="ico_s_100w dash">
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image: url(../img/sub/people_02.png)" class="only_w"></span> 
                            <span style="background-image: url(../img/sub/people_02_m.jpg)" class="only_m"></span> 
                            <strong>PS</strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>PS (Profit Sharing) → 회사 성과에 따른 보상​ </p>
                        </div>
                    </li>
                    <li>
                        <div class="ico_wrap">
                            <span style="background-image: url(../img/sub/people_03.png)" class="only_w"></span> 
                            <span style="background-image: url(../img/sub/people_03_m.jpg)" class="only_m"></span> 
                            <strong>PI</strong>
                        </div>
                        <div class="ico_desc">
                            <p><i> · </i>PI (Personal Incentive) → 개인별 성과(평가)에 따른 보상</p>
                        </div>
                    </li>
                   
                </ul>
              
              
            </div>

            <div class="cont_box">
                <div class="bor_ttl_box">
                    <h3 class="bor_ttl"><span>평가</span>원칙</h3>
                </div>
                <div class="img_wrap">
                    <img src="../img/sub/people_04_m.jpg" alt="" class="only_m">
                    <img src="../img/sub/people_04.png" alt="" class="only_w">
                </div>
            </div>

            
            
           

        </div>

    </div> <!-- inner -->
     
   


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(2).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('peo')
    });
</script>