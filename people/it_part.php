<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/head_inc.php');?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/inc/header_m.php');?>

<section id="container" class="sub people " data-depth="people" data-menu="peo_03" data-subnav="peo_03">
    <div class="inner_1200">

    <?php include($_SERVER['DOCUMENT_ROOT'].'/people/peo_nav.php');?>

        <div class="sub_cont" data-depth="people" data-menu="part_01" data-partnav="part_01">

            <!-- <h2 class="ttl ttl_02">직무소개</h2> -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/people/part_nav.php');?>

            <div class="ttl_box mar_ov  m0 ">
                <h2 class="ttl">IT</h2>
                <span class="line"></span>
            </div>

            <div class="cont_box">
                <ul class="part_list">
                    <li class="part_top">
                        <div class="part_ttl">직무</div>
                        <div class="part_desc">내용</div>
                    </li>
                    <li>
                        <div class="part_ttl">Enterprise Solution</div>
                        <div class="part_desc"> <p><i>·</i>  엔터프라이즈 솔루션 구현 및 운영업무를 수행하는 직무</p></div>
                    </li>
                    <li>
                        <div class="part_ttl">Application Development </div>
                        <div class="part_desc">  
                        <p><i>·</i>  Application System에 대한 업무를 이해하고, 구축 시 환경에 최적화된 방법론에 따라 요구사항 분석, 설계, 개발, 테스트, 전개를 수행하는 직무 </p>
                        <p><i>·</i>  ERP 사업부문 : SAP 개발 및 설계(ABAP / Fiori) LG CNS ERP 플랫폼 개발 및 설계</p>
                        <!-- BIGIN: 2022 04 25 텍스트 수정  -->
                        <p><i>·</i>  이행사업부문 : Web 개발 및 설계 </p>
                        <!-- END: 2022 04 25 텍스트 수정  -->
                        </div>
                    </li>
                    <li>
                        <div class="part_ttl">Data Science </div>
                        <!-- BIGIN: 2022 04 25 줄바꿈 수정  -->
                        <div class="part_desc">  <p><i>·</i>  문제 식별 및 데이터 모델링을 위한 데이터 저장, 처리, 분석, 시각화 그리고 활용까지 데이터와 관련된<br class="only_w"> 전 업무상에서 창의적 대안을 제시 할 수 있으며 데이터 분석 방법론과 최적 알고리즘을 설계하여 데이터 중 가치 있는 정보를 찾아 사업기획에 필요한 Data를 정의하고 내/외부의 다양한 형태의 Data를 결합하여 전방위적인 신규 사업기회 창출하는 직무 </p></div>
                          <!-- END: 2022 04 25 줄바꿈 수정  -->
                    </li>
                    <li>
                        <div class="part_ttl">Robotic Process Automation</div>
                        <div class="part_desc"> <p> <i>·</i>  업무 담당자와 로봇이 자동 수행 가능한 대상 업무를 파악하고 RPA솔루션(로봇)이 업무를 수행할 수 있도록 개발, 구축된 RPA시스템을 모니터링하고 안정적인 운영이 되도록 유지보수 수행하는 직무</p> </div>
                    </li>
                    <li>
                        <div class="part_ttl">Infra Architecture</div>
                        <div class="part_desc"> <p><i>·</i>  시스템(서버/스토리지/백업), 네트워크, 미들웨어, 클라우드 등 Infrastructure의 요구사항을 분석하여, 성능/가용성/운영편의 측면에서 최적의 아키텍처를 설계하고, 구축/테스트/운영하는 직무</p>  </div>
                    </li>
                    <li> 
                        <div class="part_ttl">User eXperience</div>
                        <div class="part_desc">  <p><i>·</i>  고객 비즈니스 성과 달성을 위해 사용자, 비즈니스, 신기술을 융합한 고객 경험을 창조하기 위해 서비스/제품/시스템의 기획, 디자인, 퍼블리싱 업무를 수행</p> </div>
                    </li>
                    <li>
                        <div class="part_ttl">IT보안 </div>
                        <div class="part_desc"> <p> <i>·</i>  정보보호 영역에 대한 분석, 설계, 구축, 테스트, 운영 등의 SI / SM 사업과 정보보호 컨설팅 업무를 수행하는 직무 </p> </div>
                    </li>
                </ul>
            </div>

           
            
            
           

        </div>

    </div> <!-- inner -->
     
   


</section>

<? include('../inc/footer.php');?>

<script type="text/javascript">

    $(document).ready(function(){
        $('#gnb ul').children().eq(2).addClass('active');

    // #gnb에 자식 요소(li)가 몇번째인지를 확인한 후 on이라는 클래스 추가
    $('.m_header').addClass('peo')
    });
</script>